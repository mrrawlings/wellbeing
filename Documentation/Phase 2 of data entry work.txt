
Meeting 10/8/2018
Meet in one month to demo search 
Add filter by activity type & status
Home page todo still

All done:


1. Add new first column onto far left-hand side of ‘Activity/Services’ table on Activities Page – this column to state ‘Primary Category’ i.e. the 7 categories - done

 - 1 hour (assuming we just show first main category if there happen to be activity types within different main categories for each activity)
 actual = 3 hours


2. Add/create home page with a summary of the database and Hand in Hand. Clive/Kirstie to supply the copy.
No data changes - Need copy / text

4. Add drop down to filters to activities for most columns - ie type, location, service, etc.
No data changes

5. Create link through on ‘Organisations’ tab which filters activities to the selected organisation.
Linked by provider not data owner 
 - 1 hour

6. Add a Verify button to data entry site and addition of links between a user and their Organisation

 - Will only be for Admin users 
 - Need to be able to create users! 
 - done - used a drop down instead 3 hours

7. Add an add user screen to create users for other organisations
 
Adding this functionality involves the following steps: 

 6.1 Linking users to Organisations within the database - note separate logins will be needed for "external" users - 1 hour

 6.2 Any activity data they add (or all data) will be set to a draft status when entered - 1/2 hour 

 6.3 There will need to be a way of filtering /finding these draft records - add filter & display status on Activity list screen?  - 1 hour

 6.4 Each activity entered can then be edited and the status field updated to verified (ignore reject options /non happy path here) - 1/2 hour

 6.5 This also means all existing data will need to be updated to verified status - 1/2 hour 

 6.6 Ensure the search application only shows verified data. - 1/2 hour 