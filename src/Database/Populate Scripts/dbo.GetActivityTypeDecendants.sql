alter FUNCTION dbo.GetActivityTypeDecendants 
(	
	-- Add the parameters for the function here
	@ActivityTypeId varchar(40)
)
RETURNS @ActivityTypeIds Table (ActivityTypeId uniqueidentifier, Description nvarchar(400)) 
AS
Begin
	-- insert this level
	Insert Into @ActivityTypeIds (ActivityTypeId,Description)
	select id, Description from ActivityTypes where id = @ActivityTypeId

	-- work down decendants
	if (exists(select id from ActivityTypes where ParentId = @ActivityTypeId))
	begin

		-- loop through next level of decendants id and recurse down
	    Declare @ChildId uniqueidentifier
		Declare @Description nvarchar
			
		DECLARE Temp_Cursor CURSOR FOR
		select id, Description from ActivityTypes where ParentId = @ActivityTypeId

		OPEN Temp_Cursor

		FETCH NEXT FROM Temp_Cursor Into @ChildId, @Description
		WHILE @@FETCH_STATUS = 0
		BEGIN
			--Insert Into @ActivityTypeIds (ActivityTypeId,Description)
			--Select @ChildId, @Description

			-- call on each decendant
			Insert Into @ActivityTypeIds (ActivityTypeId, Description)
			select ActivityTypeId, Description
			From dbo.GetActivityTypeDecendants(@ChildId)
					
			FETCH NEXT FROM Temp_Cursor Into @ChildId, @Description
		END

		CLOSE Temp_Cursor
		DEALLOCATE Temp_Cursor		
	end
		
	return
end
