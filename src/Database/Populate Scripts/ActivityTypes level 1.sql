
delete [ActivityTypes]

insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'1. Friendships and Connections', '00000000-0000-0000-0000-000000000000',getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'2. Keeping Healthy & Active', '00000000-0000-0000-0000-000000000000',getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'3. Ability to Get Out and About', '00000000-0000-0000-0000-000000000000',getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'4. Practical Home Help', '00000000-0000-0000-0000-000000000000',getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'5. Safety', '00000000-0000-0000-0000-000000000000',getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'6. Useful Information', '00000000-0000-0000-0000-000000000000',getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'7. Money and Benefits', '00000000-0000-0000-0000-000000000000',getdate())

select * from [dbo].[ActivityTypes]
