USE [Wellbeing]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 07/11/2018 17:13:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Activities]    Script Date: 07/11/2018 17:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Activities](
	[Id] [uniqueidentifier] NOT NULL,
	[DateCreated] [datetime2](7) NOT NULL,
	[DateDeleted] [datetime2](7) NULL,
	[DateTimeDescription] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[LocationDescription] [nvarchar](max) NULL,
	[LocationPostcode] [nvarchar](max) NULL,
	[OrganisationId] [uniqueidentifier] NULL,
	[Title] [nvarchar](max) NULL,
	[Contact] [nvarchar](max) NULL,
	[CreatedById] [uniqueidentifier] NULL,
	[DateUpdated] [datetime2](7) NULL,
	[Status] [int] NOT NULL DEFAULT ((0)),
	[UpdatedById] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Activities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[ActivityClassifications]    Script Date: 07/11/2018 17:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityClassifications](
	[Id] [uniqueidentifier] NOT NULL,
	[ActivityId] [uniqueidentifier] NULL,
	[ActivityTypeId] [uniqueidentifier] NULL,
	[DateCreated] [datetime2](7) NOT NULL,
	[DateDeleted] [datetime2](7) NULL,
	[TypePriority] [int] NOT NULL,
 CONSTRAINT [PK_ActivityClassifications] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[ActivityTypes]    Script Date: 07/11/2018 17:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityTypes](
	[Id] [uniqueidentifier] NOT NULL,
	[DateCreated] [datetime2](7) NOT NULL,
	[DateDeleted] [datetime2](7) NULL,
	[Description] [nvarchar](max) NULL,
	[ParentId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ActivityTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Organisations]    Script Date: 07/11/2018 17:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organisations](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[Contact] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Funding] [nvarchar](max) NULL,
	[Generic] [bit] NOT NULL DEFAULT ((0)),
	[Noteworthy] [bit] NOT NULL DEFAULT ((0)),
	[Phone] [nvarchar](max) NULL,
	[Religious] [bit] NOT NULL DEFAULT ((0)),
	[Website] [nvarchar](max) NULL,
 CONSTRAINT [PK_Organisations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 07/11/2018 17:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Users]    Script Date: 07/11/2018 17:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [uniqueidentifier] NOT NULL,
	[DateCreated] [datetime2](7) NOT NULL,
	[DateDeleted] [datetime2](7) NULL,
	[Email] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[RoleId] [uniqueidentifier] NULL,
	[UserCompanyId] [uniqueidentifier] NULL,
	[DateUpdated] [datetime2](7) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  View [dbo].[ActivityTypesLevel1]    Script Date: 07/11/2018 17:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ActivityTypesLevel1]
as

select * from [dbo].[ActivityTypes] where ParentId = '00000000-0000-0000-0000-000000000000'

GO
/****** Object:  View [dbo].[ActivityTypesLevel2]    Script Date: 07/11/2018 17:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ActivityTypesLevel2]
as

select * from [dbo].[ActivityTypes] where ParentId in (select id from [dbo].[ActivityTypes] where ParentId = '00000000-0000-0000-0000-000000000000')


GO
/****** Object:  View [dbo].[ActivityTypesLevel3]    Script Date: 07/11/2018 17:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ActivityTypesLevel3]
as

select * from [dbo].[ActivityTypes] where ParentId in(
	select id from [dbo].[ActivityTypes] where ParentId in (select id from [dbo].[ActivityTypes] where ParentId = '00000000-0000-0000-0000-000000000000')
)




GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20180415071935_Initial', N'2.1.1-rtm-30846')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20180418170645_Added-Organisation', N'2.1.1-rtm-30846')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20180418200704_Added-Contact', N'2.1.1-rtm-30846')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20180422164320_Added-UserAndRoles', N'2.1.1-rtm-30846')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20180430063647_New-Fields-Added-Organisation', N'2.1.1-rtm-30846')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20180521175048_Modified-ActivityTypeColumnType', N'2.1.1-rtm-30846')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20180726074321_Add-Org-User-Etc', N'2.1.1-rtm-30846')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'5bca4db8-4468-4a46-b0fb-00684d37478a', CAST(N'2018-10-23 10:56:34.9413677' AS DateTime2), NULL, N'Monday - Friday 8am - 6pm, Saturday 8am - 3pm', N'Gives impartial information about financial products and services and offers tips on everyday money management.
', NULL, NULL, N'3fc795b5-fb87-41b4-9be0-2c6bf299bd52', N'Money Advice Service', N'0800 138 7777', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 10:56:35.5447486' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'803a10e5-0bd3-4b29-94d8-010e63eac2d9', CAST(N'2018-10-18 10:31:20.7286970' AS DateTime2), NULL, NULL, N'The Olive Branch seeks to provide a professional counselling service demonstrating the Christian principle of love and care for each individual, within a Christian framework. People do not have to have any religious affiliation to use the service. All counsellors are professionally trained. Fees may apply. ', N'The Olive Branch Christian Counselling Service Company Limited
14 St. Clement Street
Winchester', N'SO23 9HH', N'7b781dc5-0d9c-401a-bff6-6f303297d227', N'Christian Counselling Service', N'01962 842858', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-18 10:31:20.7344644' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'907cb6c6-d189-45f5-91a1-014792872b3d', CAST(N'2018-09-12 20:59:26.4633333' AS DateTime2), CAST(N'2018-10-17 11:12:04.9612974' AS DateTime2), NULL, N'Forum dedicated to improving access and conditions for people with physical or sensory impairments in the Winchester area. Winchester Area Access for All (WAAFA) are looking for more people to join the forum. It is not mandatory that volunteers have mobility or sensory issues, we just want volunteers to help us get things done.  If you would like to assist then please contact us via this link for a discussion on how you can help.', N'Winchester City', NULL, N'1b705a0d-36b1-4e3f-bfd1-1c0ce6ad22f4', N'Winchester Area Access for All Forum', N'01962 855016 email - http://www.waafa.org.uk/index.html', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-02 15:17:56.6109239' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'bbc71a48-8e57-4a90-b864-027429639959', CAST(N'2018-10-23 11:05:55.6747833' AS DateTime2), NULL, N'Monday - Friday 8am - 6pm', N'Provides help to find a lost pension.
', NULL, NULL, N'a4826f67-c8ef-4973-9a80-8977e6c42e77', N'Pension Tracing Service', N'0800 731 0193', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:05:55.6800345' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'60840546-e7ff-4608-b4aa-0327f4490dc0', CAST(N'2018-10-23 08:01:31.9888123' AS DateTime2), NULL, N'Monday - Friday', N'AbilityNet is a national charity which helps people of any age and with any disability to use technology to achieve their goals at home, at work and in education.', NULL, NULL, N'7506a37d-a4cb-4cca-8aa7-ea3c386289a9', N'AbilityNet Digital Accessibility Services', N'0800 269 545', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 08:21:05.3857026' AS DateTime2), 1, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'b95557a1-589d-443f-a46c-03efc77c7fa1', CAST(N'2018-10-24 11:32:47.1534496' AS DateTime2), NULL, N'Monday - Friday 9am - 6pm ', N'Produces information on mental health issues and gives support and advice. It also provides information on local services available.', NULL, NULL, N'69d0fa49-a537-4e3a-a0ff-49cc9d9d80c9', N'Mind Support & Advice', N'0300 123 3393 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:32:47.1583842' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'4e15e5fd-096d-4f5a-8b0f-04031a50cd24', CAST(N'2018-10-22 11:52:07.2566726' AS DateTime2), NULL, NULL, N'Anyone with a mobility or sensory impairment can use the service, meaning that you cannot use or have discomfort in using public transport like a bus or taxi is welcome to use our service, you do not have to be registered disabled or a wheelchair user to be eligible, maybe you just struggle climbing the steps onto buses.  The Dial-a-Ride service is funded by Hampshire County Council and Winchester City Council.  It is operated by Community First.

You can go places such as shopping or social trips.  Assistance is always available from our well trained, friendly drivers so you don’t have to worry about getting on and off the bus.', N'Winchester Dial a Ride
Upper Parking Level,
The Brooks Shopping Centre
Winchester ', N'SO23 8QY', N'a200e8d4-8d61-4811-a623-ceeacbb764fb', N'Dial-a-Ride Winchester', N'01962 852602', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 09:21:57.2646098' AS DateTime2), 0, N'8fa3eab7-2709-42f9-82dc-8748566ea8fb')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'2674971a-28c7-44ef-b3e5-053e5e489982', CAST(N'2018-09-12 20:59:26.2300000' AS DateTime2), NULL, NULL, N'Helpline and national charity providing impartial advice, information and training on all aspects of independent living.#rtn##rtn#The Disabled Living Foundation (DLF) offers advice and guidance about products and suppliers as well as online discussion forums. ', N'Winchester City', NULL, NULL, N'Online and Phone Support', N'', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'ce2fffb8-dc7d-4d73-9eac-056ba1d33954', CAST(N'2018-10-24 14:41:39.6679874' AS DateTime2), NULL, N'Monday - Friday 9am - 5pm', N'The society offers support and information about funerals, along with contact details for Society members in the UK. 
', NULL, NULL, N'4a0d42e0-40eb-4a75-a7ef-9546cf6c4bde', N'Information on Funerals', N'0345 230 6777 or 01279 726 777', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 14:41:39.6730580' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'e068fc72-f335-4a52-86be-070385253002', CAST(N'2018-09-12 20:59:26.1200000' AS DateTime2), NULL, N'Tuesdays 11:45am', N'Held every Tuesday from 11:45 at the Jolly Farmer, Andover Road', N'The Jolly Farmer, Andover Road', N'SO22 6AE', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'WLAH Men''s Pub Lunch', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:24:17.0814930' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'3924157e-f81a-4b7c-ae14-08465e15e1a1', CAST(N'2018-09-12 20:59:25.6666667' AS DateTime2), NULL, N'Monday - Sunday, 8am – 7pm', N'Counselling for people with memory loss or dementia.', N'18 St George''s Street
Winchester
Hampshire', N'SO23 8BG', N'5788fcec-2508-4793-8a15-ebbc47a0bb30', N'Time to Talk Service', N'01962 868545 / 0800 328 7154', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 08:21:58.8851283' AS DateTime2), 1, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'8669a19a-7b71-4c00-8caf-085cd4aa4e67', CAST(N'2018-10-08 08:08:16.5485242' AS DateTime2), NULL, NULL, N'Lifeline is an alarm system that allows you to call for held day or night, 365 days a year. It is available to all residents living in the Winchester district and provides peace of mind for you and your family. The lifeline unit is rented, there is no installation charge or membership fee and the cost is less than 50p per day approximately.', N'Winchester City Council
City Offices
Colebrook Street
Winchester', N'SO23 9LJ', N'1468dc81-30ce-4963-bf27-8e26f76f8577', N'Winchester City Council Lifeline', N'01962 855 335', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-08 08:08:16.5544463' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'177f0be4-5f80-4519-af10-0b45d0875743', CAST(N'2018-09-12 20:59:26.0866667' AS DateTime2), NULL, NULL, N'Hospital transport, GP transport, prescription collection, Lunch Club (1st Friday of each month)', N'Winchester City', NULL, NULL, N'Transport assistance/lunch club', N'Telephone Shirley Lupton (scretary) 01962 884376', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'0e6dc22a-2809-4fa3-96ef-0c06e79d58c7', CAST(N'2018-10-26 10:01:27.0802191' AS DateTime2), NULL, NULL, N'Hampshire Cultural Trust was established as an independent charity in 2014 to promote Hampshire as a county that offers outstanding cultural experiences to both its residents and visitors. It offers a range of one-off exhibitions, events and activities.', NULL, NULL, N'29c2618a-b697-4f23-9c8c-65d8587060a4', N'Cultural Classes & Workshops', N'01962 678140 www.hampshireculturaltrust.co.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-26 10:01:27.0865051' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'31a645f4-8ea3-4d4c-8850-0d50b876e7d0', CAST(N'2018-10-18 10:37:18.1293682' AS DateTime2), NULL, NULL, N'Hospital and GP transport – other health transport, lunch club
', N'Bishops Waltham
', N'SO32 1AD', N'd6bec560-9707-46af-9857-dc5029149a86', N'Bishops Waltham Voluntary Care Group', N'01489 893140', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-18 10:37:18.7951155' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'7222d817-425c-47a2-92fb-0d7a669efba2', CAST(N'2018-09-12 20:59:26.1500000' AS DateTime2), NULL, N'Saturdays, 10:30am – 11:30am', N'Hour long classes at various locations, for those recovering from a fall.

', N'Colden Common Community Centre/Village Hall
St Vigor Way
Winchester', N'SO21 1UU', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Saturday Exercise Plus', N'01692 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 09:46:54.3405807' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'60113e12-da31-4569-89ed-0d9ae9eaadef', CAST(N'2018-10-18 10:42:10.7130153' AS DateTime2), NULL, NULL, N'Hospital and GP transport – other health transport
Other ad hoc lunches', N'Twford, Winchester', N'SO21 1NS', N'd6bec560-9707-46af-9857-dc5029149a86', N'Twyford Good Neighbours', N'01962 713033', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-18 10:42:10.7176506' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'ef6ef08d-0692-4323-aba5-0e0b6299a6b2', CAST(N'2018-09-12 20:59:25.9300000' AS DateTime2), NULL, NULL, N'For those nearing or in retirement. We meet for pub lunches, walks, concerts, Bible study, weekends away and more.', N'Winchester City', NULL, NULL, N'Christ Church Third Way', N'01962 864454 office@ccwinch.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'9b81dbe9-8631-4ed7-a1b2-0e92795e9f9a', CAST(N'2018-10-22 10:39:09.9891074' AS DateTime2), NULL, N' Monday 11:45am - 13:15pm', N'Held every Monday, 11:45 - 13:15, half an hour for chat and coffee before an hour of singing', N'United Church, Jewry Street, Winchester', N'SO23 8RZ', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Singing for Wellbeing: Class Two', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:39:09.9937271' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'fa3c8cbb-7e4c-4b38-92ef-11a9338a2db9', CAST(N'2018-09-12 20:59:25.6966667' AS DateTime2), NULL, NULL, N'If you live in the Winchester District, have a disability or are finding increasing difficulty in managing in your home, you many qualify for help towards the cost of adaptions to enable you to live more safely and independently at home. ', N'City Offices, Colebrook Street, Winchester', N' SO23 9LJ ', N'1468dc81-30ce-4963-bf27-8e26f76f8577', N'Grants for Home Adaptions', N'01962 848455 disabledfacilitiesgrant@winchester.gov.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 10:03:46.4150030' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'fe2ec5b2-57a7-4c57-852d-1227a87f7878', CAST(N'2018-09-12 20:59:26.0700000' AS DateTime2), NULL, NULL, N'Meet at Hyde Tavern, Hyde Street, Winchester. 3rd Thursday of the month. Informal events, happy to see new people. Contact Kath Whiting', N'Winchester City', NULL, NULL, N'Book Group', N'literature@hyde900.co.uk website - www.hype900.co.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'154516e0-2ac0-4e91-aef0-1590b9a9178e', CAST(N'2018-10-23 14:34:42.4765612' AS DateTime2), NULL, NULL, N'National charity providing housing for older people with varying levels of support.

www.abbeyfield.com ', NULL, NULL, N'f4b77647-7221-4596-868b-2331cbce7d4a', N'Abbeyfield Housing Support', N'01727 857 536 enquiries@abbeyfield.com ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 14:34:42.4814285' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'a2169115-dbda-4332-a986-18d4161c903f', CAST(N'2018-09-12 20:59:26.4500000' AS DateTime2), NULL, NULL, N'Network of health walking schemes across the country helping people to live a more active lifestyle & improve mental and physical wellbeing.#rtn##rtn#Find a health walk scheme near you.', N'Winchester City', NULL, NULL, N'', N'020 7339 8541 walkingforhealth@ramblers.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'7934cf81-218f-49d1-9306-198c20eb35d9', CAST(N'2018-10-23 10:33:02.2277338' AS DateTime2), NULL, N'Tuesdays & Thursdays, 11am - 1pm', N'Lunch served at 12pm with the opportunity to socialise, used to be run by Age UK Mid Hants. ', N'Winchester Baptist Church
Swan Lane
Winchester
', N'SO23 7AA', N'f8089372-5a5d-4f22-b937-1f3ccde4dd4b', N'Winchester Wellbeing Lunch Club', N'01962 868 770', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 10:33:03.1798511' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'623c5c31-0a8a-45f1-ab9e-1af4f1987304', CAST(N'2018-10-17 14:53:29.2238385' AS DateTime2), NULL, N'Fridays 6am - 7am', N'The chance to get together for worship and early morning prayers.', N'Hope Church Winchester
The Middle Brook Centre
Middle Brook Street
Winchester', N'SO23 8DQ', N'b7c1e612-53a6-447a-87ef-8068b8257c95', N'Early Morning Prayer', N'01962 840 800', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 14:53:29.2286552' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'f3cc5abb-54b9-481e-a8de-1b00af6e584f', CAST(N'2018-09-12 20:59:26.1966667' AS DateTime2), NULL, NULL, N'Action on Hearing Loss is the largest UK charity offering support and advice to help people confronting deafness, tinnitus and hearing loss to live the life they choose', N'Winchester City', NULL, NULL, N'', N'0808 8080 123 informationline@hearingloss.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'0ffc9eef-9902-4bdd-a423-1b7537215b1e', CAST(N'2018-09-12 20:59:25.9000000' AS DateTime2), NULL, NULL, N'Monday. Tuesday and Friday at varying times', N'Badger Farm, Winchester City', NULL, NULL, N'Yoga', N'01962 868346 Chris Wyeth', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'abc3e56a-b735-4998-91d8-1be2f92c34bf', CAST(N'2018-09-12 20:59:26.2900000' AS DateTime2), NULL, NULL, N'National organisation offering information and support for prostate cancer in the UK.#rtn##rtn#If you are concerned about prostate cancer or prostate problems we can help. We provide a range of information and support so you can choose the services that work for you. All our services are open to men, their family and their friends.#rtn##rtn#Find local support.', N'Nationwide', NULL, NULL, N'', N'020 3310 7000 info@prostatecanceruk.org', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'bc0d8304-bb76-4dc2-897d-1ca621e341e7', CAST(N'2018-09-12 20:59:25.8366667' AS DateTime2), NULL, NULL, N'Information and support for anyone affected by dementia. 3rd Thursday of each month 1.30pm to 3.30 pm. St. Cross Grange, 140 St Cross Road, Winchester S023 9RJ', N'St Cross, Winchester City', NULL, NULL, N'Alzheimer Information Drop In', N'01962 865585 (Sheila Ancient, Support Worker)', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'7cdf60ac-8a45-4e8d-89b4-1d828040e36a', CAST(N'2018-10-17 14:12:35.8938171' AS DateTime2), CAST(N'2018-10-17 14:54:22.5676928' AS DateTime2), N'Mondays 10', N'The Monday Friendship Group caters mostly for the older generation, although all ages are very welcome. The group meets at the MBC every Monday morning from 10.30am, have lunch at 12 noon and finish about 1.00pm.  This is a time for all to enjoy each others company, have fun, meet with new friends, go on outings, worship together and hear the Word of God with lively discussion.  We have a heart of friendship and also enjoy delicious meals together.', NULL, NULL, N'1b705a0d-36b1-4e3f-bfd1-1c0ce6ad22f4', N'The Monday Friendship Club', NULL, N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 14:12:35.9112491' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'dbbd1826-3a3b-43da-8b60-1df263f852c9', CAST(N'2018-10-24 11:12:06.8007972' AS DateTime2), NULL, NULL, N'Provides information and support for people living with bladder and bowel control problems.
', NULL, NULL, N'ba5d4210-43ff-4ecb-a5aa-304c8e4f1988', N'Bladder and Bowel Support', N'01926 357 220', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:12:07.4995339' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'3379e3cf-c038-49a8-8efb-1e1900412337', CAST(N'2018-09-12 20:59:25.9166667' AS DateTime2), NULL, NULL, N'Meet alternate Fridays 2 - 4pm at Weeke Community Centre, Taplings Road, SO22 6HG', N'', NULL, NULL, N'Weeke Friendship Club', N'01962 857099 Mrs Fellingham', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'3a63d748-c014-4c6f-ad67-1e4a754efe4f', CAST(N'2018-10-17 14:35:43.8558507' AS DateTime2), CAST(N'2018-10-17 14:37:18.5575755' AS DateTime2), N'Mondays 10:30am - 1pm', N' The Monday Friendship Group caters mostly for the older generation, although all ages are very welcome. The group meets to enjoy each others company, have fun, meet with new friends, go on outings, worship together with lively discussion.  Lunch is served at 12pm.', N'Hope Church Winchester
The Middle Brook Centre
Middle Brook Street
Winchester
Hampshire', N'SO23 8DQ', N'b7c1e612-53a6-447a-87ef-8068b8257c95', N'The Monday Friendship Club', N'01962 840800', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 14:35:44.2441181' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'ce7a0ca1-b554-4a6d-a3aa-201a20a23067', CAST(N'2018-10-23 11:28:19.0038403' AS DateTime2), NULL, NULL, N'Turn2us is a national charity helping people when times get tough. They provide financial support to help people get back on track. Helps people access the money available to them through welfare benefits, grants and other sources. 
www.turn2us.org.uk ', NULL, NULL, N'6a6c8ecd-24d4-43e7-8a32-bc1f436e22a3', N'Turn2Us Charity Helpline', N'0808 802 2000', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:28:19.0112688' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'cc712f68-74f3-428d-8ddb-204bf8ea7559', CAST(N'2018-10-17 11:17:13.6772844' AS DateTime2), NULL, N'Monday - Friday 9:00am - 5:30pm', N'Forum dedicated to improving access and conditions for people with physical or sensory impairments in the Winchester area.  Their aim is to improve inclusion, participation and representation by disability access groups and individuals in multi-agency partnerships and networks. ', N'Winchester Discovery Centre
Jewry Street
Winchester
', N'SO23 8SB', N'f5152061-fdc8-4618-8fb0-9953e33a0ed1', N'Winchester Area Access For All Safety Advice', N'01962 855016 info@waafa.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 11:17:13.6832074' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'8dc80ea9-6642-4900-96b1-204ef84249de', CAST(N'2018-10-23 11:36:14.7235962' AS DateTime2), NULL, NULL, N'Registers Lasting Powers of Attorney and helps attorneys carry out their duties in England and Wales.

', NULL, NULL, N'ecf53e7b-3a89-4bc6-b189-6131c81e03d7', N'Office of the Public Guardian', N'0300 456 0300', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:36:15.7100102' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'9620f8fa-6c4e-4b64-845a-2083dfc01203', CAST(N'2018-10-24 15:01:24.1598855' AS DateTime2), NULL, N'Tuesday - Thursday', N'Offers research material, guidance and support for people interested in looking into their family history.', NULL, NULL, N'b33cbe20-7add-49a2-92a2-aa7ce31d172d', N'Information for People Interested in Genealogy ', N' 020 7251 8799 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 15:01:24.1679600' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'680aeb19-91d3-4a78-b47d-20b7a455dc07', CAST(N'2018-10-24 14:58:13.7253259' AS DateTime2), NULL, NULL, N'Gentle exercise-to-music classes for older and disabled people.', NULL, NULL, N'9f291be7-f9a3-4142-ad9d-1a3eb9ed126f', N'Exercise-to-Music Classes', N'01582 832 760 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 14:58:13.7298320' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'5d3ab072-3015-4165-a94e-20d3c81c9f8c', CAST(N'2018-10-22 10:06:43.3985894' AS DateTime2), NULL, N'Wednesday 10:30am - 12:00pm', N'An hour long session followed by tea & coffee', N'The United Church, Jewry Street, Winchester', N'SO23 8RZ', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Wednesday Seated Exercise Class', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:06:43.4035325' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'f148fb24-0615-46d8-bceb-212ab7b24fe3', CAST(N'2018-09-12 20:59:25.7133333' AS DateTime2), NULL, NULL, N'Able to give advice to anyone affected by Dementia', N'', NULL, NULL, N'Living with Alzheimer''s and Dementia advice', N'See organisation', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'77085df5-391f-45f6-8ed2-220d2c76da9c', CAST(N'2018-09-12 20:59:25.9633333' AS DateTime2), NULL, NULL, N'', N'Winchester City', NULL, NULL, N'', N'', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'eb5811b9-4624-4d28-8c8d-2220c46c57bd', CAST(N'2018-10-22 10:08:10.8557108' AS DateTime2), NULL, N'Friday 10:30am - 12pm', N'An hour long session followed by tea & coffee', N'St Gregory''s Church, 1 Grange Road, Alresford', N' SO24 9HD', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Friday Seated Exercise Class', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:08:10.8611539' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'23ccd417-98be-439d-8f23-23ebad8f28fc', CAST(N'2018-10-22 10:29:21.8390882' AS DateTime2), NULL, N'Tuesdays 11:45am', N'Every week for everyone, not just men. ', N'The Roebuck Inn, Stockbridge Road', N'SO22 6RP', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'WLAH Tuesday Pub Lunch', N'01962 890995 Winchester.LiveAtHome@mha.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:29:22.6640398' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'f7f12eb5-aa7e-4c28-8a73-24f9ebf2f52c', CAST(N'2018-09-12 20:59:25.7433333' AS DateTime2), NULL, NULL, N'New age indoor curling. Takes place 2nd and 4th Friday of the month at 10:1m. All welcome, just turn up.', N'', NULL, N'9ec6f994-dfa3-4da2-923d-e5d34a7ca7a3', N'New Age Curling', N'See organisation', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'ba9ea71a-3ee9-4957-939b-25baed747e60', CAST(N'2018-09-12 20:59:25.9166667' AS DateTime2), NULL, NULL, N'Provides friendship and fun for older people. Alternate Thursdays 2 pm - 4 pm. Cost of £2.50 per session at Badgers Farm Community Centre, Badger Farm Road SO22 4QB. Spaces available, transport provided. ', N'Badger Farm, Winchester City', NULL, NULL, N'Badgers Farm Friendly Group - ', N'01962 8577099 Mrs C Fellingham', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'8d24a992-5af1-40e6-b0f0-25f8064beb74', CAST(N'2018-10-17 10:40:07.8148339' AS DateTime2), NULL, N'Tuesdays 10am - 11am', N'Chair-based dance class for over 65''s. Fun and relaxed with a focus on exploring movement, learning new skills and increasing fitness and wellbeing. Refreshments provided and the chance to socialise in the cafe. £2 per class.', N'Unit 12 
Winnall Valley Road
Winchester', N'SO23 0LD', N'2627e3dc-242e-4d72-a951-6ded72f754f7', N'Integr8 Movement Gems - Gentle Dance', N'01962 807328', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 10:40:07.8200779' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'ee49590b-f0cd-4165-beac-2857735de061', CAST(N'2018-09-12 20:59:26.4633333' AS DateTime2), NULL, N'Tuesdays 10am - 12pm', N'Rock Choir is the pioneering contemporary choir of the UK and remains unique with over 20,0000 members enjoying it across the UK. Each member is welcomed into the Rock Choir family and their journey with us allows music, friendship and a newfound confidence to impact their everyday lives. Together we have created a safe, up-lifting and exciting experience just for you.

Weeke Community Centre offers a range of clubs and activities catering for all groups and ages in the North Winchester area. This includes: Bridge, Rock Choir, Senior Line Dancing, Painting, Keep Fit, Weight Management, Brendon Care and Upholstery. The centre also has a Social Club open to all Association Members offering additional activities and social events. ', N'Weeke Community Centre
Taplings Road
Weeke
Winchester', N'SO22 6HG ', N'ea55afce-d9fd-4bc6-8913-ac2b4bf4041d', N'Weeke Community Centre Rock Choir ', N'01252 714 276 office@rockchoir.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 14:57:24.2873555' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'28cd5ae4-539c-4325-ae40-2a838b025968', CAST(N'2018-10-24 14:51:12.5988669' AS DateTime2), NULL, N'Monday - Friday 9am - 5pm', N'The only specialist national charity for older people and their much loved pets. A network of 15,000 volunteers “hold hands” with owners to provide vital loving care for their animals. The Cinnamon Trust keeps them together - for example, they will walk a dog every day for a housebound owner, foster pets when owners need hospital care, fetch cat food, or even clean out a bird cage, etc. When staying at home is no longer an option, their Pet Friendly Care Home Register lists care homes and retirement housing that are happy to accept residents with pets, and providing previous arrangements have been made they will take on life-time care of a bereaved pet. Emergency calls available 24/7 outside of usual hours.', NULL, NULL, N'62164edd-606d-4f5a-9a87-f4c9ca33ee7f', N'Support for People with Pets', N'01736 757 900', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 14:51:12.6066203' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'5bb91980-d0f1-4c88-902c-2b33cf696a4d', CAST(N'2018-10-24 11:22:07.4533565' AS DateTime2), NULL, N'Monday - Friday 08:30am – 5:30pm ', N'A local Healthwatch is an independent organisation whose staff listen to and share your views to make local services better. It can put you in touch with your local NHS Complaints Advocacy service. ', NULL, NULL, N'e7b6af90-e9d3-47a2-ac62-d9b54185d0aa', N'Healthwatch England ', N'03000 683 000 enquiries@healthwatch.co.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:22:08.0016329' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'a66ac502-5397-4a75-b5a1-2b69b6140f89', CAST(N'2018-10-23 14:59:59.3903080' AS DateTime2), NULL, N'Monday - Friday 9am - 5pm', N'Government-endorsed scheme for traders to ensure they meet industry standards. TrustMark accreditation gives customers reassurance of quality and protection from rogue traders. ', NULL, NULL, N'd71c3fe3-289d-48d9-bbf2-988e6dac63e5', N'TrustMark Trades Checker', N'0333 555 1234 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 14:59:59.3948062' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'9830d213-a21e-4545-9ebe-2b78ed2d78d9', CAST(N'2018-10-17 09:44:01.4909764' AS DateTime2), NULL, NULL, N'For people with dementia, their carers, family and friends. Andover Mind''s Dementia Advice Service is a point of contact and support from first diagnosis and beyond. The answer questions, listen, help to plan for the future, and provide tailored information on how to live well while living with dementia.', NULL, NULL, N'b52ee183-65c9-4245-b9cd-5bdcbd504110', N'Dementia Advice Service', NULL, N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 09:44:01.4960583' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'03e533b3-1f0a-4bb2-a7ed-2bff6c1c39af', CAST(N'2018-09-12 20:59:25.7133333' AS DateTime2), NULL, NULL, N'Details needed here', N'', NULL, N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Live at home', N'See organisation', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'164fabe1-1c19-4b74-8115-2dd086680e3c', CAST(N'2018-09-12 20:59:25.9300000' AS DateTime2), NULL, NULL, N'Saturdays 2pm - 4pm at the Learning Rooms, Discovery Centre, SO23 8SB', N'', NULL, NULL, N'Winchester Weekend Club', N'01962 857099 Andy Hogg', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'317eb298-5ed6-4ab5-b73e-2f2885519cec', CAST(N'2018-09-12 20:59:25.9000000' AS DateTime2), NULL, NULL, N'Fridays 8.30am - 11am', N'Badger Farm, Winchester City', NULL, NULL, N'Winchester Country Market', N'01962 855977 Mrs Tina Sharpe', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'b2d0ee29-b9a3-4c08-9b45-2f47b992daba', CAST(N'2018-10-23 11:39:48.4735191' AS DateTime2), NULL, NULL, N'Independent national organisation of lawyers who are committed to helping older people and their families and carers.
 ', NULL, NULL, N'21b9f466-c45a-48d2-ad0e-a2e0f2c604e6', N'SFE (Solicitors for the Elderly)', N'0844 567 6173', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:39:48.4784282' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'6c3a2126-b188-4bc6-9f8f-31d2f5864b3c', CAST(N'2018-10-23 11:01:46.4464724' AS DateTime2), NULL, NULL, N'Government office that provides information on National Insurance contributions.

www.gov.uk/check-national-insurance-record
', NULL, NULL, N'ab5150dd-db93-436b-809a-77782f3d2c17', N'National Insurance Contributions ', N'0300 200 3500', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:01:48.0388173' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'eb94626a-9f4e-489f-b208-31e74de04093', CAST(N'2018-10-17 14:42:46.0202575' AS DateTime2), NULL, N'Thursdays 1pm - 3pm', N'Drop in service, pointing older people to help, information, local support services, social clubs and activities.', N'Hope Church Winchester
The Middle Brook Centre
Middle Brook Street
Winchester
Hampshire', N'SO23 BDQ', N'b7c1e612-53a6-447a-87ef-8068b8257c95', N'Hope Church Compass ', N'01962 840 800 hopelife@hopewinchester.org', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 14:47:53.5120302' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'45e2939a-43f9-46a8-8e44-32b19e79c86a', CAST(N'2018-10-23 11:32:09.9491923' AS DateTime2), NULL, NULL, N'Government-run information line providing information about the Winter Fuel Payment, guidance on reporting changes to your circumstances, and a claim form.
', NULL, NULL, N'6f067daf-4f71-48ee-80b1-3a99758f8da7', N'Winter Fuel Payments Advice', N'0800 731 0160', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:32:09.9538695' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'8c131023-4ec2-4fc1-a347-349cc7a61a21', CAST(N'2018-10-22 09:51:52.4770878' AS DateTime2), NULL, N'Alternate Fridays, 10:30am - 1pm', N'Includes a range of activities as well as a sandwich lunch served at 12:30. Sessions include Craft & Coffee, Jigsaws, Table Tennis and New Age Kurling
', N'The United Church, Jewry Street, Winchester', N' SO23 8RZ', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'WLAH Friday Activity Day', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 09:51:52.4819267' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'06804950-a493-4290-81cf-370458be8010', CAST(N'2018-10-17 09:26:36.6654579' AS DateTime2), NULL, NULL, N'Operates across Eastleigh, Fareham, Havant, Gosport and Winchester. The agency helps people to remain in their own home.', NULL, NULL, N'770aa7d1-3ed4-4a12-8068-1608b30d9cdd', N'Home Improvement Agency', N'02380 462444', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 09:26:37.1297266' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'5661b687-fba2-4a62-898b-370cafe3826d', CAST(N'2018-10-24 11:18:26.7994244' AS DateTime2), NULL, N'Monday - Friday 9am - 5:45pm', N'Provides information, support and services to help people manage their diabetes.', NULL, NULL, N'a960a657-09d6-4966-9d84-bb5e839877ff', N'Diabetes UK Helpline & Support', N' 0345 123 2399 helpline@diabetes.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:18:26.8040226' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'12d68d05-de09-4f3c-b7fd-3722d00daee7', CAST(N'2018-10-17 10:33:45.8082198' AS DateTime2), CAST(N'2018-10-17 10:38:45.6914165' AS DateTime2), N'Tuesday 10am - 11am', N'Chair-based dance classes for over 65''s. Fun and relaxed with a focus on exploring movement, learning new skills and increasing fitness and wellbeing. Refreshments provided and the chance to socialise in the cafe.', N'Unit 12
Winnall Valley Road
Winchester', N'S023 0LD', N'1b705a0d-36b1-4e3f-bfd1-1c0ce6ad22f4', N'Integr8 Movement Gems - Gentle Dance ', NULL, N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 10:33:45.8135859' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'1e0a336f-039d-49ec-8383-387f2af96d50', CAST(N'2018-09-12 20:59:26.2433333' AS DateTime2), NULL, NULL, N'Key Changes is a registered charity that provides a comprehensive and professional music therapy service to the local community in Hampshire and surrounding areas. #rtn##rtn#Music therapy is a creative therapy and a largely non-verbal approach to personal therapy. It is increasingly recognised as a valuable part of the care of people, of all ages, who have sensory, physical, learning or neurological disabilities, or who have emotional or behavioural problems.', N'Winchester City', NULL, NULL, N'Music Therapy', N'01962 842269 email - info@keychanges.org', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'4c3b518c-3c3e-41f8-b8e0-395fd06c429c', CAST(N'2018-10-26 09:47:00.0949314' AS DateTime2), NULL, NULL, N'Helping the elderly, disabled or anyone who can''t drive continue to be mentally and physically active within their community. DBS checked & first-aid trained drivers. DMD Winchester help people to maintain their independence and to support them when family are unable to.  They are flexible and able to adapt service to meet individual needs - their aim is to help people remain mobile, encourage social interaction and provide companionship to help them to live life to the full.', NULL, NULL, N'16eed828-91e1-4d67-af90-7aca0a0abf98', N'Driving Miss Daisy Winchester', N'07552 331353 winchester@drivingmissdaisy.co.uk ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-26 09:47:00.9368676' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'b0b588ee-4cee-42b6-96b6-3b2792388a8f', CAST(N'2018-09-12 20:59:26.2133333' AS DateTime2), NULL, NULL, N'Charity providing advice and support to people with diabetes.#rtn##rtn#Our wide range of products and services are designed to help you manage your diabetes whatever support you need. Whether you''re looking for insurance, information, or just someone to talk to – Diabetes UK are here to help.#rtn##rtn#Find your local support group within Hampshire.', N'Winchester City', NULL, NULL, N'Local support groups', N'', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'aedcf327-e408-4157-8aa9-3c328bd64b08', CAST(N'2018-09-12 20:59:25.9166667' AS DateTime2), NULL, NULL, N'Social club held fortnightly on a Wednesday from 2pm - 4pm. £2.50 per session at St Marks Church, Olivers Battery Road South', N'Olivers Battery', NULL, NULL, N'Battery Club', N'01962 852650 or 07860193119 Mr J LeRiche', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'd09844d0-5d78-46ec-bb2f-3e2919f437b2', CAST(N'2018-10-26 10:26:41.7613452' AS DateTime2), NULL, N'24/7', N'Offering company and conversation, whether they live alone and cannot see their family and friends as often as they would like, or if they are recently bereaved and are finding it hard to adjust. Home Instead Senior Care aims to prevent loneliness through companionship care. Their CAREGivers'' support ranges from giving  a hand with the weekly shop, accompanying to doctor’s appointment or to a favourite social group or event, to make sure you’re eating healthily or just fancy popping out for lunch. ', N'Construct House
Winchester Road
Alresford', N'SO24 9EZ', N'e3f960ee-a32f-4105-b30e-4141e86a64e8', N'Home Instead Companionship Care Services', N'01962 736681', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-26 10:32:56.8085426' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'dc208ac0-052a-47db-90d5-4005566ac888', CAST(N'2018-09-12 20:59:26.1800000' AS DateTime2), NULL, N'First Friday of the month, 10:30am - 12:30pm', N'Also offers one to one befriending, help with shopping as required and signposting to other groups and organisations that may be of help.', N'The United Church, Jewry Street, Winchester', N'SO23 8RZ', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Winchester Memory Café', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:10:00.8155372' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'7a6f07a7-ee07-433a-8556-401fa20d2b10', CAST(N'2018-09-12 20:59:26.0233333' AS DateTime2), NULL, NULL, N'Meet at St Paul''s Church Rooms, St Paul''s Hill, Winchester. Monthly meetings for elderly people and those with disabilities, with transport, outings, home and hospital visits. 3rd Monday or each month.', N'Winchester City', NULL, NULL, N'', N'01962 881173', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'a0a269cf-fbb8-4f25-856c-407be1601635', CAST(N'2018-10-23 11:16:23.6379851' AS DateTime2), NULL, N'Monday - Friday 8am - 8pm, Saturday 8am - 4pm', N'Provides debt advice and can help you manage your debts.
', NULL, NULL, N'5a53e37a-a8e1-4063-89a5-3d118efd0b67', N'StepChange Free Debt Helpline', N'0800 138 1111 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:18:09.2177732' AS DateTime2), 1, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'3fccd1f8-9abb-44b0-8c0b-41b09799cf7e', CAST(N'2018-09-12 20:59:26.0100000' AS DateTime2), NULL, NULL, N'', N'Winchester City', NULL, NULL, N'Bereavement Support', N'01962 863626', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'04c010c4-1a4d-4c7a-a303-41cf69f0ef46', CAST(N'2018-09-12 20:59:25.8200000' AS DateTime2), NULL, NULL, N'Alzheimer Café facilitated by Princess Royal Trust for carers in Hampshire.', N'', NULL, N'1c4f6bf4-3096-41d8-b7a5-ec5b27436833', N'Alzheimer Café', N'See organisation', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'70369370-1415-4bd5-94df-4249e27642ea', CAST(N'2018-10-24 11:38:38.6618676' AS DateTime2), NULL, NULL, N'Provides information about local NHS services in England and information and advice on a range of health conditions and keeping healthy.
', NULL, NULL, N'348a9d72-a67d-44ee-82e0-74a9fb55015a', N'NHS Choices Online Service', N'www.nhs.uk ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:38:39.4885088' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'33dea8ac-b9f5-45fe-bd38-431a35cf53a9', CAST(N'2018-09-12 20:59:26.1966667' AS DateTime2), NULL, NULL, N'Useful advice and details on products that make daily living easier if you have mobility difficulties, vision loss, hearing loss or are experiencing memory loss', N'Winchester City', NULL, NULL, N'Online Support', N'0300 999 0004 info@dlf.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'ab86d85e-dc8f-4240-ba5a-443e53202fbb', CAST(N'2018-10-24 14:34:24.4568796' AS DateTime2), NULL, N'Monday - Friday 8am – 6pm, Saturday 11am – 5pm ', N'Marie Curie provides end-of-life care to people with cancer and other life-limiting illnesses in their own home or hospices. It also provides a phone line for emotional support.', NULL, NULL, N'1318d97d-50b5-49e9-9370-390b748ccf60', N'Marie Curie Cancer Support', N'0800 716 146 supporter.relations@mariecurie.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 14:34:24.4613005' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'715d75cf-8a67-4412-8ae4-46530db662d3', CAST(N'2018-10-17 11:50:55.4125119' AS DateTime2), NULL, N'Tuesdays 10am - 11am', N'£3.15 per person', N'River Park Leisure Centre
Gordon Road
Winchester
', N'SO23 7DD', N'20386a91-5cab-42a3-9bd8-3153d82cdcef', N'River Park Badminton Class', N'01962 848 707', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 11:50:55.4174627' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'92dad0fc-5865-46f3-8f7d-47cd60582d42', CAST(N'2018-10-18 10:38:56.9043291' AS DateTime2), NULL, NULL, N'Hospital and GP transport – other health transport
Prescription collection
Lunch club
', N'Littleton', N'SO22 6QY', N'd6bec560-9707-46af-9857-dc5029149a86', N'Littleton Village Community Care Group', N'01962 884376', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-18 10:38:57.7711944' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'a4ed1d14-f4d8-4697-b14a-47dba4fcc42e', CAST(N'2018-09-12 20:59:26.1033333' AS DateTime2), NULL, NULL, N'', N'Winchester City', NULL, NULL, N'', N'', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'3f79fe4c-967e-423f-ae82-487420ffbad7', CAST(N'2018-09-12 20:59:26.1966667' AS DateTime2), NULL, NULL, N'Hampshire County Council has strong links with the Armed Forces community in Hampshire. We are working together to make sure that the Armed Forces community, including veterans, have access to the help and support they need.', N'Winchester City', NULL, NULL, N'', N'0300 555 1375 info@hants.gov.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'dfe996a5-67b9-45be-8d38-490c72d4dd12', CAST(N'2018-10-23 14:15:16.8336747' AS DateTime2), NULL, NULL, N'Investigates complaints about gas and electricity companies, phone and internet providers, property companies and copyright issue.
Tel: 0330 440 1614 (communications services)
Tel: 0330 440 1624 (energy companies)
Tel: 0330 440 1634 (property)
Tel: 0330 440 1601 (copyright licensing)
Tel: 0333 300 1620 (complaints to Consumer Ombudsman)
www.ombudsman-services.org', NULL, NULL, N'0a0a505f-bbd9-41cd-95eb-64a92e9ac56f', N'Ombudsman Services (Consumer & Complaints)', N'www.ombudsman-services.org', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 14:15:18.4847381' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'34ee64bc-ee74-4b8c-9a82-493e9e69dde4', CAST(N'2018-09-12 20:59:25.8533333' AS DateTime2), NULL, N'Third Tuesday of each month, 11:30am to 1:45pm', N'Cafe for people with dementia and carers. ', N'Littleton Millennium Memorial Hall
The Hall Way
Littleton
Winchester', N'SO22 6QL', N'3a0e0caa-ad01-4a77-b2cb-9424983861ce', N'Winchester Dementia Café (Littleton)', N'07540919402 / 01256 363393', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-08 08:28:07.6907243' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'e5cd009b-151f-4f51-b064-4cd785048b7a', CAST(N'2018-09-12 20:59:26.2600000' AS DateTime2), NULL, NULL, N'Local hall offering various groups & activities for people of all ages.#rtn##rtn#The Millennium Memorial Hall at Littleton offers various groups, clubs & activities including: Flower Arranging Club, WI, Gardening Club, Yoga, Bridge, Art Club, Festival Choir, Probus Club, U3A, Photographic Society, Pilates & Alzheimers Memory Cafe. #rtn##rtn#Please see website for full listings.', N'Winchester City', NULL, NULL, N'Groups and Activities', N'01962 888419 littletonhall@hotmail.co.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'eb170010-001c-4035-a0a3-4e3ae543d589', CAST(N'2018-10-22 11:07:13.2557062' AS DateTime2), NULL, NULL, N'Network of health walking schemes across the country helping people to live a more active lifestyle & improve mental and physical wellbeing.

Find a health walk scheme near you.', NULL, NULL, N'f283bad8-baca-4260-a862-d23a2e44d58f', N'Ramblers Walking Group', N'01962 853640', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 11:07:13.2600586' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'816f6afd-2181-4da3-80ac-4fc8672b7618', CAST(N'2018-10-24 13:38:40.0248337' AS DateTime2), NULL, NULL, N'Provides information, support and local services to people affected by stroke.', NULL, NULL, N'228019cd-dad5-436b-8cf6-8e499101ba58', N'Stroke Advice & Support', N' 0303 303 3100 helpline@stroke.org.uk ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 13:38:40.6354241' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'9d3a3c04-bc6d-4943-8bce-507618d65975', CAST(N'2018-10-23 08:15:37.5160523' AS DateTime2), NULL, NULL, N'A new service available to help ensure isolated older people have regular social contact through volunteer visits. ', N'Second Floor, 18  St George''s House, Winchester
', N'SO23 8BG  ', N'5788fcec-2508-4793-8a15-ebbc47a0bb30', N'Age Concern Day Centres ', N'0800 328 7154', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 08:20:28.6891533' AS DateTime2), 1, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'808365e3-3c6f-4a6a-bd59-5407e490788b', CAST(N'2018-10-17 11:29:33.3234319' AS DateTime2), NULL, N'Thursdays 2pm - 3pm', N'A seated class to help build and maintain muscle strength and balance in a fun and safe environment. In association with Winchester City Council.', N'River Park Leisure Centre
Gordon Road
Winchester', N'SO23 7DD', N'20386a91-5cab-42a3-9bd8-3153d82cdcef', N'Forever Active Dementia Friendly Class', N'01962 848 707', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 11:29:33.3286244' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'4ba18a6f-f804-4458-adc7-55a78a7c44eb', CAST(N'2018-10-24 14:30:55.0681423' AS DateTime2), NULL, NULL, N'Provides information on how to arrange a non-religious funeral, including how to find someone to conduct it (AKA a celebrant).
', NULL, NULL, N'de39aeb5-7133-4801-bc10-ac999c1085cd', N'Non-Religious Funeral Advice', N' 01480 861 411 info@iocf.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 14:30:55.5758227' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'c6955c85-4552-4115-baaf-56761d961c25', CAST(N'2018-10-17 09:26:36.5757818' AS DateTime2), NULL, NULL, N'Operates across Eastleigh, Fareham, Havant, Gosport and Winchester. The agency helps people to remain in their own home.', NULL, NULL, N'770aa7d1-3ed4-4a12-8068-1608b30d9cdd', N'Home Improvement Agency', N'02380 462444', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 09:26:37.1285076' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'c030910f-034d-407b-9d77-567c9cb1449b', CAST(N'2018-10-24 13:58:39.8747534' AS DateTime2), NULL, N'Monday - Friday 9:30am - 5pm', N'Offers information, advice and support to bereaved people through a telephone helpline and face-to-face support (depending on location - the nearest branches are in Andover and Basingstoke).
', NULL, NULL, N'06b343a1-4ba7-41b3-a71f-f267cf10b553', N'Cruse Bereavement Helpline & Support', N'0808 808 1677', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 13:58:40.8658731' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'd705b896-d3de-4bf7-9e1b-568f6407aaed', CAST(N'2018-09-12 20:59:26.4300000' AS DateTime2), NULL, NULL, N'The Salvation Army is a Christian church and charity providing various types of social support, befriending, information & advice through numerous groups, activities & services.', N'Parchment Street
Winchester
', N'SO23 8AZ', N'153164a8-3dac-4bbf-aa34-23d1299e2d05', N'The Salvation Army Clubs', N'020 7367 4500 info@salvationarmy.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-08 09:21:50.0081509' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'77b6c435-7602-432c-a9ef-58757e729fa3', CAST(N'2018-09-12 20:59:26.2133333' AS DateTime2), NULL, NULL, N'Colden Common Parish Council - listings and contact details for social groups, lunch clubs and activities for adults in the Colden Common area.#rtn##rtn#There is also a local mini bus service. ', N'Winchester City', NULL, NULL, N'Adult Social Groups', N'01962 713700 clerk@coldencommon-pc.gov.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'10110f81-ac6a-4834-8040-5943918ca21f', CAST(N'2018-10-23 10:42:40.7306243' AS DateTime2), NULL, NULL, N'AdviceUK offers a professional insurance brokering service to member organisations, and to other advice providers that meet certain standards. ', NULL, NULL, N'f94acd69-ce18-4a87-bc3d-634c68ea7c06', N'AdviceUK Insurance Help', N'0300 777 0106 insurance@adviceuk.org.uk ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 10:43:05.2782624' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'8fca5514-5872-4258-aa60-5afee6b42094', CAST(N'2018-10-23 14:19:19.6816234' AS DateTime2), NULL, N'24/7', N'A free online tool run in conjunction with Money Saving Expert, which assists you to make complaints to energy, finance, legal, leisure, motoring, retail, food, travel, telecoms, water and other firms. No telephone support but a form can be submitted online.
', NULL, NULL, N'502af31b-d09e-4a1d-98fe-c75caaa753d9', N'Resolver Free Help Service', N'www.resolver.co.uk ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 14:19:20.9938063' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'cfbf2cc1-4452-4371-bcb4-5bc3c3373a5a', CAST(N'2018-09-12 20:59:26.4166667' AS DateTime2), CAST(N'2018-10-17 10:24:39.8337015' AS DateTime2), NULL, N'Strength and balance classes for older people to aid balance and minimise the risk of falls.#rtn##rtn#Steady and Strong strength and balance classes are suitable for older people who are unsteady and will help them to carry on their normal daily routines and improve their balance. Balance exercises can help with falls prevention. #rtn##rtn#Classes are available at various locations across the county - please see website to find one near you.', N'Winchester City', NULL, NULL, N'', N'', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'2a83052b-7b67-4489-be6f-5d1cce048de2', CAST(N'2018-10-26 09:21:38.6665112' AS DateTime2), NULL, N'Alternate Fridays 2pm - 4pm (Closed in August)', N'Come and join in for a varied programme of entertainers, guest speakers, seated exercise and outings. £4 cost.
Call for transport options.', N'Weeke Community Centre, Weeke Community Association, Taplings Road, Winchester', N'SO22 6HG', N'02ed9535-9785-42cc-b57e-eaf40052acbf', N'Weeke Friendship Club', N'01962 857099', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-26 09:21:39.6656858' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'ea92d77d-dfed-4caf-94d6-5df170e41002', CAST(N'2018-09-12 20:59:25.9000000' AS DateTime2), NULL, NULL, N'Wednesdays 1.30pm - 2.30pm', N'Badger Farm, Winchester City', NULL, NULL, N'Zumba', N'07724136428 Debbie Woodley', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'365bf148-8d62-4cc3-badc-5ec7713628d5', CAST(N'2018-10-18 10:40:38.6181888' AS DateTime2), NULL, NULL, N'Hospital and GP transport – other health transport
Lunch club
Other – coffee morning', N'Swanmore', N'SO32 2PA', N'd6bec560-9707-46af-9857-dc5029149a86', N'Swanmore Voluntary Care Group', N'01489 893270 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-18 10:40:38.6227144' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'2627b729-d799-430a-9080-5ef32bd47bfa', CAST(N'2018-10-23 14:48:53.0651335' AS DateTime2), NULL, NULL, N'National body for home improvement agencies and handy person schemes in England. Home improvement agencies provide support for vulnerable homeowners and tenants wanting to carry out adaptations, repairs and home improvements.', NULL, NULL, N'3f1d2481-aa5c-4cec-be3a-ab26b081ce17', N'Foundations Home Improvement/Handy Person Scheme', N'0300 124 0315 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 14:48:53.0835846' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'33595d96-863c-4f65-9717-5f9e1c6edc70', CAST(N'2018-09-12 20:59:25.8700000' AS DateTime2), NULL, NULL, N'2nd Wednesday of the month, 7.30 - 9.30pm', N'Badger Farm, Winchester City', NULL, NULL, N'Hampshire Wildlife Trust', N'01980 862285 Lorraine Martin', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'8412c763-4d18-42f2-a413-60e3da8772a5', CAST(N'2018-10-23 10:40:23.1669326' AS DateTime2), NULL, NULL, N'viceUK is a registered charity supporting the UK’s largest network of independent advice services. The charity operates as a co-ordination and support network for organisations providing independent social welfare law advice. There are currently over 700 members and AdviceUK provides services to a further 500 organisations and individuals.', NULL, NULL, N'f94acd69-ce18-4a87-bc3d-634c68ea7c06', N'AdviceUK National Helpline ', N'0300 777 0107', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 10:40:23.1716795' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'2d10fb70-edb4-4a37-ace0-6441e75febd6', CAST(N'2018-10-24 11:48:54.5113217' AS DateTime2), NULL, N'Monday - Friday 9:30am - 5pm', N'National healthcare charity that gives patients the opportunity to raise concerns and share experiences.
 ', NULL, NULL, N'30baf5a2-2a6a-4038-a429-28e397ce5e10', N'The Patients Association ', N'020 8423 8999 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:48:55.4293994' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'bc4f4c03-e6c7-49dd-8abb-657f708a8229', CAST(N'2018-10-23 14:50:00.1562051' AS DateTime2), NULL, NULL, N'Helps to resolve complaints about social landlords (local authorities and housing associations) and private landlords who are members of the scheme in England. ', NULL, NULL, N'0a0a505f-bbd9-41cd-95eb-64a92e9ac56f', N'Housing Ombudsman Service (HOS)', N' 0300 111 3000 www.housing-ombudsman.org.uk ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 14:50:00.1606940' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'0d2dfbfe-38e9-430e-922b-665aa677f5b0', CAST(N'2018-10-17 10:23:08.5744592' AS DateTime2), NULL, N'Tuesdays 10:15am and 11:30am', N'Community exercise class aimed at improving stability and strength to help prevent falls in older people. Specifically aimed at older people who are having trouble with their balance and walking. Call to book a place. Two classes are run per week.', N'St John''s Community Room
Colebrook Street
Winchester', N'SO23 9LR', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Steady and Strong Class', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 10:23:09.1722261' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'7cc1601c-edf5-4d8f-8a66-6901575f33e0', CAST(N'2018-09-12 20:59:25.9633333' AS DateTime2), NULL, NULL, N'Personal care, social support domestic care, respite care. Provides a friendly and professional service.', N'Winchester City', NULL, NULL, N'Home Care', N'01962 761461 infor@homecarefinder.co.uk www.homecarefinder.co.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'912fde1a-c9dd-4da7-a11f-6a4785d17399', CAST(N'2018-09-12 20:59:25.7733333' AS DateTime2), NULL, NULL, N'Adult contemporary choir formed in 2009. You don''t have to be able to sing, just keen to learn. Wednesday evenings at Springvale Hall at 7:45pm', N'Kings Worthy', NULL, N'f575185a-948d-4de0-9707-3b400ff4d721', N'Choir', N'Camilla 07545 960542', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'57f8717e-294c-4beb-a01a-6b1b0346863d', CAST(N'2018-09-12 20:59:25.8833333' AS DateTime2), NULL, NULL, N'2nd Saturday of every month, 10am - 12.30pm', N'Badger Farm, Winchester City', NULL, NULL, N'National Federation of Spiritual Healers', N'01980 862285 Doreen Rivet', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'3aaa0e03-0b00-4c1d-9f79-6b85bfc91961', CAST(N'2018-09-12 20:59:26.1500000' AS DateTime2), NULL, N'Alternate Fridays 1pm - 2:30pm', N'Alternate Friday''s at Craft and Coffee 1pm - 2.30pm', N'The United Church, Jewry Street, Winchester', N'SO23 8RZ', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'WLAH New Age Kurling', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:11:54.3067373' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'583d634b-3d63-45dd-abff-6bf404580020', CAST(N'2018-09-12 20:59:26.0866667' AS DateTime2), NULL, NULL, N'Monthly afternoon tea parties for small groups of older people, aged 75 and over, who live alone and have little contact with friends and family. Free of charge, car transport included.', N'Winchester City', NULL, NULL, N'Tea parties with friends', N'0800 716543 (freephone)', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'180a0048-ff39-487c-8763-6d7af3e83cd6', CAST(N'2018-09-12 20:59:26.3066667' AS DateTime2), NULL, NULL, N'Local venue for various local groups, clubs & activities including: Table Tennis, Bridge, Ladies Keep Fit, Bowls, Craft Group, Patchwork & Quilting, Senior Club, Book Club, WI & Gardening Club. #rtn##rtn#Please see website for full listings. ', N'Winchester City', NULL, NULL, N'', N'01962 881796 email - jds@greenbee.net', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'437bda94-eef3-4b38-a861-6f0a59d40d57', CAST(N'2018-10-23 14:56:34.7575104' AS DateTime2), NULL, NULL, N'Provides information, advice and advocacy to people with housing problems.
', NULL, NULL, N'da50147f-a4e6-4d8e-8232-b38dab5490d2', N'Shelter Housing and Homelessness Support', N'0808 800 4444 www.england.shelter.org.uk ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 14:56:34.7620988' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'4ffedd5a-7d16-43ea-b4a1-6f9e98f3e8d2', CAST(N'2018-10-23 15:08:22.3191896' AS DateTime2), NULL, NULL, N'Provides advice and information for older people, their families and carers.  Telephone support calls or friendship visits from volunteers can be arranged, or an individual can make contact on their helpline. Referrals can also be made to Independent Age by a relative, friend or third party.', NULL, NULL, N'90b536d1-20e5-49da-8f40-39a2f504d8e9', N'Independent Age Friendship Service and Support', N' 0800 319 6789 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 15:08:22.9722787' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'636f6f93-be8f-48e6-b421-70c731fa5c3d', CAST(N'2018-09-12 20:59:25.8366667' AS DateTime2), NULL, N'First Monday, 2 - 3:30pm', N'First Monday of each month from 2 - 3.30pm at Badger Farm Community Centre, Badger Farm Road, Winchester SO22 4QB', N'Badger Farm Community Centre, Badger Farm Road, Winchester ', N'SO22 4QB', N'3a0e0caa-ad01-4a77-b2cb-9424983861ce', N'Winchester Dementia Café', N'07540919402 / 01256 363393', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-08 08:32:50.0163722' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'ee9bad14-9b30-4f72-bc00-72304ae1e845', CAST(N'2018-10-23 14:26:05.7108556' AS DateTime2), NULL, NULL, N'A charity offering advice and support to victims of anti-social behaviour (ASB).
', NULL, NULL, N'2ef7b465-9715-4cca-90ac-648bef33ae59', N'Anti-Social Behaviour Advice & Support', N'www.asbhelp.co.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 14:26:05.7155073' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'a1117186-be05-4780-ab43-729690e35e5f', CAST(N'2018-10-17 11:54:40.0215602' AS DateTime2), NULL, N'Tuesdays 10am - 11am', N'£3.15 per person', N'River Park Leisure Centre
Gordon Road
Winchester', N'SO23 7DD', N'20386a91-5cab-42a3-9bd8-3153d82cdcef', N'River Park Table Tennis Class', N'01962 848 707', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 11:54:40.0264550' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'dfcd64dc-5bbd-48b5-bcd1-75393457c586', CAST(N'2018-09-12 20:59:25.9300000' AS DateTime2), NULL, NULL, N'Runs every second Wednesday of the month from 10.30am - 2.00 pm. We transform the church into a restaurant and fill it with people for a relaxed, fun time together. There''s space tea and a chat beforehand, or join us at midweek communion from 11.30 - 12.15. This is not just for regular church attendees but is open to anyone who is retired and would like a delicious, hot meal and some great company. Cost is £2.', N'Winchester City', NULL, NULL, N'Christ Chruch Lunch Club', N'brian.wakelin@ccwinch.org.uk ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'f131bd78-a500-43bf-ac11-76a6c5c8db4e', CAST(N'2018-09-12 20:59:25.6966667' AS DateTime2), NULL, NULL, N'Support group for amputees and their families in the Winchester area', N'Winchester area', NULL, N'b6b1f000-24bd-4a97-9795-b6012af6990e', N'Tea club for amputees', N'See organisation', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'b8cf3a9f-9bc5-4692-922c-776a7c91c128', CAST(N'2018-10-18 10:45:22.4081437' AS DateTime2), NULL, NULL, N'Hospital and GP transport – other health transport
Prescription collection
D.I.Y
Shopping 
Befriending
', N'Winchester City', N'SO23 9LS', N'd6bec560-9707-46af-9857-dc5029149a86', N'Winchester Good Neighbours', N'0845 094 8946 winchestergoodneighbours@outlook.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-18 10:45:22.4126622' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'b0cc327a-4088-4676-be95-77b0dc01f648', CAST(N'2018-09-12 20:59:26.2300000' AS DateTime2), NULL, NULL, N'List of extra care housing, sheltered housing, retirement and age exclusive housing in the Winchester area.#rtn##rtn#Details are provided by the Elderly Accommodation Counsel, a charity who provide information and advice to older people. ', N'Winchester City', NULL, NULL, N'Housing Support', N'', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'cc0989c9-448d-4efb-8b4d-7965068e14ab', CAST(N'2018-10-24 11:42:33.3965341' AS DateTime2), NULL, NULL, N'Provides information and support to people with Parkinson’s and their families through a helpline staffed by nurses, with a network of local support groups. ', NULL, NULL, N'98009f8c-4d78-4c15-ae8d-4d20f50f5ddb', N'Parkinson''s Support & Advice', N'0808 800 0303 hello@parkinsons.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:42:33.4013385' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'c5c1b348-0282-474e-a2ff-7acf236c52d7', CAST(N'2018-10-22 11:00:50.2493993' AS DateTime2), NULL, NULL, N'Weeke Community Centre offers a range of clubs and activities catering for all groups and ages in the North Winchester area.

This includes: Bridge, Rock Choir, Senior Line Dancing, Painting, Keep Fit, Weight Management, Brendon Care and Upholstery.

The centre also has a Social Club open to all Association Members offering additional activities and social events. ', N'Weeke Community Centre 
Taplings Road 
Weeke 
Winchester 
', N'SO22 6HG ', N'ea55afce-d9fd-4bc6-8913-ac2b4bf4041d', N'Weeke Community Clubs', N'01962 888547', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 11:00:50.2574821' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'bb5a5106-366f-4d48-825e-7b955e0ddbef', CAST(N'2018-10-26 10:08:07.0293736' AS DateTime2), NULL, N'Monday - Thursday 8:30am - 5pm, Friday 8:30am - 4:30pm ', N'Hampshire Fire and Rescue Service provides useful information on how to stay safe at home, on the road, at leisure and staying well in the winter months. Hampshire Fire and Rescue Services’ Fire as a Health Asset team is responsible for overseeing and delivering: prevention initiatives in the health arena; the implementation of medical response;
the co-responding partnership with South Central Ambulance Service (SCAS).', N'Hampshire Fire and Rescue Services Headquarters, Leigh Road, Eastleigh', N'SO50 9SJ', N'ffb5d36c-5f23-4111-8db4-84dfc68c48d4', N'Hampshire Fire and Rescue - Fire as a Health Asset', N'023 8064 4000', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-26 10:08:07.4487985' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'7fc9ac21-da35-4049-951d-7cb97ed054a4', CAST(N'2018-09-12 20:59:26.2900000' AS DateTime2), NULL, NULL, N'Service that provides a wide selection of manual and powered wheelchairs and battery powered mobility scooters, any of which can be hired for a small fee. #rtn##rtn#We can meet you from your car, taxi or Dial-a-Ride, and we will show you how to operate the equipment safely and you can have a practice before going out into the town.#rtn##rtn#Equipment from Shopmobility Winchester may be hired for a day or up to 2 weeks, details are available on application.', N'Winchester City', NULL, NULL, N'', N'01962 842626', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'1c7fab34-aeca-46c8-8cce-7d6805810c6d', CAST(N'2018-09-12 20:59:25.9000000' AS DateTime2), NULL, NULL, N'Monday and Wednesdays 6.30 - 9pm', N'Badger Farm, Winchester City', NULL, NULL, N'Winchester Tae Kwon-Do', N'info@winchestertkd.co.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'975df969-e173-472d-a341-7e90310de5cd', CAST(N'2018-09-12 20:59:26.2733333' AS DateTime2), NULL, NULL, N'Charity who provide care and support for people living with any terminal illness, and their families.#rtn##rtn#Marie Curie offer expert care, guidance and support to help them get the most from the time they have left.', N'Nationwide', NULL, NULL, N'', N'0800 090 2309 supporter.relations@mariecurie.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'95f2f3a1-b675-444e-9994-82cbe58be98f', CAST(N'2018-09-12 20:59:25.9933333' AS DateTime2), NULL, NULL, N'Support Group for those who have lost another through suicide.', N'Winchester City', NULL, NULL, N'Support Group', N'Paul on 07889009393 sobshampshire@gmail.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'36cd342c-94ce-414b-bd65-869c7b232d07', CAST(N'2018-10-24 14:28:11.2057293' AS DateTime2), NULL, NULL, N'Dying Matters has numerous resources for members who want to help raise awareness and promote conversation about death, dying and bereavement. It aims to change the national perception of dying.', NULL, NULL, N'f35e3d70-2561-44d1-8da9-48ff811845fc', N'Dying Matters Advice & Support', N'www.dyingmatters.org ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 14:28:11.3503073' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'26e74aea-0698-4770-9958-86cdd0e5261a', CAST(N'2018-10-22 10:37:23.5000424' AS DateTime2), NULL, N'Monday 10am - 11:30am', N'Held every Monday, 10:00 - 11:30 & 11:45 - 13:15, half an hour for chat and coffee before an hour of singing', N'The United Church, Jewry Street, Winchester', N'SO23 8RZ', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Singing for Wellbeing: Class One', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:37:24.2139993' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'c6b548b9-04fe-493f-8c94-86d201d1a2b7', CAST(N'2018-10-24 15:07:36.3323079' AS DateTime2), NULL, NULL, N'Provides information and guidance on how to make gardening easier and more enjoyable in older age.', NULL, NULL, N'7fb27ae9-640b-485c-92f7-71d5fa85e491', N'Gardening Guidance & Advice', N' 0118 988 5688 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 15:07:37.2974311' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'0952f7c8-6cc0-4b6c-b64f-875ed5d8ab1c', CAST(N'2018-10-23 11:08:37.1016873' AS DateTime2), NULL, NULL, N'Provides guidance on pension pot options if you have a defined contribution pension.
Call to book an appointment for advice.
www.pensionwise.gov.uk ', NULL, NULL, N'a4826f67-c8ef-4973-9a80-8977e6c42e77', N'Pension Wise National', N'0800 138 3944 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:08:37.1062246' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'f864e2b6-39e0-4f13-a62b-88c29ba479b0', CAST(N'2018-10-23 14:31:17.5267565' AS DateTime2), NULL, N'Monday - Saturday 8am - 8pm', N'Provides free, confidential support to victims of crime and their friends and family.
', NULL, NULL, N'a3a14c6e-eb1f-4aa8-b9eb-10b0f9bea7a1', N'Victim Support Helpline', N' 0808 168 9111 www.victimsupport.org.uk  ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 14:31:17.5311553' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'1c58cc2a-04c8-4f26-a0c7-89c0c930c9bc', CAST(N'2018-09-12 20:59:25.7433333' AS DateTime2), NULL, NULL, N'3rd Friday of the month at 10:00 am. All welcome.', N'Kings Worthy', NULL, N'9ec6f994-dfa3-4da2-923d-e5d34a7ca7a3', N'Scrabble', N'See organisation', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'd2337f94-9b2f-416a-9ddb-8ae31e5d7447', CAST(N'2018-09-12 20:59:26.1200000' AS DateTime2), NULL, NULL, N'Mondays 11am-12.30pm, two sessions held every Monday with tea and coffee and chance to chat inbetween sessions. Particularly aimed at those with memory problems, Parkinson''s, depression and those that look after them.', N'Winchester City', NULL, N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Singing for Wellbeing', N'', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'9dd2b25b-0a01-4776-9ce4-8c24ac538ea6', CAST(N'2018-09-12 20:59:26.4300000' AS DateTime2), NULL, NULL, N'Weekly lunch club for elderly members of the parish.#rtn##rtn#The Swanmore Voluntary Lunch Club aims to provide a two course lunch with tea and coffee on a weekly basis. The club offers a varied menu, volunteers to help at table and chat as well as a raffle. Transport can be provided to and from houses if required. ', N'Winchester City', NULL, NULL, N'', N'01489 892911 email - dennis.123.wheeler@gmail.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'968b60a3-e513-41be-8da9-8db6a2362999', CAST(N'2018-10-17 14:35:44.3845906' AS DateTime2), NULL, N'Mondays 10:30am - 1pm', N' The Monday Friendship Group caters mostly for the older generation, although all ages are very welcome. The group meets to enjoy each others company, have fun, meet with new friends, go on outings, worship together with lively discussion.  Lunch is served at 12pm.', N'Hope Church Winchester
The Middle Brook Centre
Middle Brook Street
Winchester
Hampshire', N'SO23 8DQ', N'b7c1e612-53a6-447a-87ef-8068b8257c95', N'The Monday Friendship Club', N'01962 840800', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 14:35:44.6028749' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'998ff2f4-12fe-490d-9b88-8ec3596c7c3a', CAST(N'2018-09-12 20:59:26.0566667' AS DateTime2), NULL, NULL, N'Social afternoons, coach outings etc for members. St Larence Parish Room, Colebrook Street on alternate Fridays 2-4pm', N'Winchester City', NULL, NULL, N'Social Club', N'Mr N Alexander on 01962 865585', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'6ad88269-4ffc-4dbc-a5a3-9103ed926504', CAST(N'2018-09-12 20:59:25.8200000' AS DateTime2), CAST(N'2018-10-23 08:45:48.2477322' AS DateTime2), NULL, N'Mondays at St. Barnabas Church, Weeke. Tuesdays & Thursdays at the Baptist Hall, Swan Lane, Winchester. Wednesdays at Salvation Army Hall, Parchment St, Winchester. Use own transport or minibus (if you live within a 3 mile radius of the city centre). The cost for minibus transport is £1.30. ', N'Winchester City', NULL, N'79ca50fe-d393-4c66-bd59-2a6f685280e2', N'Computer & internet training', N'Lunch club coordinator Cynthia Boswell. 01962 871 725/ 07984 099097  lunchclub@ageukmidhampshire.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'af876821-7ff5-41cd-b652-93408caf3e25', CAST(N'2018-09-12 20:59:26.4500000' AS DateTime2), NULL, NULL, N'Details of Walking Football clubs across Hamphire.#rtn##rtn#Walking Football is a variant of association football that is aimed at keeping people aged over 50 involved with football if, due to a lack of mobility or for other reason, they are not able to play the traditional game.#rtn##rtn##rtn#The main differences between walking football and the standard game are two key rules: No running and no slide tackling. With plenty of breaks, participants enjoy a safe and enjoyable return to football. ', N'Winchester City', NULL, NULL, N'', N'walkingfootballunited@hotmail.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'f12d40b8-e83d-4e52-ae74-9341164dac7c', CAST(N'2018-10-24 13:07:59.3161018' AS DateTime2), NULL, N'12:00pm - 10.00pm', N'Provides confidential, emotional support 24 hours a day, seven days a week by phone. The local office can be visited.', N'13 Upper High Street
Winchester
Hampshire
', N'SO23 8UT', N'96ee7c0e-c78c-47f0-90f2-f3c525668f17', N'Samaritans Drop-In Support', N'116 123', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 13:07:59.3245531' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'cafcb554-32bc-4101-af77-943b162a6709', CAST(N'2018-09-12 20:59:26.4633333' AS DateTime2), NULL, NULL, N'Outreach project which seeks to serve the spiritual and social needs of seniors from our own and other churches,  who can no longer easily attend any services. The main meetings are: Silver Service (first Thursday in the month), Reflections discipleship group, Songs of Praise afternoon (twice a year), services in local residential homes and the very popular Holiday at Home. The Winchester Baptist Church - Mission to Seniors team are always on hand to offer support, prayer, and a friendly listening ear whenever folk express a need.', N'Winchester Baptist Church
Swan Lane
Winchester
', N'SO23 7AA', N'f8089372-5a5d-4f22-b937-1f3ccde4dd4b', N'Winchester Baptist Church - Mission to Seniors', N'01962 868770 missiontoseniors@winbap.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:58:50.8250233' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'b6a294f4-c750-4d0f-bcb8-946aa98b8cc7', CAST(N'2018-09-12 20:59:26.1800000' AS DateTime2), NULL, NULL, N'AbilityNet is a national charity which helps people of any age and with any disability to use technology to achieve their goals at home, at work and in education.', N'Winchester City', NULL, NULL, N'', N'0800 269 545 enquiries@abilitynet.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'37fd0884-a6af-41cf-9817-9587cdda4a48', CAST(N'2018-09-12 20:59:25.8366667' AS DateTime2), NULL, NULL, N'For people with dementia and their carers. 3rd Tuesday of each month, 10.30am to 12.30pm at Littleton Millenium Memorial Hall, The Hall Way, Littleton, Winchester.', N'Littleton', NULL, NULL, N'Winchester Dementia Café', N'01962 865585 (Sheila Ancient, Support Worker)', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'f967d89a-73a5-40b5-b05e-97e7ed7ee3a2', CAST(N'2018-09-12 20:59:25.8700000' AS DateTime2), NULL, NULL, N'Fridays 7.15 - 10.30pm.', N'Badger Farm, Winchester City', NULL, NULL, N'Bridge Club', N'02380 251277 Roland Richardson', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'd1019175-0918-4877-a18a-98482baf0c79', CAST(N'2018-09-12 20:59:26.0100000' AS DateTime2), NULL, NULL, N'The first Monday or each month. 12.30pm - 2pm at Friends meeting house, 16 Colebrook Street, Winchester.', N'Winchester City', NULL, NULL, N'Social Group', N'', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'9200d771-7eec-4052-b6c1-9a7f01a4b704', CAST(N'2018-10-23 11:04:50.2850765' AS DateTime2), NULL, N'Monday - Friday 8am - 6pm', N'Provides details of the State Pension, including pension statements and how to claim your pension.
', NULL, NULL, N'a4826f67-c8ef-4973-9a80-8977e6c42e77', N'State Pension Service', N'0800 731 7898', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:04:50.2897551' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'd8eee229-78a7-4c25-b482-9ac2d196ac75', CAST(N'2018-09-12 20:59:25.8200000' AS DateTime2), CAST(N'2018-10-23 08:50:00.4261828' AS DateTime2), NULL, N'Fridays at Winnall Community Centre, Winnall.', N'Winchester City', NULL, N'79ca50fe-d393-4c66-bd59-2a6f685280e2', N'Cinema', N'Lunch club coordinator Cynthia Boswell. 01962 871 725/ 07984 099097  lunchclub@ageukmidhampshire.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'b09e8b09-1516-4771-9646-9c598abc3426', CAST(N'2018-10-22 09:24:12.1405499' AS DateTime2), NULL, NULL, N'Free support and advice - the company was set up by a GP who recognised that families need more support in making decisions about care. They work across 60 hospitals nationwide and can support families to make decisions about care homes and packages to help people remain in their homes for as long as possible.', NULL, NULL, N'5e4ab376-e494-4e1c-9cda-0053c72be9a9', N'CHS Care in Your Own Home', N'0121 362 8840', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 09:24:13.0280039' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'3d2d9b6e-6cb9-46b1-b6b9-9c6d15572e5a', CAST(N'2018-09-12 20:59:25.7900000' AS DateTime2), NULL, NULL, N'The group is held on the 1st and 3rd Tuesday of the month. It is open to people with dementia and their carers. ', N'Stanmore, Winchester', NULL, N'6ee5b521-ce7e-456c-90d3-5ab1274a8eb0', N'Afternoon Tea', N'See organisation', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'05e5aed8-4368-4630-826b-9cb5f59e9476', CAST(N'2018-09-12 20:59:26.1500000' AS DateTime2), NULL, N'Alternate Fridays, 11am - 1:30pm', N'Two course home cooked meal and a programme of varied speakers and entertainment.', N'The United Church, Jewry Street
Winchester', N'SO23 8RZ', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'WLAH Friday Lunch Club', N'01962 890995 Winchester.LiveAtHome@mha.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 09:48:22.9974273' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'03f9c025-5c07-4704-9beb-9e0ea3288350', CAST(N'2018-09-12 20:59:26.0100000' AS DateTime2), NULL, NULL, N'Winchester city centre, Winnall, Teg Down, Olivers Battery, Weeke, Badgers Farm, Hospital Transport, GP Transport, Other Health Transport, prescription collection, D.I.Y, shopping, befriending.', N'Winchester City', NULL, NULL, N'Good Neighbours', N'0845 094 8946 winchestergoodnighbours@outlook.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'84dd534b-1bf4-449f-9403-9e7f03db781f', CAST(N'2018-09-12 20:59:26.2900000' AS DateTime2), NULL, NULL, N'One to one or group flower arranging activities, particularly aimed at people with dementia.#rtn##rtn#We specialise in sessions for people with dementia and currently hold the Blooming Cafe at the Tubb''s Hall in King''s Worthy. Here you can come for coffee, cake and a chat while making your own flower arrangement. Places are limited so please contact us to reserve your place.', N'Winchester City', NULL, NULL, N'', N'07958 138481 ztraceyburner@gmail.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'02497cea-c2dd-40a3-94e1-9fcd5c6589c4', CAST(N'2018-10-22 10:50:53.6219058' AS DateTime2), NULL, N'Monthly', N'Monthly lectures by gardening experts, garden visits, coach outings and social events. Monthly meetings held at the Winchester Club, Churchill Room, Worthy Lane at 7.30pm. Annual subscription £10.', N'Winchester Club, Churchill Room, Worthy Lane', N'SO23 7AB', N'983945f7-2757-47a9-9e50-f54e095cd762', N'Horticultural Society', N'01962 865557 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:50:53.6266702' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'65ffa717-7a87-4c4f-a7fa-a16230642b12', CAST(N'2018-10-24 11:37:52.3665287' AS DateTime2), NULL, N'24/7', N'NHS 111 is a 24-hour helpline in England for advice on urgent but non-life threatening symptoms. If worried about an urgent medical concern, you can call 111 to speak to a fully trained adviser. Depending on the situation, the NHS 111 team can connect you to a nurse, emergency dentist or even a GP, and can arrange face-to-face appointments if they think you need one. NHS 111 advisers can also assess if you need an ambulance and send one immediately if necessary.', NULL, NULL, N'348a9d72-a67d-44ee-82e0-74a9fb55015a', N'NHS 111 Helpline Service', N'111', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:37:52.3711768' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'9fac7f50-1bbd-425d-8346-a182361d164a', CAST(N'2018-09-12 20:59:25.9933333' AS DateTime2), NULL, NULL, N'', N'Winchester City', NULL, NULL, N'Nordic Walking', N'0845 260 9339 info@nordicwalking.co.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'ccc9e3c8-5811-4fcb-aa90-a26615d7c051', CAST(N'2018-10-23 11:23:25.9410675' AS DateTime2), NULL, N'Monday - Friday 9am - 5pm', N'Offers independent advice from qualified tax advisers for people in later life on a low income.

', NULL, NULL, N'8b8e9b3e-4e16-46bb-9222-a1af6eaf4297', N'Tax Help for Older People', N' 0845 601 3321 or 01308 488066', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:23:25.9454980' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'07b81ab3-ae5f-4f60-b636-a2bc055f08a5', CAST(N'2018-10-17 10:18:41.5933893' AS DateTime2), NULL, N'Monday - Thursday, 10am - 4pm', N'Wessex Cancer Trust''s Cancer Support Centre offers free support to anyone affected by Cancer. They offer a befriending drop in service, counselling and complementary therapies. ', N'91-95 Winchester Road
Chandler''s Ford
Hampshire', N'SO53 2GG', N'37fd957c-58b4-4fe3-bc35-abcc318c399c', N'Health and Wellbeing Cancer Support', NULL, N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 10:18:41.5998414' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'ee543068-4569-473f-8066-a320cabb8f78', CAST(N'2018-10-24 11:35:06.0467700' AS DateTime2), NULL, N'Monday - Friday  9am – 5pm', N'The National Osteoporosis Society offers support and information to people with osteoporosis. ', NULL, NULL, N'fa2d3f94-4812-48fc-93c3-be2edb84a1c5', N'Osteoporosis Support', N'0808 800 0035 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:35:06.0550541' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'5763f055-d460-4e60-be33-a4534281d3a6', CAST(N'2018-10-23 15:04:48.3560098' AS DateTime2), NULL, N'Monday - Friday 9am - 5pm', N'Provides information and training on direct payments and independent living. Includes the RADAR national key scheme, allowing disabled people to access locked public toilets.

www.disabilityrightsuk.org', NULL, NULL, N'6018ee18-f4d6-4747-a05a-ed937b221846', N'Disability Rights UK Advice Service', N'0330 995 0400 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 15:04:48.3606173' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'cdc23c09-f083-4968-976e-a454b5bd7198', CAST(N'2018-10-17 14:50:28.3051647' AS DateTime2), NULL, N'One Saturday a month, 10am - 12pm', N'Bring the craft you are working on: knitting, cross stitch, sewing, card making, jewelry making, patchwork; take part in the taught craft (£2 to cover materials used); or just sit and have a cup of tea and a natter. Once a month.', N'Hope Church Winchester
The Middle Brook Centre
Middle Brook Street
Winchester', N'SO23 8DQ', N'b7c1e612-53a6-447a-87ef-8068b8257c95', N'Face2Face for Women Craft Cafe', N'01962 840 800', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 14:50:28.3099079' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'646d532c-3580-4e38-ab63-a8cb68c92ac2', CAST(N'2018-10-24 13:47:08.2116294' AS DateTime2), NULL, N'Monday - Friday 9:30am - 4:30am', N'Self-help organisation providing peer support and friendship to bereaved parents, grandparents and their families.', NULL, NULL, N'f66c5783-436d-4231-ad7d-ba4b5970f3ef', N'Compassionate Friends Bereavement Support', N'0345 123 2304', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 13:47:09.1383543' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'745b3bc6-2449-4023-bfd5-a8f141715593', CAST(N'2018-10-23 10:51:20.4961416' AS DateTime2), NULL, NULL, N'Government service providing information about claiming disability benefits.
For Disability Living Allowance (DLA) if you were born on or before 8 April 1948:
Tel: 0800 731 0122
Textphone: 0800 731 0317

For Disability Living Allowance (DLA) if you were born after 8 April 1948:
Tel: 0800 121 4600
Textphone: 0800 121 4523 ', NULL, NULL, N'7f00bdd0-e34d-452f-8daa-af67fd9ec41c', N'Disability Service Centre', N'0800 731 0122 / 0800 121 4600', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 10:51:21.0373924' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'1f65dfc1-45be-4c9d-b8c0-aa56b7b6d057', CAST(N'2018-09-12 20:59:26.0100000' AS DateTime2), NULL, NULL, N'A range of activities for people over 50. Tuesdays and Thursdays between 10.00am - 2.00pm at River park Leisure Centre, Winchester. Membership forms from the centre or downloadable.', N'Winchester City', NULL, NULL, N'Fitness Group', N'www.wfsc.hampshire.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'7453340e-dc40-4ece-8fd7-ac36e28ca4ed', CAST(N'2018-10-22 10:47:27.5375873' AS DateTime2), NULL, N'Tuesday 1pm - 2pm', N'A member led creative group.', N'United Church, Jewry Street, Winchester', N'SO23 8RZ', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'WLAH Radio Theatre Group', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:47:27.5919121' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'cce1cc64-7386-44a3-b66c-adb372b42cb0', CAST(N'2018-09-12 20:59:26.0400000' AS DateTime2), NULL, NULL, N'Tuesday 2-3pm at Lawrence Parish Room in Colebrook Street', N'Winchester City', NULL, NULL, N'Golden Age Singers', N'Miss G Pratten 01962 851053', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'38b4eb6f-9ad7-4e3c-a8e6-adf9e3c87db2', CAST(N'2018-10-26 09:13:29.3908738' AS DateTime2), NULL, N'Saturdays 2pm - 4pm', N'Members enjoy a variety of activities, games, guest speakers and other entertainment - the chance to meet new friends. Transport options available - call to arrange.', N'Learning Rooms, Discovery Centre, Jewry Street, Winchester', N'SO23 8SB', N'02ed9535-9785-42cc-b57e-eaf40052acbf', N'Discovery Weekenders Club', N'01962 857099', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-26 09:13:29.3958203' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'a137d7dc-1f8b-4585-a9ea-aedf35616748', CAST(N'2018-09-12 20:59:26.0233333' AS DateTime2), NULL, NULL, N'Monthly lectures by gardening experts, garden visits, coach outings and social events. Monthly meetings held at the Winchester Club, Churchill Room, Worthy Lane at 7.30pm. Annual subscription £10.', N'Winchester City', NULL, NULL, N'Horticultural Society', N'01962 865557 www.winchesterhorticulturalsociety.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'60155f39-5532-41b4-b0d8-aee0acaf77ae', CAST(N'2018-09-12 20:59:26.0700000' AS DateTime2), NULL, NULL, N'Prescription collection, library book collection, visits to the housebound, lightbulb changing, god walking, emergency meals, shopping.', N'Winchester City', NULL, NULL, N'', N'01962 867796', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'3a9761b9-1b89-4aa6-8353-afd44eb1b904', CAST(N'2018-09-12 20:59:25.7900000' AS DateTime2), NULL, N'Fridays', N'Available for those without additional health issues eg diabetes. Available at Avalon House on Fridays. Call to make an appointment.', N'NHS Avalon House 
Chesil St, Winchester ', N'SO23 0HU', N'5788fcec-2508-4793-8a15-ebbc47a0bb30', N'Toe Nail Cutting Service', N'01329 842481', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 08:20:49.4230336' AS DateTime2), 1, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'437157c4-398d-40f7-a7bd-b0bc30ae65b4', CAST(N'2018-10-23 11:49:57.5212481' AS DateTime2), NULL, NULL, N'Free register for people who don’t want to receive unsolicited sales and marketing telephone calls. You can register landlines and mobile phones.
', NULL, NULL, N'c2273a27-08ae-49c6-91ef-e677e5a4bd64', N'Telephone Preference Service (TPS)', N'0345 070 0707', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:49:58.1890108' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'3df5fd8c-e514-459c-9644-b1ab6f02be20', CAST(N'2018-09-12 20:59:26.1200000' AS DateTime2), NULL, N'Alternate Fridays, 10:30am-12:30pm', N'A variety of activities and crafts to take part in or just to have a coffee and a chat.', N'The United Church, Jewry Street, Winchester, Hampshire', N'SO23 8RZ', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Winchester Craft and Coffee Morning', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:17:36.8327837' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'09db61ce-73ed-4b5c-94e0-b27b4656e5f0', CAST(N'2018-09-12 20:59:25.9300000' AS DateTime2), NULL, NULL, N'3rd Monday at 11am during the months of March, June, September and December', N'', NULL, NULL, N'', N'01962 854542 Mr Ian Millar', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'b7c1009c-36f9-44a4-b26d-b3bdadfb2b42', CAST(N'2018-09-12 20:59:25.9466667' AS DateTime2), NULL, NULL, N'4th Thursday of every month at the Holy Trinity Church hall in Upper Brook Street, Winchester', N'Winchester City', NULL, NULL, N'', N'01962 760261 Mrs F Carmen', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'6cd80b6d-5668-47c8-9a49-b44018970df4', CAST(N'2018-09-12 20:59:26.4166667' AS DateTime2), NULL, NULL, N'The Stroke Association is a charity & helpline providing information, advice & support for people affected by stroke. #rtn##rtn#Support groups include Speakability self-help groups run by and for people suffering with Aphasia. #rtn##rtn#Find support in your area.', N'Winchester City', NULL, NULL, N'', N'0303 303 3100 email - info@stroke.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'b9694a45-c3ba-4752-8aa5-b4f1c776aa6c', CAST(N'2018-10-23 11:30:24.5612813' AS DateTime2), NULL, NULL, N'Government-run information line for people in England and Wales who may be eligible for the Warm Home Discount Scheme, because they receive the Guarantee Credit element of Pension Credit and whose energy supplier is part scheme.
', NULL, NULL, N'6f067daf-4f71-48ee-80b1-3a99758f8da7', N'Warm Home Discount Team', N'0800 731 0214', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:30:25.1931288' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'a4ee2331-ef0f-4fdc-b48f-b605fe65db50', CAST(N'2018-10-24 11:26:58.9453262' AS DateTime2), NULL, N'Monday - Friday 9am - 5pm', N'Provides practical, medical and financial support for people living with cancer.
', NULL, NULL, N'd3927ade-1420-480c-b83f-99a207e50900', N'Macmillan Cancer Support Helpline', N'0808 808 0000 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:26:58.9499637' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'e193a15d-1cd7-41ea-b880-b61974789163', CAST(N'2018-10-18 10:43:52.2889666' AS DateTime2), NULL, NULL, N'Hospital and GP transport – other health transport
Social transport
Prescription collection
Gardening
D.I.Y
Shopping
Pet care
Befriending
Visiting 
Other – emergency meals', N'Bramdean, cheriton, Hinton, Ampner, Kilmeston, Tichborne', N'SO24 0LA', N'd6bec560-9707-46af-9857-dc5029149a86', N'Upper Itchen Voluntary Care Group', N'01962 771741', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-18 10:43:52.3046747' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'65833500-ef68-4558-a91c-b70fee1bada7', CAST(N'2018-10-17 14:35:21.1477309' AS DateTime2), CAST(N'2018-10-17 14:37:41.6343977' AS DateTime2), N'Mondays 10:30am - 1pm', N' The Monday Friendship Group caters mostly for the older generation, although all ages are very welcome. The group meets to enjoy each others company, have fun, meet with new friends, go on outings, worship together with lively discussion.  Lunch is served at 12pm.', N'Hope Church Winchester
The Middle Brook Centre
Middle Brook Street
Winchester
Hampshire', N'SO23 8DQ', N'b7c1e612-53a6-447a-87ef-8068b8257c95', N'The Monday Friendship Club', N'01962 840800', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 14:35:21.4439402' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'2c3f3461-42a7-4573-8fe2-b7709c7ca6d9', CAST(N'2018-10-26 10:36:34.5436580' AS DateTime2), NULL, NULL, N'Help for older people living in their own home - their home help services for older people include help taking out the bins, dusting, hoovering, de-cluttering, feeding the cat and even washing and ironing. It covers all aspects of housekeeping to help you to live well in your home environment, so that it remains clean, tidy and safe. 

Their CAREGivers are matched to the individual, and they will take time to get to know how you like to keep your house and make it a happy place for you to live. ', N'Construct House Winchester Road Alresford', N'SO24 9EZ', N'e3f960ee-a32f-4105-b30e-4141e86a64e8', N'Home Help & Housekeeping', N'01962 736681 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-26 10:36:35.2850870' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'a124aa37-0f4e-4352-ad0d-b7e58fe9dbbd', CAST(N'2018-10-22 10:57:39.2472099' AS DateTime2), NULL, NULL, N'Winchester Bereavement Support is a registered charity. It is a voluntary organisation, offering a confidential support service to bereaved people in Winchester and the surrounding area.
Support is given through home visits by carefully selected and trained volunteers. These volunteers are called Bereavement Visitors.
There is no charge for the service.', N'10 Paternoster House
Colebrook Street
Winchester
', N'SO23 9LG', N'7cf76be9-d2f7-428e-98f1-5ca3fbc29f4a', N'Winchester Bereavement Support', N'01962 863626 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 10:57:39.2521022' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'dc6991ff-87c0-49ea-820d-b91d8463a3ed', CAST(N'2018-09-12 20:59:25.8066667' AS DateTime2), NULL, NULL, N'Mondays at St. Barnabas Church, Weeke. Thursdays at the Baptist Hall, Swan Lane, Winchester. Use own transport or minibus (if you live within a 3 mile radius of the city centre). The cost for minibus transport is £1.30. ', N'Winchester City', NULL, N'79ca50fe-d393-4c66-bd59-2a6f685280e2', N'Fit as a Fiddle class', N'Lunch club coordinator Cynthia Boswell. 01962 871 725/ 07984 099097  lunchclub@ageukmidhampshire.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'a52d8c18-a587-4dbd-8b5d-ba77ae54dec1', CAST(N'2018-09-12 20:59:26.0866667' AS DateTime2), NULL, NULL, N'This group in held on ??? Thursdays of every month. It is open to people with dementia and their carers.', N'Winchester City', NULL, NULL, N'Exercise Group', N'Church Number - 01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'b361ee83-10d5-466e-a7ae-bc42a4a926f5', CAST(N'2018-10-24 11:40:00.7719045' AS DateTime2), NULL, NULL, N'This provides information on stopping smoking and details of support. ', NULL, NULL, N'348a9d72-a67d-44ee-82e0-74a9fb55015a', N'NHS Smokefree Service', N' 0300 123 1044 www.nhs.uk/smokefree ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:40:00.7765124' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'd2292f2b-1d61-4bcc-b6c1-bc4761197b74', CAST(N'2018-10-24 11:43:22.4311811' AS DateTime2), NULL, NULL, N'The Ombudsman can investigate complaints after patients have been through the NHS complaints process and haven’t been able to resolve the matter.', NULL, NULL, N'0a0a505f-bbd9-41cd-95eb-64a92e9ac56f', N'Parliamentary & Health Service Ombudsman', N'0345 015 4033 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:43:22.4360682' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'e1376327-2c8f-4b92-9055-bd851b2e7c7e', CAST(N'2018-09-12 20:59:26.2133333' AS DateTime2), NULL, NULL, N'Community centre in Colden Common offering various clubs, activities & groups for all ages and interests, particularly older people.#rtn##rtn#Regular activities include: Fitsteps, Pilates, Bridge Club, Yoga, Lunch Club, Club 55, Badminton, Gardening Club, Singing Group & Good Company Fridays. #rtn##rtn#DisabledGo Access Guide for Colden Common Community Centre', N'Winchester City', NULL, NULL, N'Community Centre', N'01962 713700 clerk@coldencommon-pc.gov.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'cfcc4274-23b3-4c21-bf49-bde3f3288e3d', CAST(N'2018-10-23 14:45:09.9715396' AS DateTime2), NULL, NULL, N'Provides information on care, housing and financial options for older people and their carers through their ‘Housing Options for Older People’ (HOOP) tool.
', NULL, NULL, N'958ec2df-808e-4be6-9d0f-17c46a3e81c3', N'Elderly Accommodation Counsel (EAC) ', N'hoop.eac.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 14:45:10.5567149' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'fef51877-3de2-4493-b38b-c0228502f06c', CAST(N'2018-10-23 11:11:54.6195124' AS DateTime2), NULL, NULL, N'Provides independent information and guidance on different types of pensions.

', NULL, NULL, N'665e9bdc-e88b-4203-939a-44e64bf66ffd', N'Pensions Advisory Service', N'0300 123 3797', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:11:55.1470567' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'8a240c6d-158e-4aea-8977-c054eb6c5b4d', CAST(N'2018-10-17 13:46:32.7854699' AS DateTime2), NULL, NULL, N'Exercise classes in the community, thorough assessment and personalised exercise programmes, assessment and treatment of mobility difficulties and functional skills, identification, trialing and recommendation of specialist equipment, environmental adaptations and assistive technology at home, specialist dementia support, advice on falls reduction and prevention, identifying and enabling access to appropriate community activities - available at home, Hobbs’ team of physiotherapists, occupational therapists, psychologists, speech and language therapists and fitness professionals provide personalised respite and rehabilitation packages for older people who are looking to regain their independence and reduce their care needs.', NULL, NULL, N'9d47ce9b-8c1f-4f31-b4d2-a4778493bb37', N'Hobbs Rehabiliation Older People''s Services', NULL, N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 13:46:32.7908071' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'6455db28-6367-44f6-8117-c0c22b539081', CAST(N'2018-09-12 20:59:25.8833333' AS DateTime2), NULL, NULL, N'3rd Thursday of every month 8 - 10pm', N'Badger Farm, Winchester City', NULL, NULL, N'Royal British Legion', N'01962 881737 Ruth Harrison', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'e5441c81-2aa7-4fd5-bf93-c0fbc679408b', CAST(N'2018-09-12 20:59:25.8833333' AS DateTime2), NULL, NULL, N'Fridays 3pm - 4.30pm', N'Badger Farm, Winchester City', NULL, NULL, N'Tea Dance', N'01962 854542 Ian and Joan Millar', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'354c6d7f-3c15-4687-afcd-c15edc9e609b', CAST(N'2018-10-22 11:21:06.6081193' AS DateTime2), NULL, N'Friday 6pm - 8pm', N'Adult disability session - the opportunity to meet new people and keep active.', N'River Park Leisure Centre, Gordon Road', N'SO23 7DD', N'0509bd49-f4b4-4d22-a6b0-6446df863b71', N'WADSAD Archery & Swimming', N'WADSADclub@gmail.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 11:21:06.6169281' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'6b4399b3-c815-44a9-9575-c1606df0ecb9', CAST(N'2018-09-12 20:59:25.7600000' AS DateTime2), NULL, NULL, N'This group is held in the morning every Friday at 10:00am until 12:15pm. It is open to people with dementia and their carers. ', N'Winchester SO22 4QB', NULL, NULL, N'Activity Club', N'Sheila.ancient@alzheimers.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'f615c7eb-8dd9-4cc3-b7ec-c16f0d32a7d6', CAST(N'2018-09-12 20:59:25.9800000' AS DateTime2), NULL, NULL, N'Voluntary organisation supporting carers and their families. Drop in on Mondays 9am-1 pm. The Carers Centre for Memory Concerns, Church Road, Swanmore, SO32 2PA', N'Winchester City', NULL, NULL, N'', N'01489 895444', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'e596d33e-1e40-4c31-8668-c2b29c46ec78', CAST(N'2018-09-12 20:59:25.6500000' AS DateTime2), NULL, NULL, N'A new service available to help ensure isolated older people have regular social contact through volunteer visitis. ', N'', NULL, N'5788fcec-2508-4793-8a15-ebbc47a0bb30', N'Befriending', N'Paul Simmonds 0800 328 7154', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'9ee111b4-3d39-491c-9d94-c2cc6564d3d5', CAST(N'2018-09-12 20:59:26.1200000' AS DateTime2), NULL, NULL, N'Wednesday 10.30am - 11.30 am followed by tea and coffee until noon.', N'Winchester City', NULL, N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Seated Exercise Class', N'', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'18815fd0-19f2-461c-a6d0-c5e421ca3c3b', CAST(N'2018-10-24 14:55:47.9413970' AS DateTime2), NULL, N'Monday - Friday 9am - 5:30pm', N'Contact the Elderly organises Sunday afternoon tea parties for small groups of older people, aged 75 and over, who live alone. ', NULL, NULL, N'b206fe0f-9d63-49f5-ab34-188f29484bfb', N'Contact the Elderly UK', N'0800 716 543 ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 14:55:49.6577516' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'db415d72-eb01-41aa-8b5f-c6ac27e26441', CAST(N'2018-09-12 20:59:26.2433333' AS DateTime2), NULL, NULL, N'National charity providing information, advice and support for older people.#rtn##rtn#Independent Age provide: Friendship Calls or Visits, a range of online Information and Advice and also Helpline.', N'Winchester City', NULL, NULL, N'Advice and Support', N'', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'5bd2e518-dc30-482e-8c54-c93a71d90253', CAST(N'2018-09-12 20:59:26.2433333' AS DateTime2), NULL, NULL, N'Hope Church in Winchester offers a range of groups & activities including Monday Friendship Group & Soup Service. ', N'Winchester City', NULL, NULL, N'Church Group and Activities', N'01962 840800 office@hopewinchester.org', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'0bb97892-d14d-4f9a-a82c-ca0ff1ba19fb', CAST(N'2018-09-12 20:59:26.4300000' AS DateTime2), NULL, N'24/7', N'A confidential, free helpline for older people across the UK open every day and night of the year. The Silver Lines specially-trained helpline team can offer information, friendship and advice, link callers to local groups and services as well as help to protect and support older people who are suffering abuse and neglect. ', N'Winchester City', NULL, N'6bff8c82-7bc6-42f3-a328-3db13a3730bd', N'The Silver Line Helpline', N'0800 470 8090 info@thesilverline.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 14:59:16.5237251' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'6c0f5222-9fb1-4c6b-9001-cc10850eac35', CAST(N'2018-09-12 20:59:26.0233333' AS DateTime2), NULL, NULL, N'', N'Winchester City', NULL, NULL, N'', N'', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'3240417e-566e-44da-af13-cc3431e1f575', CAST(N'2018-09-12 20:59:25.8200000' AS DateTime2), CAST(N'2018-10-23 08:50:26.2125726' AS DateTime2), NULL, N'Baptist Hall, Swan Lane on Thursdays', N'Winchester City', NULL, N'79ca50fe-d393-4c66-bd59-2a6f685280e2', N'Foot clinic', N'Lunch club coordinator Cynthia Boswell. 01962 871 725/ 07984 099097  lunchclub@ageukmidhampshire.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'310e47f5-7ee0-4921-8de3-cd61e4b3839e', CAST(N'2018-09-12 20:59:25.7133333' AS DateTime2), NULL, NULL, N'Accessible cinema for people with dementia and their carers at Everyman Cinema, working with Home Instead. ', N'Winchester City', NULL, N'e3f960ee-a32f-4105-b30e-4141e86a64e8', N'Accessible Cinema', N'01962 736681', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'de0a992b-cfb0-4f20-86d4-ceb9ab33b457', CAST(N'2018-10-17 14:46:51.5887156' AS DateTime2), NULL, N'One Friday a month, 2pm - 4pm', N'Monthly social events, designed for older people to meet others with themed events and topical refreshments.', N'Hope Church Winchester
The Middle Brook Centre
Middle Brook Street
Winchester', N'SO23 8DQ', N'b7c1e612-53a6-447a-87ef-8068b8257c95', N'Hope Life Monthly Events', N'01962 840 800 hopelife@hopewinchester.org', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 14:47:28.3978917' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'41c83813-7914-410b-bb2d-d0758a45c160', CAST(N'2018-10-24 11:15:56.9517224' AS DateTime2), NULL, NULL, N'Provides information and support for people with heart disease.
', NULL, NULL, N'425099ff-96eb-4ad2-8bee-99b9ea2f6230', N'British Heart Foundation Support', N' 0300 330 3311 heretohelp@bhf.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:15:57.4972819' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'c28016f2-d2ee-4e00-809d-d1d4ae1a1761', CAST(N'2018-09-12 20:59:26.1033333' AS DateTime2), NULL, NULL, N'Offers a 1 to 1 volunteer befriender who will be matched up with the member to visit them in their own home for ongoing support and friendship. They can offer a short term shopping service for Members who are unwell or who have recently been in hospital. Members of the Bad Weather Scheme will be contacted by their matched Volunteer in the event of snow or ice to see if they need any particular help, including shopping.
 
Our Telephone Link Service matches a volunteer to a member for a regular telephone catch-up and chat.
 
Transport (subject to availability) for clubs and one off appointments.', N'Winchester Live at Home Scheme, United Church, Jewry Street, Winchester', N'SO23 8RZ', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Winchester Befriending', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 09:28:15.3216081' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'7f2a6baf-c650-400e-b04e-d1d9545b5c4c', CAST(N'2018-09-12 20:59:25.8533333' AS DateTime2), NULL, N'Every Friday, 10:30am to 12:15pm', N'Weekly activity group for people with dementia and their carers.', N'Badger Farm Community Centre
Badger Farm Road
Winchester

', N'SO22 4QB', N'3a0e0caa-ad01-4a77-b2cb-9424983861ce', N'Dementia Activity Group', N'07540919402 / 01256 363393', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-08 08:24:12.6793437' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'67650360-032d-4ba3-a178-d24da574e99f', CAST(N'2018-09-12 20:59:26.0700000' AS DateTime2), NULL, NULL, N'Meet once a month in the community lounge at Henry Beaufort School from 3.15 - 5.15pm. It is a friendly group and a warm welcome is given to all. There is a small charge of £1.50 per meeting which includes afternoon tea.', N'Winchester City', NULL, NULL, N'Retirement Club', N'Cindy Williams on 01962 8812242', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'4182a0eb-9aad-4468-874d-d4513e4a9806', CAST(N'2018-10-23 08:11:47.1401011' AS DateTime2), NULL, NULL, N'Action on Hearing Loss is the largest UK charity offering support and advice to help people confronting deafness, tinnitus and hearing loss to live the life they choose.', NULL, NULL, N'5b94b2a2-42c2-4b52-8c4f-c069a40e8baa', N'Action on Hearing Loss Helpline', N'0808 8080 123  www.informationline@hearingloss.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 10:43:24.7119168' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'4c9556a9-29c6-4aac-adaa-d4797343d540', CAST(N'2018-09-12 20:59:26.2733333' AS DateTime2), NULL, NULL, N'National charity that drives better care, treatments and quality of life for anyone affected by Parkinsons disease.#rtn##rtn#Parkinsons UK provide information, advice & support through our confidential helpline, local advisers and support groups, Information Resources & Courses. ', N'Nationwide', NULL, NULL, N'Parkinsons ', N'0808 800 0303 hello@parkinsons.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'bfb9289e-7c06-413b-b622-d614b57fe094', CAST(N'2018-10-17 11:49:21.9261802' AS DateTime2), NULL, N'Tuesdays & Thursdays, 10am - 12pm', N'£3.15 per person', N'River Park Leisure Centre 
Gordon Road
Winchester
', N'SO23 7DD', N'20386a91-5cab-42a3-9bd8-3153d82cdcef', N'River Park Tennis Class', N'01962 848 707', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 11:49:22.7549025' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'fe1d41d2-0165-48f2-aa36-d81c3021ba89', CAST(N'2018-10-23 10:47:07.5430528' AS DateTime2), NULL, NULL, N'Provides information on Carer’s Allowance and how to claim.
', NULL, NULL, N'eceb054f-478c-4ac1-8e11-e6497405fcaa', N'Carers Allowance Unit', N'0800 731 0297 www.gov.uk/carers-allowance-unit ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 10:47:08.5307386' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'346e9150-01af-401d-8aae-d894cbff97fb', CAST(N'2018-09-12 20:59:25.9933333' AS DateTime2), NULL, NULL, N'Activities are centred over ninety activity groups, ranging from Architecture and Cookery to video photography and walking', N'Winchester City', NULL, NULL, N'Range of activities', N'01962 867010 www.u3a.hampshire.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'8e26fb94-0398-4f0f-a92e-da1c417a05fe', CAST(N'2018-10-23 11:01:48.4429410' AS DateTime2), NULL, NULL, N'Government office that provides information on National Insurance contributions.

www.gov.uk/check-national-insurance-record
', NULL, NULL, N'ab5150dd-db93-436b-809a-77782f3d2c17', N'National Insurance Contributions ', N'0300 200 3500', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:01:48.4487696' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'6e3cc3b0-d995-422f-be32-da391d954eba', CAST(N'2018-09-12 20:59:25.8066667' AS DateTime2), CAST(N'2018-10-23 09:31:47.8462394' AS DateTime2), N'Mondays 10:45am - 1pm', N'Coffee served from 10:45am onwards and social time until meal is served at 12pm. Use own transport or minibus (if you live within a 3 mile radius of the city centre). The cost for minibus transport is £4.00 return. Lunch costs £6.00.', N'St Barnabas'' Church, Weeke
Fromond Rd, Winchester ', N'SO22 6EF', N'451b9902-50a8-4524-9952-29b894f13f95', N'Winchester Wellbeing Lunch Club', N'07907 503 421  wellbeingwinchester@gmail.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 08:49:01.8053067' AS DateTime2), 1, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'6b851f11-d291-4635-90f0-da6abb3b8a7f', CAST(N'2018-09-12 20:59:26.1033333' AS DateTime2), NULL, NULL, N'Mondays at 10.30 am, gentle walk around the city followed by refreshments. Meet outside The United Church on Jewry Street (SO23 8RZ)', N'Winchester City', NULL, N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Walking Group', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'11580fbe-7ad1-47c9-924c-da77c6470a33', CAST(N'2018-10-22 11:25:08.2932741' AS DateTime2), NULL, N'Mondays 2pm - 3pm, Fridays 12:30pm - 1:30pm', N'Designed specifically for people with neurological conditions, such as MS, Parkinson or those who have suffered a stroke. Circuit-based classes and encourage light physical activity to improve health and wellbeing. £3 per class.', N'River Park Leisure Centre, Gordon Road, Winchester', N'SO23 7DD', N'20386a91-5cab-42a3-9bd8-3153d82cdcef', N'Neuro Fit Classes (Referral)', N'07980 732 124', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 11:25:08.4783504' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'5328059c-a2ce-458a-ba97-db18728a91d5', CAST(N'2018-10-23 14:38:20.3811383' AS DateTime2), NULL, N'Monday - Friday 9am - 5pm', N'Provides advice and information on disability equipment and assisted products.

', NULL, NULL, N'f4b77647-7221-4596-868b-2331cbce7d4a', N'Disabled Living Foundation Support', N'0300 999 0004 / 020 7289 6111', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 14:38:21.0551280' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'1363d40e-e8c0-41af-9463-dcafe6dc2944', CAST(N'2018-10-17 15:07:31.4656773' AS DateTime2), NULL, N'Annual events', N'An online platform designed to promote days of free events and activities up and down the UK in order to change the story of loneliness in older people.', NULL, NULL, N'db91188a-6da9-46ad-b9a6-5c111f3a1f91', N'Silver Sunday Event Finder', N'www.silversunday.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 15:07:31.4705077' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'1377d3de-8b45-4495-b177-dd0481d50d1c', CAST(N'2018-10-17 11:55:37.5194789' AS DateTime2), NULL, N'Tuesdays 10am - 11am', N'£3.15 per person', N'River Park Leisure Centre
Gordon Road
Winchester', N'SO23 7DD', N'20386a91-5cab-42a3-9bd8-3153d82cdcef', N'River Park Swimming Class', N'01962 848 707', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 11:55:37.5248195' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'6843f383-c21b-4233-bae6-dd6bec8c0e5b', CAST(N'2018-09-12 20:59:25.7300000' AS DateTime2), NULL, NULL, N'Meet at ACE on Mondays 3:15pm-5:15pm, Tuesdays 9:30am-12noon and 7pm-9pm. Cost of course varies, writing activities and writing sharing. Open to new members. ', N'Winchester City', NULL, N'1b705a0d-36b1-4e3f-bfd1-1c0ce6ad22f4', N'Writers group', N'See organisation', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'8696325f-c379-4655-b05b-ddf75ab788b2', CAST(N'2018-09-12 20:59:26.2600000' AS DateTime2), NULL, NULL, N'Lipreading classes to aid communication for Deaf and hard of hearing people.', N'Winchester City', NULL, NULL, N'Lipreading Classes', N'Website - http://www.lipreading.net/classes.html', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'f6bac3f0-dd63-4394-85d7-de46a8ea5681', CAST(N'2018-10-22 11:57:26.1975413' AS DateTime2), NULL, N'12:00pm - 10.00pm', N'A freephone number to use from landlines and mobiles. ', N'13 Upper High Street, Winchester, Hampshire
', N'SO23 8UT', N'f83fdeca-18c3-48ab-86ba-104f13b3c18b', N'Winchester and District Samaritans Helpline', N'116 123', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 11:57:26.2027752' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'386279ae-73ab-490a-a563-de8f066224df', CAST(N'2018-10-17 11:47:49.0963101' AS DateTime2), NULL, N'Tuesdays 10am - 11am', N'£2 per session', N'River Park Leisure Centre
Gordon Road
Winchester', N'SO23 7DD', N'20386a91-5cab-42a3-9bd8-3153d82cdcef', N'Walking Football for 50+', N'01962 848 707', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 11:47:49.7404337' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'0725f5ea-1d36-4b08-a608-df40d38b3128', CAST(N'2018-09-12 20:59:25.9633333' AS DateTime2), NULL, NULL, N'', N'Winchester City', NULL, NULL, N'Home Library Service', N'0845 603 5631 library@hants.gov.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'7a03b423-6faf-4fc5-9fae-e244225e8ab3', CAST(N'2018-09-12 20:59:25.8533333' AS DateTime2), NULL, NULL, N'Thursdays 1.30 - 4pm.', N'Badger Farm, Winchester City', NULL, NULL, N'Art Group', N'01962 856689 Elizabeth Weir', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'ed97d2eb-639d-42f8-8590-e275fd787447', CAST(N'2018-10-23 10:58:45.7612321' AS DateTime2), NULL, NULL, N'National helpline for people with debts, giving self-help advice and support.
', NULL, NULL, N'e55535f8-46c2-42ad-9aa3-4c2fc3caf85a', N'National Debtline Service', N'0808 808 4000', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 10:58:45.7657303' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'73eefb29-3aca-4f3c-8a94-e4ce8eb3be07', CAST(N'2018-10-17 11:41:50.6327864' AS DateTime2), NULL, N'Tuesdays 2:15pm - 3pm', N'An instructor-led activity in the water for those wishing to use the positive benefits of water, such as residents and buoyancy, to gain a highly effective workout that has low or no impact on joints. ', N'River Park Leisure Centre
Gordon Road
Winchester', N'SO23 7DD', N'20386a91-5cab-42a3-9bd8-3153d82cdcef', N'River Park Hydro Jog Class ', NULL, N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 11:41:50.6380484' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'2c298a9b-da5b-4547-a491-e606f2c13e9b', CAST(N'2018-10-22 09:54:07.6344336' AS DateTime2), NULL, N'Second Wednesday of the month, 12pm - 1:30pm', N'Join for a two course meal with a drink.', N'Brasserie Blanc, 19/20 Jewry St, Winchester', N' SO23 8RZ', N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'WLAH Restaurant Club', N'01962 890995', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 09:54:07.6393085' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'69e81ef1-4d65-4560-8f03-e6cc48c209cb', CAST(N'2018-09-12 20:59:26.4500000' AS DateTime2), NULL, NULL, N'The United Church in Winchester hosts a selection of groups & activities including: Craft Club, Coffee Bar & House Groups. #rtn##rtn#The church is also used as a meeting place for local societies such as the Winchester Musicals and Opera Society. ', N'Winchester City', NULL, NULL, N'', N'01962 849559 email - office@unitedchurchwinchester.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'fecebc01-0500-4da2-a036-e6f125ffc732', CAST(N'2018-09-12 20:59:25.8533333' AS DateTime2), NULL, N'2nd Tuesdays, 2pm - 3:30pm', N'An activity group for veterans with dementia and carer, family member or friend. A group where like minded people can get together and share their experiences in a
relaxed environment. “What’s in your kitbag?” Lots of opportunity to reminisce around service life, with sessions tailored around those that attend the group.', N'St Andrew''s Garrison Church
Connaught Road
Worthy Down
Winchester ', N'SO21 2RG', N'3a0e0caa-ad01-4a77-b2cb-9424983861ce', N'Kitbags & Berets Activity Group ', N'winchester@alzheimers.org.uk 07540919402 01256 363393', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-08 08:31:17.3101342' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'4613f4cd-c99f-4f7e-aad7-e752fdcb2488', CAST(N'2018-10-23 11:44:01.1372494' AS DateTime2), NULL, N'Monday - Friday 8am - 8pm', N'You can report fraud or cyber crime to Action Fraud any time of the day or night using our online fraud reporting tool online.  You can also report and get advice about fraud or cyber crime by calling 0300 123 2040.

Action Fraud will not call you unless you have requested us to, and will never ask for your bank details. If you are unsure whether a call is genuine, call the Action Fraud team on 0300 123 2040. Please note this number will be charged at your normal network rate.

If you have inclusive minutes within your mobile tariff then calls to our number are included at no cost. If you have used all of your inclusive minutes then the call will typically cost 35p per minute depending on your network provider.', NULL, NULL, N'55c7842b-fc4f-4c9a-9e6b-b72935a8e3c1', N'Action Fraud National Helpline', N'0300 123 2040 www.actionfraud.police.uk ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:44:01.1425555' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'fe355298-80ff-4f8f-90d0-e7ba696094bd', CAST(N'2018-09-12 20:59:25.9466667' AS DateTime2), NULL, NULL, N'Health and fun club at River Park Leisure Centre (non league fitness) on Tuesdays from 10.30 - 11.30, run by Sue King, also at Weeke Community Centre, Taplings Road, Winchester on Thursdays (league fitness) 7.30 - 8 40pm. Both are £5 per session', N'Winchester City', NULL, NULL, N'Fitness League', N'01264 772872/07747061473 suekingtf;@aol.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'2d684f77-a444-4b3e-9138-e800fd2d9375', CAST(N'2018-09-12 20:59:26.2733333' AS DateTime2), NULL, NULL, N'Probus Clubs provide an opportunity for retired professionals to attend regular meetings with like-minded people who appreciate similar interests and social standing.#rtn##rtn#Probus is an organisation for active retirees who enjoy the camaraderie that belonging to this organisation brings. Many clubs have extensive networking, social programmes, which include meetings with other Probus clubs, group trips, holidays and involvement in community projects.#rtn##rtn#Find your local Probus club within Hampshire. ', N'Winchester City', NULL, NULL, N'', N'0845 230 7720 geff.sheasby@talktalk.net', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'4b402548-b272-462f-bf1c-e89475c823e5', CAST(N'2018-09-12 20:59:26.2600000' AS DateTime2), NULL, NULL, N'Fully professional and insured music therapists offering music therapy sessions to help improve physical and mental health.#rtn##rtn#Living With Harmony offer both group and individual sessions as well as specific therapy for the elderly and people with autism.', N'Winchester City', NULL, NULL, N'Music Therapy', N'07768 553058 info@livingwithharmony.net', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'264aedae-48f5-4d67-8070-e8e8ee6beb1c', CAST(N'2018-10-23 11:53:00.2360143' AS DateTime2), NULL, NULL, N'Free register for people who don’t want to receive sales and marketing contacts by post.
', NULL, NULL, N'c2273a27-08ae-49c6-91ef-e677e5a4bd64', N'Mailing Preference Service (MPS)', N'0207 291 3310 www.mpsonline.org.uk ', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 11:53:00.2407568' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'69fab7f6-d9e8-45b5-8763-ea0498c0e317', CAST(N'2018-09-12 20:59:25.7300000' AS DateTime2), NULL, NULL, N'The first Friday of the month at 10:30am. There is plenty of chatter and a choice of up to date books from the paperback book exchange.', N'', NULL, N'9ec6f994-dfa3-4da2-923d-e5d34a7ca7a3', N'Coffee Morning', N'See organisation', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'4537561c-9162-4ba1-ae44-ea5f7982b56c', CAST(N'2018-10-17 11:52:11.2356547' AS DateTime2), NULL, N'Tuesdays 10am - 11am', N'£3.15 per person', N'River Park Leisure Centre
Gordon Road
Winchester', N'SO23 7DD', N'20386a91-5cab-42a3-9bd8-3153d82cdcef', N'River Park Tai Chi Class', N'01962 848 707', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 11:52:12.2387049' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'01762578-301a-47a9-9040-ecac099b3e98', CAST(N'2018-10-22 09:43:05.5487187' AS DateTime2), NULL, N'Alternate Wednesdays, 2pm - 4pm', N'Members meet and have a chance to exchange memories based on the objects that they bring along.', N'St Mark''s C of E Church, Olivers Battery Rd S, Winchester', N'SO22 4EU', N'7113521a-2cea-4eb2-84f4-ebd8c3e1ea3e', N'Oliver''s Battery - Battery Club', N'01962 852650', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 09:43:05.5537112' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'300a163c-3a4c-4cca-8c40-ed76648d82d9', CAST(N'2018-09-12 20:59:25.9800000' AS DateTime2), NULL, NULL, N'', N'Winchester City', NULL, NULL, N'Meals on Wheels', N'01962 779338', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'f75bb39f-6074-4a9b-823e-eefe3f2aaa3a', CAST(N'2018-09-12 20:59:26.3066667' AS DateTime2), NULL, NULL, N'Choir for all ages and abilities based in Winchester.#rtn##rtn#The aim of Sing Winchester is to bring the joy of singing world music songs to anyone who wants to join in – beginners, seasoned choir goers, men, women, all ages and all abilities. The atmosphere is light-hearted, friendly and very productive!', N'Winchester City', NULL, NULL, N'', N'07941 246685 email - jac@singwinchester.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'14802bc1-aedf-4271-9672-f1281c9099f9', CAST(N'2018-09-12 20:59:25.9800000' AS DateTime2), NULL, NULL, N'Talks, plant sale, coffee morningetc. If you would like to join the club please contact membership secretary Phil Butler', N'Winchester City', NULL, NULL, N'Garden Club', N'01489 877596 philbutler831@btinternet.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'387bebf3-2e43-459d-bff5-f3be344ef811', CAST(N'2018-09-12 20:59:25.9466667' AS DateTime2), NULL, NULL, N'An informal craft activity group which takes place in the café area of the discovery centre. Fortnightly on Mondays 5pm - 6.45pm and Tuesdays 10am - 12pm', N'Winchester City', NULL, NULL, N'Knit and Natter', N'0845 603 5631', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'af4c71f9-2ec3-4201-97c4-f4aa2000b1e2', CAST(N'2018-10-17 10:35:16.7576736' AS DateTime2), NULL, N'Tuesdays 11:30am - 12:30pm', N'Dance class for over 65''s. Fun and relaxed with a focus on exploring movement, learning new skills and increasing fitness and wellbeing. Refreshments provided and the chance to socialise in the cafe. £2 per class.', N'Unit 12
Winnall Valley Road
Winchester', N'SO23 0LD', N'2627e3dc-242e-4d72-a951-6ded72f754f7', N'Integr8 Movement Gems - Active Dance', NULL, N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-17 10:35:16.7627940' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'3aab0c5a-61da-415b-9868-f4d8a61a249e', CAST(N'2018-09-12 20:59:25.8700000' AS DateTime2), NULL, NULL, N'Thursday 8 - 9pm', N'Badger Farm, Winchester City', NULL, NULL, N'Badminton', N'01962 861525 Wendy King', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'2172a1c4-4a6c-446f-916c-f5e3ef863bae', CAST(N'2018-09-12 20:59:25.6800000' AS DateTime2), NULL, N'Mondays, Tuesdays & Thursdays', N'Lunch with tea and biscuits
Coffee served from 10.45am onwards and social time until meal is served at 12 noon. Use own transport or minibus (if you live within a 3 mile radius of the city centre). The cost for minibus transport is £4.00 return. Lunch £6.00  ', N'The Baptist Hall, Swan Lane, Winchester', N'SO23 7AA', N'451b9902-50a8-4524-9952-29b894f13f95', N'Winchester Lunch Club', N'Lunch Club Coordinator 07907 503 421 wellbeingwinchester@gmail.com', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-23 10:33:30.6646905' AS DateTime2), 1, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'13aafccb-ea86-43e3-bde2-f63519ffb5ea', CAST(N'2018-10-22 11:31:08.5711939' AS DateTime2), NULL, N'Mondays 3pm - 3:45pm', N'Class for people referred by their GP (email vijones@winchester.gov.uk if you think you are eligible to be enrolled on the scheme). A variety of exercises within a class environment, in a circuit format where exercises are low level. £3.', N'River Park Leisure Centre, Gordon Road, Winchester', N'SO23 7DD', N'20386a91-5cab-42a3-9bd8-3153d82cdcef', N'Assisted Circuit Class (Referral)', N'079810 732 124', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-22 11:31:08.5761698' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'54ce8867-c2eb-468b-b2cd-f7f71cf2b3d8', CAST(N'2018-09-12 20:59:26.0400000' AS DateTime2), NULL, NULL, N'At the University of Winchester, Sparkford Road, ten evenings of the year, from 7.30 on the 2nd of Tuesday of every month. £30 per year.', N'Winchester City', NULL, NULL, N'Writing Group', N'Barbera Large - 01962 841515 and Barbara.large@winchester.ac.uk Gary Farnell - 01962 827267 and gary.farnell@winchester.ac.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'f65d7f6b-343c-4610-8ac7-fd0c7b6a4005', CAST(N'2018-09-12 20:59:26.2300000' AS DateTime2), NULL, NULL, N'Hampshire Cultural Trust exists to provide a platform for world class arts, culture and heritage. The website provides further details on venues, events & activities. ', N'Winchester City', NULL, NULL, N'Cultural Trust', N'01962 826700 enquiries@hampshireculturaltrust.org.uk', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:06:10.9333333' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[Activities] ([Id], [DateCreated], [DateDeleted], [DateTimeDescription], [Description], [LocationDescription], [LocationPostcode], [OrganisationId], [Title], [Contact], [CreatedById], [DateUpdated], [Status], [UpdatedById]) VALUES (N'8b59fd93-5a79-4ec8-96a7-ff096493a312', CAST(N'2018-10-24 11:10:10.8419346' AS DateTime2), NULL, NULL, N'Offers information and support for people with arthritis and has a network of branches and groups.
', NULL, NULL, N'3b584634-f995-4961-9a05-bf7c60a62955', N'Arthritis Care UK Support', N'0808 800 4050 / 0300 790 0400', N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-10-24 11:10:10.8470536' AS DateTime2), 0, N'3459a547-4d80-42e7-b8b2-ba3d1f128071')
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'2d82a37d-6a60-4753-b639-001ad6530a25', N'a2169115-dbda-4332-a986-18d4161c903f', N'52f71900-4746-427b-9d5d-95057bf5b6cd', CAST(N'2018-09-12 21:00:14.2433333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e8f603d4-a8e0-4436-8109-00b82a7bec24', N'0ffc9eef-9902-4bdd-a423-1b7537215b1e', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-09-12 21:00:13.5866667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'1a9c222b-4ac1-4846-8dcb-06fda59a650c', N'77b6c435-7602-432c-a9ef-58757e729fa3', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.9933333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'daa54fbc-b7cf-4724-8967-08d6287a3cf5', N'907cb6c6-d189-45f5-91a1-014792872b3d', N'043fd5bb-3a99-4eed-91bc-e87fe0f57085', CAST(N'2018-10-02 15:17:59.2731900' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'6779b7b6-dead-4bb3-e2e9-08d62cf5346c', N'8669a19a-7b71-4c00-8caf-085cd4aa4e67', N'f81ce861-d5fc-4639-834b-5328ee3fd853', CAST(N'2018-10-08 08:08:17.7486571' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'ef3ae26f-700a-43da-e2ea-08d62cf5346c', N'8669a19a-7b71-4c00-8caf-085cd4aa4e67', N'0dbba5d4-8729-4a88-b6a4-7753ddfa8774', CAST(N'2018-10-08 08:08:17.8578529' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e2aeef93-1c2b-431c-e2ed-08d62cf5346c', N'7f2a6baf-c650-400e-b04e-d1d9545b5c4c', N'5901bd92-07bc-483d-a7c0-f050c22bb052', CAST(N'2018-10-08 08:24:12.7368519' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'808b5738-7491-490b-e2ee-08d62cf5346c', N'34ee64bc-ee74-4b8c-9a82-493e9e69dde4', N'5901bd92-07bc-483d-a7c0-f050c22bb052', CAST(N'2018-10-08 08:28:07.7509314' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'90b4fcc6-4b7e-4f1f-e2ef-08d62cf5346c', N'fecebc01-0500-4da2-a036-e6f125ffc732', N'5901bd92-07bc-483d-a7c0-f050c22bb052', CAST(N'2018-10-08 08:31:17.3678307' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'dad06285-235e-478b-e2f0-08d62cf5346c', N'636f6f93-be8f-48e6-b421-70c731fa5c3d', N'5901bd92-07bc-483d-a7c0-f050c22bb052', CAST(N'2018-10-08 08:32:50.0730398' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'c46889a1-2417-4805-e2f6-08d62cf5346c', N'd705b896-d3de-4bf7-9e1b-568f6407aaed', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-10-08 09:21:50.0706747' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'0657fc1d-7aa9-4d88-a361-08d63412a3a2', N'06804950-a493-4290-81cf-370458be8010', N'33f402bc-993e-4f82-9620-75bdef3ef824', CAST(N'2018-10-17 09:26:37.8755056' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e10900f9-8312-4315-a362-08d63412a3a2', N'c6955c85-4552-4115-baaf-56761d961c25', N'33f402bc-993e-4f82-9620-75bdef3ef824', CAST(N'2018-10-17 09:26:37.9850502' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3b9b1405-5186-4953-a363-08d63412a3a2', N'c6955c85-4552-4115-baaf-56761d961c25', N'9ef6d493-04fe-42b9-8212-4372a4940468', CAST(N'2018-10-17 09:26:38.0865033' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3c15bda9-1322-47c5-a364-08d63412a3a2', N'06804950-a493-4290-81cf-370458be8010', N'9ef6d493-04fe-42b9-8212-4372a4940468', CAST(N'2018-10-17 09:26:38.1182580' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'98277699-5d10-4799-a365-08d63412a3a2', N'c6955c85-4552-4115-baaf-56761d961c25', N'f81ce861-d5fc-4639-834b-5328ee3fd853', CAST(N'2018-10-17 09:26:38.1543861' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'1562f5f6-b8ec-4d1d-a366-08d63412a3a2', N'06804950-a493-4290-81cf-370458be8010', N'f81ce861-d5fc-4639-834b-5328ee3fd853', CAST(N'2018-10-17 09:26:38.1861046' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'93a42ccb-18a0-47f8-a367-08d63412a3a2', N'c6955c85-4552-4115-baaf-56761d961c25', N'c21c2b32-81da-483c-b54d-6078969767af', CAST(N'2018-10-17 09:26:38.2211009' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'ddb97830-dd01-4991-a368-08d63412a3a2', N'06804950-a493-4290-81cf-370458be8010', N'c21c2b32-81da-483c-b54d-6078969767af', CAST(N'2018-10-17 09:26:38.2260850' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'baa7ef99-6022-417d-a369-08d63412a3a2', N'9830d213-a21e-4545-9ebe-2b78ed2d78d9', N'1da7c6c2-2855-40e9-ba66-85b32216ad3f', CAST(N'2018-10-17 09:44:01.5392584' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'd0756c1d-5a2c-4abf-a36a-08d63412a3a2', N'9830d213-a21e-4545-9ebe-2b78ed2d78d9', N'5901bd92-07bc-483d-a7c0-f050c22bb052', CAST(N'2018-10-17 09:44:01.5783107' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'b8a947db-95a7-4f71-a36b-08d63412a3a2', N'07b81ab3-ae5f-4f60-b636-a2bc055f08a5', N'1da7c6c2-2855-40e9-ba66-85b32216ad3f', CAST(N'2018-10-17 10:18:41.6414798' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'35a05c15-4a26-4ac4-a36c-08d63412a3a2', N'07b81ab3-ae5f-4f60-b636-a2bc055f08a5', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-10-17 10:18:41.6776164' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'54ed6aa5-0237-4b3a-a36d-08d63412a3a2', N'07b81ab3-ae5f-4f60-b636-a2bc055f08a5', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-17 10:18:41.7140386' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'd6f0563e-fa94-4ef4-a36e-08d63412a3a2', N'0d2dfbfe-38e9-430e-922b-665aa677f5b0', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-10-17 10:23:09.2264325' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e4568ee3-0f1d-461b-a36f-08d63412a3a2', N'0d2dfbfe-38e9-430e-922b-665aa677f5b0', N'fd728574-b579-4f32-b464-e754329f4edf', CAST(N'2018-10-17 10:23:09.2635676' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'6d9707f3-4dc4-4c12-a370-08d63412a3a2', N'12d68d05-de09-4f3c-b7fd-3722d00daee7', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-10-17 10:33:45.8677369' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'ba5a1a55-117b-4b24-a371-08d63412a3a2', N'12d68d05-de09-4f3c-b7fd-3722d00daee7', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-17 10:33:45.9044724' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'04df6a4f-e447-4237-a372-08d63412a3a2', N'af4c71f9-2ec3-4201-97c4-f4aa2000b1e2', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-10-17 10:35:16.8060474' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'6427c92a-a088-4e44-a373-08d63412a3a2', N'af4c71f9-2ec3-4201-97c4-f4aa2000b1e2', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-17 10:35:16.8428140' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'aa85f35d-90e6-4c03-a374-08d63412a3a2', N'8d24a992-5af1-40e6-b0f0-25f8064beb74', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-10-17 10:40:07.8703632' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'07ee55fd-95d9-4652-a375-08d63412a3a2', N'8d24a992-5af1-40e6-b0f0-25f8064beb74', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-17 10:40:07.9077148' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e0925ac3-21f1-436a-a376-08d63412a3a2', N'cc712f68-74f3-428d-8ddb-204bf8ea7559', N'3c2c353e-fb24-495e-a6e7-7421bc7e293c', CAST(N'2018-10-17 11:17:13.7459639' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'363199e5-e97b-4eeb-a377-08d63412a3a2', N'cc712f68-74f3-428d-8ddb-204bf8ea7559', N'043fd5bb-3a99-4eed-91bc-e87fe0f57085', CAST(N'2018-10-17 11:17:13.7876424' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'6412fad6-fa69-4d74-a378-08d63412a3a2', N'808365e3-3c6f-4a6a-bd59-5407e490788b', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-10-17 11:29:33.3729903' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'71c9907f-7f91-4122-a379-08d63412a3a2', N'808365e3-3c6f-4a6a-bd59-5407e490788b', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-17 11:29:33.4102905' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'6076cc05-1c20-4460-a37a-08d63412a3a2', N'73eefb29-3aca-4f3c-8a94-e4ce8eb3be07', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-10-17 11:41:50.6966455' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'c1893f9e-12dc-4f32-a37b-08d63412a3a2', N'73eefb29-3aca-4f3c-8a94-e4ce8eb3be07', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-17 11:41:50.7340818' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'a2f6aa9c-b655-4a05-a37c-08d63412a3a2', N'386279ae-73ab-490a-a563-de8f066224df', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-17 11:47:49.7857674' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'c1b536e4-fe67-4525-a37d-08d63412a3a2', N'bfb9289e-7c06-413b-b622-d614b57fe094', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-17 11:49:22.7997111' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'db64576c-d865-45de-a37e-08d63412a3a2', N'715d75cf-8a67-4412-8ae4-46530db662d3', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-17 11:50:55.4624583' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'44413bd2-94b6-4524-a37f-08d63412a3a2', N'4537561c-9162-4ba1-ae44-ea5f7982b56c', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-17 11:52:12.2849820' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'5af898ff-b680-47b7-a380-08d63412a3a2', N'a1117186-be05-4780-ab43-729690e35e5f', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-17 11:54:40.0988089' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e9990662-3335-495a-a381-08d63412a3a2', N'1377d3de-8b45-4495-b177-dd0481d50d1c', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-17 11:55:37.5682798' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'922a5b04-75a0-404e-a382-08d63412a3a2', N'8a240c6d-158e-4aea-8977-c054eb6c5b4d', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-17 13:46:32.8430944' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'94980f7d-fa55-432b-2376-08d6343dc57c', N'65833500-ef68-4558-a91c-b70fee1bada7', N'c67dbc03-5c94-4d2b-8f65-00217b89701c', CAST(N'2018-10-17 14:35:23.0206965' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'95c524cb-286a-4ff2-2377-08d6343dc57c', N'3a63d748-c014-4c6f-ad67-1e4a754efe4f', N'c67dbc03-5c94-4d2b-8f65-00217b89701c', CAST(N'2018-10-17 14:35:45.5403684' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'5205c5a6-bee8-4e35-2378-08d6343dc57c', N'968b60a3-e513-41be-8da9-8db6a2362999', N'c67dbc03-5c94-4d2b-8f65-00217b89701c', CAST(N'2018-10-17 14:35:45.6810836' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'b4564cd8-f47d-4d6a-b115-08d6343eb4c4', N'de0a992b-cfb0-4f20-86d4-ceb9ab33b457', N'c67dbc03-5c94-4d2b-8f65-00217b89701c', CAST(N'2018-10-17 14:47:28.4540145' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'caf2d98a-5c3a-4ee4-b116-08d6343eb4c4', N'eb94626a-9f4e-489f-b208-31e74de04093', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-17 14:47:53.5763413' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'7b5cf2bc-ef25-4eb6-b117-08d6343eb4c4', N'cdc23c09-f083-4968-976e-a454b5bd7198', N'418d5727-a393-47d5-8b4f-bf6c1bdbc090', CAST(N'2018-10-17 14:50:28.3508737' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'4a03cd50-6849-47c8-b118-08d6343eb4c4', N'623c5c31-0a8a-45f1-ab9e-1af4f1987304', N'c67dbc03-5c94-4d2b-8f65-00217b89701c', CAST(N'2018-10-17 14:53:29.2691477' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'905599ce-3033-4283-b119-08d6343eb4c4', N'ee49590b-f0cd-4165-beac-2857735de061', N'bf4843c8-25c8-45ce-8e30-65a82131f417', CAST(N'2018-10-17 14:57:24.3910645' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'9c486ccf-1c32-417a-b11a-08d6343eb4c4', N'0bb97892-d14d-4f9a-a82c-ca0ff1ba19fb', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-17 14:59:16.5970160' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e892a2e0-ae5b-4132-b11b-08d6343eb4c4', N'1363d40e-e8c0-41af-9463-dcafe6dc2944', N'1da7c6c2-2855-40e9-ba66-85b32216ad3f', CAST(N'2018-10-17 15:07:31.5125895' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'06d35c56-0345-4a84-b11c-08d6343eb4c4', N'803a10e5-0bd3-4b29-94d8-010e63eac2d9', N'c67dbc03-5c94-4d2b-8f65-00217b89701c', CAST(N'2018-10-18 10:31:20.7763685' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'543c9cd3-6366-474a-b11d-08d6343eb4c4', N'31a645f4-8ea3-4d4c-8850-0d50b876e7d0', N'b16e7ca0-d372-4a9b-b373-254d9fb18cdf', CAST(N'2018-10-18 10:37:18.8617284' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'2e944770-274f-4920-b11e-08d6343eb4c4', N'92dad0fc-5865-46f3-8f7d-47cd60582d42', N'b16e7ca0-d372-4a9b-b373-254d9fb18cdf', CAST(N'2018-10-18 10:38:57.8432944' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'f1fc8c13-ba2f-48b6-b11f-08d6343eb4c4', N'365bf148-8d62-4cc3-badc-5ec7713628d5', N'0e72f62f-cdc3-427e-a7e8-b3883e137f26', CAST(N'2018-10-18 10:40:38.6618217' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'70cc7ea5-2b9a-4d2b-b120-08d6343eb4c4', N'365bf148-8d62-4cc3-badc-5ec7713628d5', N'cbfd4633-8d9b-49e0-890d-ba9125021cf3', CAST(N'2018-10-18 10:40:38.6970875' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'8520c3fb-db89-4c52-b121-08d6343eb4c4', N'365bf148-8d62-4cc3-badc-5ec7713628d5', N'b16e7ca0-d372-4a9b-b373-254d9fb18cdf', CAST(N'2018-10-18 10:40:38.7317309' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'ea511b2e-3906-459f-b122-08d6343eb4c4', N'60113e12-da31-4569-89ed-0d9ae9eaadef', N'b16e7ca0-d372-4a9b-b373-254d9fb18cdf', CAST(N'2018-10-18 10:42:10.7598476' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'95d3afcc-3e5e-47dc-b123-08d6343eb4c4', N'e193a15d-1cd7-41ea-b880-b61974789163', N'3c2c353e-fb24-495e-a6e7-7421bc7e293c', CAST(N'2018-10-18 10:43:52.3462223' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'334fd121-4f91-4c34-b124-08d6343eb4c4', N'b8cf3a9f-9bc5-4692-922c-776a7c91c128', N'3c2c353e-fb24-495e-a6e7-7421bc7e293c', CAST(N'2018-10-18 10:45:22.4521875' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'14acd800-517a-4817-47e6-08d63800218f', N'b09e8b09-1516-4771-9646-9c598abc3426', N'5baa353a-9a5d-4e4a-86fb-85e5c9cc995c', CAST(N'2018-10-22 09:24:13.3544251' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'cd7da26a-5b61-4648-47e7-08d63800218f', N'b09e8b09-1516-4771-9646-9c598abc3426', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-10-22 09:24:13.4648162' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'5d2d2614-5bcc-4ebb-47e8-08d63800218f', N'c28016f2-d2ee-4e00-809d-d1d4ae1a1761', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-10-22 09:28:15.4261898' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'bf71623d-1346-403b-47e9-08d63800218f', N'01762578-301a-47a9-9040-ecac099b3e98', N'0b8dff55-23f8-4152-96c0-a5110e0d4a7e', CAST(N'2018-10-22 09:43:05.5960758' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3fb0c552-40d6-4e15-47ea-08d63800218f', N'7222d817-425c-47a2-92fb-0d7a669efba2', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-22 09:46:54.4768928' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'2aa87532-e91d-4a4f-47eb-08d63800218f', N'05e5aed8-4368-4630-826b-9cb5f59e9476', N'0e72f62f-cdc3-427e-a7e8-b3883e137f26', CAST(N'2018-10-22 09:48:23.0600656' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'bb203429-f9e4-42e7-47ec-08d63800218f', N'8c131023-4ec2-4fc1-a347-349cc7a61a21', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-10-22 09:51:52.5345086' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e154adec-a0ca-4297-47ed-08d63800218f', N'2c298a9b-da5b-4547-a491-e606f2c13e9b', N'0e72f62f-cdc3-427e-a7e8-b3883e137f26', CAST(N'2018-10-22 09:54:07.6818555' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'156ab3b1-7dfe-40bb-47ee-08d63800218f', N'5d3ab072-3015-4165-a94e-20d3c81c9f8c', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-10-22 10:06:43.4479623' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'edbfc262-a5b7-44f9-47ef-08d63800218f', N'eb5811b9-4624-4d28-8c8d-2220c46c57bd', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-10-22 10:08:10.9045401' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'8e9020b9-ca59-42d3-47f0-08d63800218f', N'dc208ac0-052a-47db-90d5-4005566ac888', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-10-22 10:10:00.8886275' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3544d3a3-b7d1-43ec-47f1-08d63800218f', N'3aaa0e03-0b00-4c1d-9f79-6b85bfc91961', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-22 10:11:54.3828997' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'a2bac576-19de-4d50-47f2-08d63800218f', N'3df5fd8c-e514-459c-9644-b1ab6f02be20', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-10-22 10:17:36.9109179' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'6bfd5cbd-01cc-46e6-47f3-08d63800218f', N'e068fc72-f335-4a52-86be-070385253002', N'0e72f62f-cdc3-427e-a7e8-b3883e137f26', CAST(N'2018-10-22 10:24:17.1489205' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e09b594a-5ab2-481b-47f4-08d63800218f', N'23ccd417-98be-439d-8f23-23ebad8f28fc', N'0e72f62f-cdc3-427e-a7e8-b3883e137f26', CAST(N'2018-10-22 10:29:22.7097235' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'c4ad81bd-721f-4735-47f5-08d63800218f', N'26e74aea-0698-4770-9958-86cdd0e5261a', N'bf4843c8-25c8-45ce-8e30-65a82131f417', CAST(N'2018-10-22 10:37:24.2758951' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'7c7d8854-31eb-4205-47f6-08d63800218f', N'9b81dbe9-8631-4ed7-a1b2-0e92795e9f9a', N'bf4843c8-25c8-45ce-8e30-65a82131f417', CAST(N'2018-10-22 10:39:10.0342738' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'1bc66479-ea4e-43cb-47f7-08d63800218f', N'7453340e-dc40-4ece-8fd7-ac36e28ca4ed', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-10-22 10:47:27.7245205' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'4f3f27ce-73b1-4fb9-47f8-08d63800218f', N'02497cea-c2dd-40a3-94e1-9fcd5c6589c4', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-10-22 10:50:53.6677774' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'891588a2-6e76-4a82-47fb-08d63800218f', N'a124aa37-0f4e-4352-ad0d-b7e58fe9dbbd', N'6147a8b9-6fdb-4c74-8d47-bf8e945ae7e0', CAST(N'2018-10-22 10:57:39.2939519' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'02907d30-5bee-4f51-47fc-08d63800218f', N'cafcb554-32bc-4101-af77-943b162a6709', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-10-22 10:58:50.8881246' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'457f4f72-8462-43c6-47fd-08d63800218f', N'c5c1b348-0282-474e-a2ff-7acf236c52d7', N'1da7c6c2-2855-40e9-ba66-85b32216ad3f', CAST(N'2018-10-22 11:00:50.3031222' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'94c00f49-fc84-41e8-47fe-08d63800218f', N'eb170010-001c-4035-a0a3-4e3ae543d589', N'52f71900-4746-427b-9d5d-95057bf5b6cd', CAST(N'2018-10-22 11:07:13.3034777' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'f24e626d-bda4-413b-47ff-08d63800218f', N'354c6d7f-3c15-4687-afcd-c15edc9e609b', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-10-22 11:21:06.6969843' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'a2e5455a-b25b-48c9-4800-08d63800218f', N'11580fbe-7ad1-47c9-924c-da77c6470a33', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-10-22 11:25:08.7342614' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'45f1b0fc-ac65-43be-4801-08d63800218f', N'13aafccb-ea86-43e3-bde2-f63519ffb5ea', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-10-22 11:31:08.6177460' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'b3e2013e-b6e8-461d-4803-08d63800218f', N'f6bac3f0-dd63-4394-85d7-de46a8ea5681', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-22 11:57:26.2429643' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'74c3dc81-0f19-4c6c-480b-08d63800218f', N'9d3a3c04-bc6d-4943-8bce-507618d65975', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-10-23 08:20:28.7744230' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'c93ac5f9-25ec-4783-480c-08d63800218f', N'3a9761b9-1b89-4aa6-8353-afd44eb1b904', N'74a5bb6d-f734-4489-895c-341b932f3c75', CAST(N'2018-10-23 08:20:49.4815659' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'5f838df2-bc7b-479d-480d-08d63800218f', N'60840546-e7ff-4608-b4aa-0327f4490dc0', N'97688174-7cd2-47c9-9ef9-87e72b21837a', CAST(N'2018-10-23 08:21:05.4758835' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3fb6f3d5-ca00-4b02-480e-08d63800218f', N'3924157e-f81a-4b7c-ae14-08465e15e1a1', N'5901bd92-07bc-483d-a7c0-f050c22bb052', CAST(N'2018-10-23 08:21:58.9464767' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'fbc082ee-0f82-4ee8-4811-08d63800218f', N'6e3cc3b0-d995-422f-be32-da391d954eba', N'0e72f62f-cdc3-427e-a7e8-b3883e137f26', CAST(N'2018-10-23 08:49:01.8904003' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'd6789af6-4099-445e-4812-08d63800218f', N'4e15e5fd-096d-4f5a-8b0f-04031a50cd24', N'b16e7ca0-d372-4a9b-b373-254d9fb18cdf', CAST(N'2018-10-23 09:21:57.3297253' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'42175e4b-1470-43be-4815-08d63800218f', N'fa3c8cbb-7e4c-4b38-92ef-11a9338a2db9', N'9ef6d493-04fe-42b9-8212-4372a4940468', CAST(N'2018-10-23 10:03:46.4749276' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'b859cb31-fad4-4550-4816-08d63800218f', N'7934cf81-218f-49d1-9306-198c20eb35d9', N'0e72f62f-cdc3-427e-a7e8-b3883e137f26', CAST(N'2018-10-23 10:33:03.2287121' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'5a106bc9-890c-4ff3-4817-08d63800218f', N'2172a1c4-4a6c-446f-916c-f5e3ef863bae', N'0e72f62f-cdc3-427e-a7e8-b3883e137f26', CAST(N'2018-10-23 10:33:30.7308849' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'b81c97c3-9270-460e-4818-08d63800218f', N'8412c763-4d18-42f2-a413-60e3da8772a5', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-23 10:40:23.2152391' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3ba7f474-de90-408a-481a-08d63800218f', N'10110f81-ac6a-4834-8040-5943918ca21f', N'39101bce-48d7-4b65-8314-adfe6a11a50b', CAST(N'2018-10-23 10:43:05.3412906' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'115c7375-de5f-4fc7-481b-08d63800218f', N'4182a0eb-9aad-4468-874d-d4513e4a9806', N'7387c9f3-30c9-49e2-8ebe-7ef6a9aa22a6', CAST(N'2018-10-23 10:43:24.7802676' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'83792f2f-d0ab-47c5-481c-08d63800218f', N'fe1d41d2-0165-48f2-aa36-d81c3021ba89', N'29cffd9a-6e7d-4e8a-850e-26f3ae461b47', CAST(N'2018-10-23 10:47:08.5722289' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'6637c453-e918-425a-481d-08d63800218f', N'745b3bc6-2449-4023-bfd5-a8f141715593', N'29cffd9a-6e7d-4e8a-850e-26f3ae461b47', CAST(N'2018-10-23 10:51:21.1065539' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'026ec7b1-d77a-41f9-481e-08d63800218f', N'5bca4db8-4468-4a46-b0fb-00684d37478a', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5', CAST(N'2018-10-23 10:56:35.5853740' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'a0d2bea5-11a0-4814-481f-08d63800218f', N'ed97d2eb-639d-42f8-8590-e275fd787447', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5', CAST(N'2018-10-23 10:58:45.8119047' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3c616e50-3b98-4f10-4820-08d63800218f', N'6c3a2126-b188-4bc6-9f8f-31d2f5864b3c', N'39101bce-48d7-4b65-8314-adfe6a11a50b', CAST(N'2018-10-23 11:01:48.0824756' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'1dee1e79-29f9-45f7-4821-08d63800218f', N'8e26fb94-0398-4f0f-a92e-da1c417a05fe', N'39101bce-48d7-4b65-8314-adfe6a11a50b', CAST(N'2018-10-23 11:01:48.5023264' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'7e3223e9-e650-42c0-4822-08d63800218f', N'9200d771-7eec-4052-b6c1-9a7f01a4b704', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5', CAST(N'2018-10-23 11:04:50.3319248' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'86a030a7-2f31-4cc0-4823-08d63800218f', N'bbc71a48-8e57-4a90-b864-027429639959', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5', CAST(N'2018-10-23 11:05:55.7226374' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'301c089c-3f73-47ff-4824-08d63800218f', N'0952f7c8-6cc0-4b6c-b64f-875ed5d8ab1c', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5', CAST(N'2018-10-23 11:08:37.1486205' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'45f6c5ea-6878-4606-4825-08d63800218f', N'fef51877-3de2-4493-b38b-c0228502f06c', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5', CAST(N'2018-10-23 11:11:55.1902932' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'7283362a-64fb-4b76-4828-08d63800218f', N'a0a269cf-fbb8-4f25-856c-407be1601635', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5', CAST(N'2018-10-23 11:18:09.2935509' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'2ba45c71-0c26-4837-4829-08d63800218f', N'ccc9e3c8-5811-4fcb-aa90-a26615d7c051', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5', CAST(N'2018-10-23 11:23:25.9917926' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'617bcf66-d828-4f5e-482a-08d63800218f', N'ce7a0ca1-b554-4a6d-a3aa-201a20a23067', N'29cffd9a-6e7d-4e8a-850e-26f3ae461b47', CAST(N'2018-10-23 11:28:19.0722180' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'6f102024-f4f7-4ef7-482b-08d63800218f', N'b9694a45-c3ba-4752-8aa5-b4f1c776aa6c', N'29cffd9a-6e7d-4e8a-850e-26f3ae461b47', CAST(N'2018-10-23 11:30:25.2374551' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'979f2876-e392-4d47-482c-08d63800218f', N'45e2939a-43f9-46a8-8e44-32b19e79c86a', N'29cffd9a-6e7d-4e8a-850e-26f3ae461b47', CAST(N'2018-10-23 11:32:10.0010245' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'7401b0a0-2e70-4731-482d-08d63800218f', N'8dc80ea9-6642-4900-96b1-204ef84249de', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-23 11:36:15.7527775' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'b3e837a9-7c57-4c64-482e-08d63800218f', N'b2d0ee29-b9a3-4c08-9b45-2f47b992daba', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-23 11:39:48.5200980' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'99dbafd9-2152-4e5a-482f-08d63800218f', N'4613f4cd-c99f-4f7e-aad7-e752fdcb2488', N'f65b3254-46cb-494a-8a5a-1a831a009d33', CAST(N'2018-10-23 11:44:01.1826202' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3589fa83-0e3c-4b96-4830-08d63800218f', N'437157c4-398d-40f7-a7bd-b0bc30ae65b4', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-23 11:49:58.2326727' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'4e673270-9f73-47e0-4831-08d63800218f', N'264aedae-48f5-4d67-8070-e8e8ee6beb1c', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-23 11:53:00.2945424' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'806a73b1-ee6d-45f2-4832-08d63800218f', N'dfe996a5-67b9-45be-8d38-490c72d4dd12', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5', CAST(N'2018-10-23 14:15:18.5306827' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'6d11f167-c6c9-4ca1-4833-08d63800218f', N'8fca5514-5872-4258-aa60-5afee6b42094', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-23 14:19:21.0350161' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'0c2df92e-a40a-481d-4834-08d63800218f', N'ee9bad14-9b30-4f72-bc00-72304ae1e845', N'f81ce861-d5fc-4639-834b-5328ee3fd853', CAST(N'2018-10-23 14:26:05.7553103' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'b93c7337-432b-4af2-4835-08d63800218f', N'f864e2b6-39e0-4f13-a62b-88c29ba479b0', N'f65b3254-46cb-494a-8a5a-1a831a009d33', CAST(N'2018-10-23 14:31:17.5757221' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'520b1f38-b0e2-4a9a-4836-08d63800218f', N'154516e0-2ac0-4e91-aef0-1590b9a9178e', N'ead47d98-11f4-4990-abb9-0ef6e8b25841', CAST(N'2018-10-23 14:34:42.5222312' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'698b88a1-8d01-4711-4837-08d63800218f', N'5328059c-a2ce-458a-ba97-db18728a91d5', N'9ef6d493-04fe-42b9-8212-4372a4940468', CAST(N'2018-10-23 14:38:21.0986428' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'54c1c235-1dd3-4489-4838-08d63800218f', N'cfcc4274-23b3-4c21-bf49-bde3f3288e3d', N'ead47d98-11f4-4990-abb9-0ef6e8b25841', CAST(N'2018-10-23 14:45:10.5995249' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3ba749cc-d973-4c9d-4839-08d63800218f', N'2627b729-d799-430a-9080-5ef32bd47bfa', N'41766753-9048-485c-ac9c-10b2ed0520a9', CAST(N'2018-10-23 14:48:53.1448553' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'8b970240-07a5-443b-483a-08d63800218f', N'bc4f4c03-e6c7-49dd-8abb-657f708a8229', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-10-23 14:50:00.2164744' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'4c32073e-bda6-456c-483b-08d63800218f', N'437bda94-eef3-4b38-a861-6f0a59d40d57', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-10-23 14:56:34.8020852' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e6cd34b7-ca00-4311-483c-08d63800218f', N'a66ac502-5397-4a75-b5a1-2b69b6140f89', N'1a11fedd-1839-4b7f-bd68-b6e9ec5b5f18', CAST(N'2018-10-23 14:59:59.4344080' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'97e7a892-b129-4a97-483d-08d63800218f', N'5763f055-d460-4e60-be33-a4534281d3a6', N'e2e9f3b5-fc55-40ad-a65a-0cdfae98e969', CAST(N'2018-10-23 15:04:48.4418982' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'be8339d4-e208-46c4-483e-08d63800218f', N'4ffedd5a-7d16-43ea-b4a1-6f9e98f3e8d2', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-10-23 15:08:23.0217156' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'472039a4-ccc0-488d-483f-08d63800218f', N'8b59fd93-5a79-4ec8-96a7-ff096493a312', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-24 11:10:11.0761633' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'42363713-a8fd-4a2b-4840-08d63800218f', N'dbbd1826-3a3b-43da-8b60-1df263f852c9', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-24 11:12:07.5545723' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'fe424ed7-40d9-4cd1-4841-08d63800218f', N'41c83813-7914-410b-bb2d-d0758a45c160', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-24 11:15:57.5367736' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e5cc42d9-1945-4c3c-4842-08d63800218f', N'5661b687-fba2-4a62-898b-370cafe3826d', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-24 11:18:26.8429836' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'12ba3e6f-6fca-4702-4843-08d63800218f', N'5bb91980-d0f1-4c88-902c-2b33cf696a4d', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-24 11:22:08.0454078' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'1bb5c36f-b61b-492f-4844-08d63800218f', N'a4ee2331-ef0f-4fdc-b48f-b605fe65db50', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-24 11:26:58.9924928' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'4275ddba-9a87-4b91-4845-08d63800218f', N'b95557a1-589d-443f-a46c-03efc77c7fa1', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-24 11:32:47.1980685' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'ecc5fc4d-6287-4e46-4846-08d63800218f', N'ee543068-4569-473f-8066-a320cabb8f78', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-24 11:35:06.0962921' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'4cb1187a-0e85-43c4-4847-08d63800218f', N'65ffa717-7a87-4c4f-a7fa-a16230642b12', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-24 11:37:52.6533473' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'bbe03195-4da7-4ba6-4848-08d63800218f', N'70369370-1415-4bd5-94df-4249e27642ea', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-24 11:38:39.5278342' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'a362c15d-dc7f-4538-4849-08d63800218f', N'b361ee83-10d5-466e-a7ae-bc42a4a926f5', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-24 11:40:00.8331118' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'79c86f69-a9f3-4d8f-484a-08d63800218f', N'cc0989c9-448d-4efb-8b4d-7965068e14ab', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-24 11:42:33.4407326' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'b6986a8a-9228-483d-484b-08d63800218f', N'd2292f2b-1d61-4bcc-b6c1-bc4761197b74', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-24 11:43:22.4789975' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'816269a5-968a-43fb-484c-08d63800218f', N'2d10fb70-edb4-4a37-ace0-6441e75febd6', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-24 11:48:55.6199809' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'2ae5e218-aaec-41a5-484d-08d63800218f', N'f12d40b8-e83d-4e52-ae74-9341164dac7c', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-10-24 13:07:59.3662091' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e4aa4b96-9284-41d1-484e-08d63800218f', N'816f6afd-2181-4da3-80ac-4fc8672b7618', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-24 13:38:40.7003004' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'9a8f9cc3-c8c9-47bc-484f-08d63800218f', N'646d532c-3580-4e38-ab63-a8cb68c92ac2', N'6147a8b9-6fdb-4c74-8d47-bf8e945ae7e0', CAST(N'2018-10-24 13:47:09.1766895' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'741bb6c6-5206-477c-4850-08d63800218f', N'c030910f-034d-407b-9d77-567c9cb1449b', N'6147a8b9-6fdb-4c74-8d47-bf8e945ae7e0', CAST(N'2018-10-24 13:58:40.9087165' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'dbe7f3f0-a4ba-46b4-4851-08d63800218f', N'36cd342c-94ce-414b-bd65-869c7b232d07', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-24 14:28:11.4471901' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'b93496b7-2c6b-44ef-4852-08d63800218f', N'4ba18a6f-f804-4458-adc7-55a78a7c44eb', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-10-24 14:30:55.6168687' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'17120d76-1a15-497c-4853-08d63800218f', N'ab86d85e-dc8f-4240-ba5a-443e53202fbb', N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-10-24 14:34:24.5069176' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'c14db3f0-a2e4-482d-4854-08d63800218f', N'ce2fffb8-dc7d-4d73-9eac-056ba1d33954', N'6147a8b9-6fdb-4c74-8d47-bf8e945ae7e0', CAST(N'2018-10-24 14:41:39.7183246' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'76a0d205-b144-41fe-4855-08d63800218f', N'28cd5ae4-539c-4325-ae40-2a838b025968', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-10-24 14:51:12.6605454' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'2934ecbf-8bd2-497e-4856-08d63800218f', N'18815fd0-19f2-461c-a6d0-c5e421ca3c3b', N'cbfd4633-8d9b-49e0-890d-ba9125021cf3', CAST(N'2018-10-24 14:55:49.7043549' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'0f19729e-b69e-44fe-4857-08d63800218f', N'680aeb19-91d3-4a78-b47d-20b7a455dc07', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-10-24 14:58:13.7970484' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'01275c1b-c243-45fb-4858-08d63800218f', N'9620f8fa-6c4e-4b64-845a-2083dfc01203', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-10-24 15:01:24.2202084' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'98ed076e-f25f-4425-4859-08d63800218f', N'c6b548b9-04fe-493f-8c94-86d201d1a2b7', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-10-24 15:07:37.3447343' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'5801db21-d533-4e37-485a-08d63800218f', N'38b4eb6f-9ad7-4e3c-a8e6-adf9e3c87db2', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-10-26 09:13:29.9331343' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'566fae92-3ad4-4095-485b-08d63800218f', N'2a83052b-7b67-4489-be6f-5d1cce048de2', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-10-26 09:21:39.7918291' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'29528a32-f69f-4afd-485c-08d63800218f', N'4c3b518c-3c3e-41f8-b8e0-395fd06c429c', N'b16e7ca0-d372-4a9b-b373-254d9fb18cdf', CAST(N'2018-10-26 09:47:01.4765728' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'896ec8ae-06ab-4884-485d-08d63800218f', N'0e6dc22a-2809-4fa3-96ef-0c06e79d58c7', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-10-26 10:01:27.1284835' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'78f036ce-aa49-47db-485e-08d63800218f', N'bb5a5106-366f-4d48-825e-7b955e0ddbef', N'f81ce861-d5fc-4639-834b-5328ee3fd853', CAST(N'2018-10-26 10:08:07.4934704' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'ad718375-f528-475a-4860-08d63800218f', N'd09844d0-5d78-46ec-bb2f-3e2919f437b2', N'702b6312-0f84-465e-8f79-f62bf9d7da8a', CAST(N'2018-10-26 10:32:56.9675839' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'9c6b6ed8-5d88-4088-4861-08d63800218f', N'2c3f3461-42a7-4573-8fe2-b7709c7ca6d9', N'316ba3d9-9d60-44d7-b58a-98ad990179f3', CAST(N'2018-10-26 10:36:35.3895559' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3116a1c9-6d91-4616-9e17-0a89dfc2239b', N'998ff2f4-12fe-490d-9b88-8ec3596c7c3a', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.7900000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'4add92dd-4865-4420-8b66-0d077c61e9a3', N'6b851f11-d291-4635-90f0-da6abb3b8a7f', N'52f71900-4746-427b-9d5d-95057bf5b6cd', CAST(N'2018-09-12 21:00:13.8833333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'4d79f01f-751e-42ab-be97-1088fc62c056', N'af876821-7ff5-41cd-b652-93408caf3e25', N'52f71900-4746-427b-9d5d-95057bf5b6cd', CAST(N'2018-09-12 21:00:14.2300000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3677aa42-3293-439a-95e9-10d43af211f3', N'6b4399b3-c815-44a9-9575-c1606df0ecb9', N'5901bd92-07bc-483d-a7c0-f050c22bb052', CAST(N'2018-09-12 21:00:13.3700000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'caf156ac-10b7-416c-bb22-124bc7aec080', N'310e47f5-7ee0-4921-8de3-cd61e4b3839e', N'5901bd92-07bc-483d-a7c0-f050c22bb052', CAST(N'2018-09-12 21:00:13.3066667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'1ab8e6fb-9df9-4b7a-aaec-1532e84765ce', N'fe2ec5b2-57a7-4c57-852d-1227a87f7878', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.8200000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'f8cba2e3-a29f-43b3-8b7e-1e346df1fef2', N'fe355298-80ff-4f8f-90d0-e7ba696094bd', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-09-12 21:00:13.6966667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'0d654160-da64-4c5b-bb69-2fd3fc4bfcef', N'd1019175-0918-4877-a18a-98482baf0c79', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.7300000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'9dc03872-af26-4bf8-b2bb-330422fe7d4f', N'7fc9ac21-da35-4049-951d-7cb97ed054a4', N'b16e7ca0-d372-4a9b-b373-254d9fb18cdf', CAST(N'2018-09-12 21:00:14.1200000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'ee811fec-3400-4aca-a5a8-33469393015b', N'2d684f77-a444-4b3e-9138-e800fd2d9375', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:14.1033333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e188d0c1-592b-4908-9389-35037a89206a', N'9ee111b4-3d39-491c-9d94-c2cc6564d3d5', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-09-12 21:00:13.8833333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'16f1de8d-f5d7-4594-804c-366ef26477fc', N'cce1cc64-7386-44a3-b66c-adb372b42cb0', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.7600000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'cafb1f04-6c45-4007-91c5-3a80f6e76601', N'd2337f94-9b2f-416a-9ddb-8ae31e5d7447', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.8833333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'bb0b72d1-5a81-4752-8412-3ed91e7cf895', N'f3cc5abb-54b9-481e-a8de-1b00af6e584f', N'5baa353a-9a5d-4e4a-86fb-85e5c9cc995c', CAST(N'2018-09-12 21:00:13.9466667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'169b5b8b-35cb-4b6a-a2ef-40d94789e40a', N'1c58cc2a-04c8-4f26-a0c7-89c0c930c9bc', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.3366667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'beca82a4-e8fb-4b85-adbe-41ff35169472', N'1f65dfc1-45be-4c9d-b8c0-aa56b7b6d057', N'52f71900-4746-427b-9d5d-95057bf5b6cd', CAST(N'2018-09-12 21:00:13.7300000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3efd8057-d602-416a-86ec-43d2aa5393ba', N'975df969-e173-472d-a341-7e90310de5cd', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-09-12 21:00:14.0700000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'472a048f-59b9-4628-9131-459a768005e9', N'4c9556a9-29c6-4aac-adaa-d4797343d540', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-09-12 21:00:14.0866667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'6d2eef52-0d69-4c6d-aa78-46149ecad28d', N'f65d7f6b-343c-4610-8ac7-fd0c7b6a4005', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:14.0233333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'1c607a55-9f6f-48a5-a8f3-4772258937f6', N'f615c7eb-8dd9-4cc3-b7ec-c16f0d32a7d6', N'c21c2b32-81da-483c-b54d-6078969767af', CAST(N'2018-09-12 21:00:13.6966667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'5a862b7f-b218-483e-b2c2-484c0d480cbe', N'33dea8ac-b9f5-45fe-bd38-431a35cf53a9', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-09-12 21:00:13.9633333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'0ed82c4d-dc88-4b65-83b3-4b6ad2b25be1', N'd8eee229-78a7-4c25-b482-9ac2d196ac75', N'cf5ed313-069a-4b0f-b7e5-7b17d66874f7', CAST(N'2018-09-12 21:00:13.4300000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'acb73a14-30dd-46d7-93ec-4fc897d63079', N'bc0d8304-bb76-4dc2-897d-1ca621e341e7', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-09-12 21:00:13.4800000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'0d05db3d-e52e-40f9-bb17-5052ccd4f1f0', N'dc6991ff-87c0-49ea-820d-b91d8463a3ed', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-09-12 21:00:13.4000000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'9cbc53cb-7bb2-4a64-9b84-5219dde946d0', N'f75bb39f-6074-4a9b-823e-eefe3f2aaa3a', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:14.1200000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'c48cd25d-ae0b-4e20-a74a-56c0c6a3a515', N'164fabe1-1c19-4b74-8115-2dd086680e3c', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.6500000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'9efba43d-8a42-4fad-91ca-57414f757828', N'69fab7f6-d9e8-45b5-8763-ea0498c0e317', N'cbfd4633-8d9b-49e0-890d-ba9125021cf3', CAST(N'2018-09-12 21:00:13.3366667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'81e4cd39-ca7f-4d58-a666-5fffe367b718', N'69e81ef1-4d65-4560-8f03-e6cc48c209cb', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:14.2300000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'f3d33ba8-9de2-4f39-b2d0-60bb2512aa22', N'aedcf327-e408-4157-8aa9-3c328bd64b08', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.6500000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'dfc209ee-958b-4745-8280-6680a37fce08', N'583d634b-3d63-45dd-abff-6bf404580020', N'cbfd4633-8d9b-49e0-890d-ba9125021cf3', CAST(N'2018-09-12 21:00:13.8366667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'8d1fda6f-21c0-49fc-837e-67091baf1ba6', N'6ad88269-4ffc-4dbc-a5a3-9103ed926504', N'c21c2b32-81da-483c-b54d-6078969767af', CAST(N'2018-09-12 21:00:13.4300000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'c85e2679-2669-49cc-b850-6960d1660c6f', N'3379e3cf-c038-49a8-8efb-1e1900412337', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.6500000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'eb983c25-2321-4b19-ac03-6c72bdcaa867', N'a137d7dc-1f8b-4585-a9ea-aedf35616748', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.7600000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'5fb4fb72-ae16-4e2d-aea0-6f61a6b234a9', N'60155f39-5532-41b4-b0d8-aee0acaf77ae', N'a734d686-2800-49d6-a2eb-3657ee4583e7', CAST(N'2018-09-12 21:00:13.8200000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'5738be7d-ebda-47bd-a41d-72b4ca749cbe', N'f148fb24-0615-46d8-bceb-212ab7b24fe3', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-09-12 21:00:13.3066667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'7973ee6c-36a7-400b-a95f-732f7c0f9df4', N'f131bd78-a500-43bf-ac11-76a6c5c8db4e', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-09-12 21:00:13.2733333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3ba6e1f2-906d-4ed3-831d-742ab2bde4b1', N'3f79fe4c-967e-423f-ae82-487420ffbad7', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-09-12 21:00:13.9633333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'0cbac509-ce97-4bd0-b4d9-7e1449ffe9ea', N'db415d72-eb01-41aa-8b5f-c6ac27e26441', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-09-12 21:00:14.0233333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'9ca2b414-b886-4901-963e-7f2fbefd979e', N'e5cd009b-151f-4f51-b064-4cd785048b7a', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:14.0700000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'8d391366-ffdc-492b-9c38-82a64355010c', N'e5441c81-2aa7-4fd5-bf93-c0fbc679408b', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.5700000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'89523c5c-ead7-4fbe-ad8e-850e5afee757', N'04c010c4-1a4d-4c7a-a303-41cf69f0ef46', N'f65b3254-46cb-494a-8a5a-1a831a009d33', CAST(N'2018-09-12 21:00:13.4633333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'dbcd34ee-e828-4ac9-9bfd-8c3ae4add5e9', N'177f0be4-5f80-4519-af10-0b45d0875743', N'b16e7ca0-d372-4a9b-b373-254d9fb18cdf', CAST(N'2018-09-12 21:00:13.8200000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'2afc0e93-becc-4d71-8f3b-8c41a5e493d1', N'04c010c4-1a4d-4c7a-a303-41cf69f0ef46', N'5901bd92-07bc-483d-a7c0-f050c22bb052', CAST(N'2018-09-12 21:00:13.4466667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'1f823007-62d3-48d6-a284-8e641f8d8381', N'ba9ea71a-3ee9-4957-939b-25baed747e60', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.6333333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'620a8610-17a1-4369-a1dc-9108ebb49af9', N'9dd2b25b-0a01-4776-9ce4-8c24ac538ea6', N'0e72f62f-cdc3-427e-a7e8-b3883e137f26', CAST(N'2018-09-12 21:00:14.1800000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e88b4792-2424-4ecc-b64b-9154e4905d53', N'912fde1a-c9dd-4da7-a11f-6a4785d17399', N'15c32fcb-6e4a-49f7-b6dd-3d2f3d3aa6c7', CAST(N'2018-09-12 21:00:13.3700000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'cbe140c1-7911-4cba-aec1-92f3190f37c8', N'f7f12eb5-aa7e-4c28-8a73-24f9ebf2f52c', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-09-12 21:00:13.3366667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'893350bf-1782-4c77-b9d5-9485bd992680', N'84dd534b-1bf4-449f-9403-9e7f03db781f', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:14.1033333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'9a3df61a-ae72-409c-8578-996e0c651191', N'a52d8c18-a587-4dbd-8b5d-ba77ae54dec1', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-09-12 21:00:13.8366667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'0695cab8-7d6d-48e7-a968-9c18effe2f75', N'7a6f07a7-ee07-433a-8556-401fa20d2b10', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.7600000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'cd3d4ea4-2799-4844-ba9f-a15699c08567', N'14802bc1-aedf-4271-9672-f1281c9099f9', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.7133333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'69592ba8-500d-41fe-8f42-a3eb070cab0b', N'54ce8867-c2eb-468b-b2cd-f7f71cf2b3d8', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.7900000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'dabf4296-af05-4500-8b86-a860e415e0e2', N'ea92d77d-dfed-4caf-94d6-5df170e41002', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-09-12 21:00:13.6200000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'8abb73bb-3732-404c-931b-aaa854836ee4', N'3240417e-566e-44da-af13-cc3431e1f575', N'74a5bb6d-f734-4489-895c-341b932f3c75', CAST(N'2018-09-12 21:00:13.4300000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'9061d238-bddd-49c0-8743-ae28a943aa8e', N'03f9c025-5c07-4704-9beb-9e0ea3288350', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.7600000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'b2b6b75c-a4ae-42ea-b658-af3b28b29854', N'03e533b3-1f0a-4bb2-a7ed-2bff6c1c39af', N'b16e7ca0-d372-4a9b-b373-254d9fb18cdf', CAST(N'2018-09-12 21:00:13.2900000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'd75e9050-95b2-4925-b9df-b2faedf21b45', N'b0b588ee-4cee-42b6-96b6-3b2792388a8f', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-09-12 21:00:13.9933333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'8e69a413-e45e-4140-a203-b663a7400daa', N'ef6ef08d-0692-4323-aba5-0e0b6299a6b2', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.6666667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'279ae0cb-8d27-476d-ba7a-b73bff91f6ed', N'346e9150-01af-401d-8aae-d894cbff97fb', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.7300000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'7d2ea151-fd07-42a5-804e-c006c9d75cd8', N'1c7fab34-aeca-46c8-8cce-7d6805810c6d', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-09-12 21:00:13.5866667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e81393c3-47fc-4c32-b99f-c087dd3ab9d9', N'1e0a336f-039d-49ec-8383-387f2af96d50', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-09-12 21:00:14.0400000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'c7679ee1-6c10-4637-9c95-c09eb3e1c418', N'cfbf2cc1-4452-4371-bcb4-5bc3c3373a5a', N'fd728574-b579-4f32-b464-e754329f4edf', CAST(N'2018-09-12 21:00:14.1500000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'b1084ea2-a71f-4265-b9c7-c10ec88fabcf', N'3d2d9b6e-6cb9-46b1-b6b9-9c6d15572e5a', N'cbfd4633-8d9b-49e0-890d-ba9125021cf3', CAST(N'2018-09-12 21:00:13.3700000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'962c0c4e-bca3-4247-81c9-c8164c3d1347', N'6cd80b6d-5668-47c8-9a49-b44018970df4', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-09-12 21:00:14.1800000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'511f7d89-ddb9-4277-bad2-d0cc683a33ee', N'dfcd64dc-5bbd-48b5-bcd1-75393457c586', N'0e72f62f-cdc3-427e-a7e8-b3883e137f26', CAST(N'2018-09-12 21:00:13.6666667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'02079c6a-a2d1-4ab4-b362-d1bb607ad4e6', N'387bebf3-2e43-459d-bff5-f3be344ef811', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.6666667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'd5d8cfc8-ec67-471b-a118-d7fd306a9520', N'6ad88269-4ffc-4dbc-a5a3-9103ed926504', N'cf5ed313-069a-4b0f-b7e5-7b17d66874f7', CAST(N'2018-09-12 21:00:13.4000000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'65cfa3fb-4fcf-41ac-a27d-dafbe55a2d82', N'b6a294f4-c750-4d0f-bcb8-946aa98b8cc7', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-09-12 21:00:13.9466667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'2bf73f6d-57e9-4294-a2cf-dc082dc9b1d0', N'7a03b423-6faf-4fc5-9fae-e244225e8ab3', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.5400000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'8e47fd79-2585-458a-adf6-deb8bc815ad3', N'f967d89a-73a5-40b5-b05e-97e7ed7ee3a2', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.5400000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'962e091a-049e-4745-adc4-e218350eed87', N'5bd2e518-dc30-482e-8c54-c93a71d90253', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:14.0233333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'af467a86-ef68-442d-85ea-eafcdeee679c', N'180a0048-ff39-487c-8763-6d7af3e83cd6', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:14.1500000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'34186a3c-6790-4590-8150-efa8e4e27c35', N'4b402548-b272-462f-bf1c-e89475c823e5', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-09-12 21:00:14.0566667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'143e0ad3-4e64-4ed4-bc22-f04118eee8d5', N'6843f383-c21b-4233-bae6-dd6bec8c0e5b', N'5de9bfa8-4fd1-4afc-afb2-619c215355d6', CAST(N'2018-09-12 21:00:13.3200000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'd3731b8e-f3fb-46cb-bae7-f054e7c22fa3', N'e1376327-2c8f-4b92-9055-bd851b2e7c7e', N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 21:00:13.9933333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'fdb6752b-be1a-45d0-b9bc-f2d89615afdc', N'67650360-032d-4ba3-a178-d24da574e99f', N'cbfd4633-8d9b-49e0-890d-ba9125021cf3', CAST(N'2018-09-12 21:00:13.7900000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'3e7f00b8-6540-4f57-923c-f7a7028008f3', N'3aab0c5a-61da-415b-9868-f4d8a61a249e', N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-09-12 21:00:13.5566667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'58c17ede-b7f9-41a4-a618-fc18b6c0a867', N'37fd0884-a6af-41cf-9817-9587cdda4a48', N'5901bd92-07bc-483d-a7c0-f050c22bb052', CAST(N'2018-09-12 21:00:13.4800000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityClassifications] ([Id], [ActivityId], [ActivityTypeId], [DateCreated], [DateDeleted], [TypePriority]) VALUES (N'e423e406-8b4e-48df-ab50-ff5d487fb7cc', N'abc3e56a-b735-4998-91d8-1be2f92c34bf', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-09-12 21:00:14.1033333' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'c67dbc03-5c94-4d2b-8f65-00217b89701c', CAST(N'2018-09-12 20:56:58.6033333' AS DateTime2), NULL, N'Church/Spiritual hub', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'fd1559bb-75d9-4541-b329-098c5a1e5ad8', CAST(N'2018-09-12 20:56:49.9300000' AS DateTime2), NULL, N'Health checks at home', N'33f402bc-993e-4f82-9620-75bdef3ef824')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'e2e9f3b5-fc55-40ad-a65a-0cdfae98e969', CAST(N'2018-09-12 20:56:49.9933333' AS DateTime2), NULL, N'Health advice for chronic conditions (non-dementia)', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'ead47d98-11f4-4990-abb9-0ef6e8b25841', CAST(N'2018-09-12 20:56:58.7133333' AS DateTime2), NULL, N'Moving home/housing options', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'ae81b376-6e80-4a58-b0d9-0fae2028be16', CAST(N'2018-09-12 20:56:58.6966667' AS DateTime2), NULL, N'Retirement', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'41766753-9048-485c-ac9c-10b2ed0520a9', CAST(N'2018-09-12 20:56:49.9000000' AS DateTime2), NULL, N'Care & repair schemes (building/ handyperson)', N'33f402bc-993e-4f82-9620-75bdef3ef824')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'f65b3254-46cb-494a-8a5a-1a831a009d33', CAST(N'2018-09-12 20:56:50.0866667' AS DateTime2), NULL, N'Staying safe: scam avoidance & crime prevention', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'1af4302f-dcf2-4226-8af4-1c9164099b7e', CAST(N'2018-09-12 20:56:58.6800000' AS DateTime2), NULL, N'Advice and support', N'a4e9ccf5-2a02-465f-a96c-2bbfbad6ad6b')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'b16e7ca0-d372-4a9b-b373-254d9fb18cdf', CAST(N'2018-09-12 20:56:49.8833333' AS DateTime2), NULL, N'Transport assistance', N'3c2c353e-fb24-495e-a6e7-7421bc7e293c')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'29cffd9a-6e7d-4e8a-850e-26f3ae461b47', CAST(N'2018-09-12 20:56:50.0866667' AS DateTime2), NULL, N'Benefits & allowances advice', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'a4e9ccf5-2a02-465f-a96c-2bbfbad6ad6b', CAST(N'2018-09-12 20:56:49.8700000' AS DateTime2), NULL, N'Nutrition advice', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'28cbb35c-4a4e-490e-8b39-2dcffb71c743', CAST(N'2018-09-12 20:56:58.6966667' AS DateTime2), NULL, N'Hospital discharge', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'2eaeec2a-44c5-4ad5-91aa-2e692e7de533', CAST(N'2018-09-12 20:56:58.6200000' AS DateTime2), NULL, N'Drama', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'74a5bb6d-f734-4489-895c-341b932f3c75', CAST(N'2018-09-12 20:56:49.8700000' AS DateTime2), NULL, N'Health checks: eyes, ears, feet & hearing', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'a734d686-2800-49d6-a2eb-3657ee4583e7', CAST(N'2018-09-12 20:56:49.8833333' AS DateTime2), NULL, N'Help with shopping & other daily living', N'3c2c353e-fb24-495e-a6e7-7421bc7e293c')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'674e7447-2150-4046-8150-37156999efff', CAST(N'2018-09-12 20:56:58.6033333' AS DateTime2), NULL, N'Computer/Technology', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'15c32fcb-6e4a-49f7-b6dd-3d2f3d3aa6c7', CAST(N'2018-09-12 20:56:58.6333333' AS DateTime2), NULL, N'Mens', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'3acf5f76-7f78-4328-b709-3d86f1ea65b5', CAST(N'2018-09-12 20:56:38.7600000' AS DateTime2), NULL, N'7. Money and Benefits', N'00000000-0000-0000-0000-000000000000')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'9ef6d493-04fe-42b9-8212-4372a4940468', CAST(N'2018-09-12 20:56:49.9300000' AS DateTime2), NULL, N'Home adaptation services', N'33f402bc-993e-4f82-9620-75bdef3ef824')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'e28182d7-397f-4101-853d-4d2a41f4b068', CAST(N'2018-09-12 20:56:58.6333333' AS DateTime2), NULL, N'Rambling', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'94ee492f-91bf-46a5-b6a2-513b675a9ac9', CAST(N'2018-09-12 20:56:49.8366667' AS DateTime2), NULL, N'Support Groups & Counselling', N'1da7c6c2-2855-40e9-ba66-85b32216ad3f')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'f81ce861-d5fc-4639-834b-5328ee3fd853', CAST(N'2018-09-12 20:56:38.7433333' AS DateTime2), NULL, N'5. Safety', N'00000000-0000-0000-0000-000000000000')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'efef7b5e-0100-45cb-85cb-55960a00cad8', CAST(N'2018-09-12 20:56:58.6800000' AS DateTime2), NULL, N'Cookery classes', N'a4e9ccf5-2a02-465f-a96c-2bbfbad6ad6b')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'c21c2b32-81da-483c-b54d-6078969767af', CAST(N'2018-09-12 20:56:49.9800000' AS DateTime2), NULL, N'Home adaptation services (to prevent falls)', N'f81ce861-d5fc-4639-834b-5328ee3fd853')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'5de9bfa8-4fd1-4afc-afb2-619c215355d6', CAST(N'2018-09-12 20:56:58.6500000' AS DateTime2), NULL, N'Writing and Reading', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'bf4843c8-25c8-45ce-8e30-65a82131f417', CAST(N'2018-09-12 20:56:58.5866667' AS DateTime2), NULL, N'Choir/Music', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'686de044-613b-41f0-9c0e-67194fdecbc8', CAST(N'2018-09-12 20:56:49.9466667' AS DateTime2), NULL, N'Other help (library, prescription collection)', N'33f402bc-993e-4f82-9620-75bdef3ef824')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'37752042-c436-4d36-9025-6cba2b236635', CAST(N'2018-09-12 20:56:58.6500000' AS DateTime2), NULL, N'Chronic diseases (non-dementia)', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'3c2c353e-fb24-495e-a6e7-7421bc7e293c', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, N'3. Ability to Get Out & About', N'00000000-0000-0000-0000-000000000000')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'65a2d182-5533-4ceb-81b0-74753d506bf6', CAST(N'2018-09-12 20:56:50.1033333' AS DateTime2), NULL, N'Money saving tips (for the over 50s)', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'33f402bc-993e-4f82-9620-75bdef3ef824', CAST(N'2018-09-12 20:56:38.7433333' AS DateTime2), NULL, N'4. Practical Home Help', N'00000000-0000-0000-0000-000000000000')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'0dbba5d4-8729-4a88-b6a4-7753ddfa8774', CAST(N'2018-09-12 20:56:49.9466667' AS DateTime2), NULL, N'Safety technology at home (fall prevention, medication reminders, carer alerts)', N'f81ce861-d5fc-4639-834b-5328ee3fd853')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'cf5ed313-069a-4b0f-b7e5-7b17d66874f7', CAST(N'2018-09-12 20:56:58.6033333' AS DateTime2), NULL, N'Cinema', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'7387c9f3-30c9-49e2-8ebe-7ef6a9aa22a6', CAST(N'2018-09-12 20:56:49.9933333' AS DateTime2), NULL, N'Health advice on ears, eyes, feet', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'1da7c6c2-2855-40e9-ba66-85b32216ad3f', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, N'1. Friendships & Connections', N'00000000-0000-0000-0000-000000000000')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'5baa353a-9a5d-4e4a-86fb-85e5c9cc995c', CAST(N'2018-09-12 20:56:50.0866667' AS DateTime2), NULL, N'Carers support, advice & respite', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'97688174-7cd2-47c9-9ef9-87e72b21837a', CAST(N'2018-09-12 20:56:49.9800000' AS DateTime2), NULL, N'Computer/ IT advice schemes', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'78caf9c6-4e09-4f37-84d5-8b85be582168', CAST(N'2018-09-12 20:56:49.9166667' AS DateTime2), NULL, N'Computer support', N'33f402bc-993e-4f82-9620-75bdef3ef824')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'52f71900-4746-427b-9d5d-95057bf5b6cd', CAST(N'2018-09-12 20:56:49.8700000' AS DateTime2), NULL, N'Outdoor activity groups', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'88502886-2096-484d-8a7e-954f84465318', CAST(N'2018-09-12 20:56:49.8366667' AS DateTime2), NULL, N'Hobbies, clubs & societies', N'1da7c6c2-2855-40e9-ba66-85b32216ad3f')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'bf3745a9-a653-407d-b419-970a7e7e2079', CAST(N'2018-09-12 20:56:50.0566667' AS DateTime2), NULL, N'Health advice on diet, nutrition & weight management', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'316ba3d9-9d60-44d7-b58a-98ad990179f3', CAST(N'2018-09-12 20:56:49.9466667' AS DateTime2), NULL, N'Practical help (personal care & housework)', N'33f402bc-993e-4f82-9620-75bdef3ef824')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'7074d9d4-e547-46d9-bbec-a16a0ba6319b', CAST(N'2018-09-12 20:56:49.8533333' AS DateTime2), NULL, N'Fitness classes (general)', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'0b8dff55-23f8-4152-96c0-a5110e0d4a7e', CAST(N'2018-09-12 20:56:58.6200000' AS DateTime2), NULL, N'Discussion groups', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1', CAST(N'2018-09-12 20:56:49.9800000' AS DateTime2), NULL, N'Crisis point information & support (inc housing options & hospital discharge)', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'3851332c-52da-4b1e-bf51-a9d36232ff2a', CAST(N'2018-09-12 20:56:58.7133333' AS DateTime2), NULL, N'Diagnosis of chronic illness', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'39101bce-48d7-4b65-8314-adfe6a11a50b', CAST(N'2018-09-12 20:56:50.1033333' AS DateTime2), NULL, N'Insurance', N'3acf5f76-7f78-4328-b709-3d86f1ea65b5')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'0e72f62f-cdc3-427e-a7e8-b3883e137f26', CAST(N'2018-09-12 20:56:49.8366667' AS DateTime2), NULL, N'Lunch clubs', N'1da7c6c2-2855-40e9-ba66-85b32216ad3f')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'1a11fedd-1839-4b7f-bd68-b6e9ec5b5f18', CAST(N'2018-09-12 20:56:49.9633333' AS DateTime2), NULL, N'Home safety advice & support (crime prevention & buying online safely)', N'f81ce861-d5fc-4639-834b-5328ee3fd853')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'cbfd4633-8d9b-49e0-890d-ba9125021cf3', CAST(N'2018-09-12 20:56:49.8200000' AS DateTime2), NULL, N'Coffee Mornings and Afternoon Tea', N'1da7c6c2-2855-40e9-ba66-85b32216ad3f')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'418d5727-a393-47d5-8b4f-bf6c1bdbc090', CAST(N'2018-09-12 20:56:58.6333333' AS DateTime2), NULL, N'Women', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'6147a8b9-6fdb-4c74-8d47-bf8e945ae7e0', CAST(N'2018-09-12 20:56:58.6966667' AS DateTime2), NULL, N'Bereavement', N'6f7dfe61-9bb4-453f-a1a4-a56a58e9d6f1')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'bc6a506f-a178-4149-ba27-c3484adfe9bf', CAST(N'2018-09-12 20:56:49.9800000' AS DateTime2), NULL, N'Health advice for dementia', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'79721dc1-8a68-40f1-a136-c4cc35a4eb5e', CAST(N'2018-09-12 20:56:49.9466667' AS DateTime2), NULL, N'Meals on wheels', N'33f402bc-993e-4f82-9620-75bdef3ef824')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'4792bca8-4a31-44f5-a1e4-c576ac418d9e', CAST(N'2018-09-12 20:56:38.7300000' AS DateTime2), NULL, N'2. Keeping Healthy & Active', N'00000000-0000-0000-0000-000000000000')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'd76483ec-8202-426b-a300-caa90de25dc9', CAST(N'2018-09-12 20:56:58.6200000' AS DateTime2), NULL, N'Cookery classes', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'21d0215d-bd43-40d2-9453-d320e814bb7f', CAST(N'2018-09-12 20:56:50.0700000' AS DateTime2), NULL, N'Home adaptation schemes to prevent falls', N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'57b3c444-d3ba-45bb-9e81-d62ac71c21ab', CAST(N'2018-09-12 20:56:58.6500000' AS DateTime2), NULL, N'Counselling (general)', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'43445e2f-27c0-409c-a5fb-e011bbeef314', CAST(N'2018-09-12 20:56:58.6200000' AS DateTime2), NULL, N'Further educational', N'88502886-2096-484d-8a7e-954f84465318')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'7d5ec164-fa75-4d89-bc4c-e5c6811f30b7', CAST(N'2018-09-12 20:56:38.7433333' AS DateTime2), NULL, N'6. Useful Information', N'00000000-0000-0000-0000-000000000000')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'fd728574-b579-4f32-b464-e754329f4edf', CAST(N'2018-09-12 20:56:49.8533333' AS DateTime2), NULL, N'Fall prevention classes', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'043fd5bb-3a99-4eed-91bc-e87fe0f57085', CAST(N'2018-09-12 20:56:49.8833333' AS DateTime2), NULL, N'Safety advice: out & about', N'3c2c353e-fb24-495e-a6e7-7421bc7e293c')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'6df192d8-ac53-4edc-8959-e8d1930332a9', CAST(N'2018-09-12 20:56:49.8533333' AS DateTime2), NULL, N'Day trips out ', N'4792bca8-4a31-44f5-a1e4-c576ac418d9e')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'5901bd92-07bc-483d-a7c0-f050c22bb052', CAST(N'2018-09-12 20:56:58.6800000' AS DateTime2), NULL, N'Dementia', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'702b6312-0f84-465e-8f79-f62bf9d7da8a', CAST(N'2018-09-12 20:56:49.7433333' AS DateTime2), NULL, N'Befriending', N'1da7c6c2-2855-40e9-ba66-85b32216ad3f')
GO
INSERT [dbo].[ActivityTypes] ([Id], [DateCreated], [DateDeleted], [Description], [ParentId]) VALUES (N'ff487c2e-498a-436b-80e4-fe7f2591b137', CAST(N'2018-09-12 20:56:58.6800000' AS DateTime2), NULL, N'Ethnic Minority', N'94ee492f-91bf-46a5-b6a2-513b675a9ac9')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'5e4ab376-e494-4e1c-9cda-0053c72be9a9', N'CHS Healthcare', N'1 Wrens Court, 53 Lower Queen Street, Sutton Coldfield, West Midlands B72 1RT', NULL, N'hampshireplacements@chshealthcare.co.uk', NULL, 0, 0, N'0121 362 8840', 0, N'www.carehomeselection.co.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'f83fdeca-18c3-48ab-86ba-104f13b3c18b', N'Winchester and District Samaritans', N'13 Upper High Street
Winchester
Hampshire
SO23 8UT', NULL, N'jo@samaritans.org', NULL, 0, 0, N'116 123', 0, N'www.samaritans.org')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'a3a14c6e-eb1f-4aa8-b9eb-10b0f9bea7a1', N'Victim Support UK', N'Tel: 0808 168 9111
www.victimsupport.org.uk ', NULL, NULL, NULL, 0, 0, N'0808 168 9111', 0, N'www.victimsupport.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'770aa7d1-3ed4-4a12-8068-1608b30d9cdd', N'In Touch Home Improvement Agency', N'47-57 High Street
West End
Southampton
SO30 3DQ', NULL, N'N/A', NULL, 0, 0, N'02380 462444', 0, N'www.intouchsupport.co.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'958ec2df-808e-4be6-9d0f-17c46a3e81c3', N'Elderly Accommodation Counsel (EAC) ', N'89 Albert Embankment, Lambeth, London SE1 7TP', NULL, N'N/A', NULL, 0, 0, N'020 7820 1322', 0, N'www.eac.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'b206fe0f-9d63-49f5-ab34-188f29484bfb', N'Contact the Elderly', N'2 Grosvenor Gardens, London SW1W 0DH', NULL, N'N/A', NULL, 0, 0, N' 0800 716 543 ', 0, N'www.contact-the-elderly.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'9f291be7-f9a3-4142-ad9d-1a3eb9ed126f', N'Extend', N'2 Place Farm, Wheathampstead, Hertfordshire AL4 8SB', NULL, N'admin@extend.org.uk', NULL, 0, 0, N'01582 832 760 ', 0, N'www.extend.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'1b705a0d-36b1-4e3f-bfd1-1c0ce6ad22f4', N'ACE (Adults Continuing Education) Winchester', N'Stoney Lane, Weeke, Winchester', N'Nicky Morris', N'nmorris@psc.ac.uk', N'State', 1, 0, N'01962 886166', 0, N'www.psc.ac.uk/ace')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'bc4abd62-50a2-415d-b384-1e78b704b7b9', N'Disabled Living Foundation (DLF)', N'Unit 1, 34 Chatfield Road, Wandsworth, London SW11 3SE
', NULL, N'info@dlf.org.uk ', N'Charity', 0, 0, N'020 7289 6111', 0, N'www.dlf.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'f8089372-5a5d-4f22-b937-1f3ccde4dd4b', N'Winchester Baptist Church', N'Swan Lane
Winchester
SO23 7AA', N'adminmanager@winbap.org.uk', N'adminmanager@winbap.org.uk', NULL, 0, 0, N'01962 868 770', 1, N'www.winbap.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'f4b77647-7221-4596-868b-2331cbce7d4a', N'Abbeyfield', N'St Peter''s House, 2 Bricket Road,
St Albans, Hertfordshire AL1 3JW', NULL, N'enquiries@abbeyfield.com ', NULL, 0, 0, N'01727 857536', 0, N'www.abbeyfield.com')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'153164a8-3dac-4bbf-aa34-23d1299e2d05', N'The Salvation Army', N'Parchment Street
Winchester
SO23 8AZ', NULL, N'winchester@salvationarmy.org.uk', NULL, 0, 0, N'01962 854239', 1, N'www.salvationarmy.org.uk/winchester')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'30baf5a2-2a6a-4038-a429-28e397ce5e10', N'The Patients Association', N'The Patients Association, PO Box 935, Harrow, Middlesex, HA1 3YJ', NULL, N'mailbox@patients-association.com ', NULL, 0, 0, N'020 8423 8999 ', 0, N'www.patients-association.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'451b9902-50a8-4524-9952-29b894f13f95', N'St Barnabus Church ', N'Fromond Road, Winchester, SO22 6EF
', NULL, N'office@stb.church', NULL, 0, 0, N'01962 886629', 1, N'www.stb.church/senior-citizens/older-people')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'79ca50fe-d393-4c66-bd59-2a6f685280e2', N'Age UK Mid Hampshire', N'St George''s House, 18 St George''s St, Winchester SO23 8BG', NULL, N'enquiries@ageukmidhampshire.org.uk', NULL, 0, 0, N'01962 871700', 0, N'www.ageuk.org.uk/midhampshire')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'3fc795b5-fb87-41b4-9be0-2c6bf299bd52', N'Money Advice Service', N'120 Holborn, London EC1N 2TD', NULL, N'N/A', NULL, 0, 0, N'0800 138 7777', 0, N'www.moneyadviceservice.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'ba5d4210-43ff-4ecb-a5aa-304c8e4f1988', N'Bladder and Bowel Community', N'7 The Court, Holywell Business Park, Northfield Road
Southam CV47 0FS

', NULL, N' help@bladderandbowel.org', NULL, 0, 0, N'01926 357 220', 0, N'www.bladderandbowel.org')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'20386a91-5cab-42a3-9bd8-3153d82cdcef', N'Places for People Leisure Ltd', N'River Park Leisure Centre
Gordon Road
Winchester 
SO23 7DD', NULL, N'enquiries@riverparkleisurecentre.co.uk', NULL, 0, 0, N'01962 848700', 0, N'www.placesleisure.org.uk/centres/river-park-leisure-centre/')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'1318d97d-50b5-49e9-9370-390b748ccf60', N'Marie Curie', N'N/A', NULL, N'supporter.relations@mariecurie.org.uk', NULL, 0, 0, N'0800 090 2309', 0, N' www.mariecurie.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'90b536d1-20e5-49da-8f40-39a2f504d8e9', N'Independent Age', N'18 Avonmore Rd London W14 8RR', NULL, N'N/A', NULL, 0, 0, N'0800 319 6789 / 020 7605 4200', 0, N'www.independentage.org ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'6f067daf-4f71-48ee-80b1-3a99758f8da7', N'Winter Fuel Gov.uk', N'N/A', NULL, N'N/A', NULL, 0, 0, N'0800 731 0160', 0, N'www.gov.uk/winter-fuel-payment')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'f575185a-948d-4de0-9707-3b400ff4d721', N'Kings Worthy Community Centre', N'Tubbs Hall & Kings Worthy Community Centre, Fraser Rd, Kings Worthy, Winchester SO23 7PJ', NULL, N'clerk@kingsworthy-pc.org.uk', NULL, 0, 0, N'01962 884150', 0, N'http://www.kingsworthy-pc.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'5a53e37a-a8e1-4063-89a5-3d118efd0b67', N'StepChange Debt Charity', N'Wade House, Merrion Centre, Leeds LS2 8NG', NULL, NULL, NULL, 0, 0, N'0800 138 1111 ', 0, N'www.stepchange.org ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'6bff8c82-7bc6-42f3-a328-3db13a3730bd', N'The Silver Line', N'Trade Tower
Calico Row
London
SW11 3YH', NULL, NULL, NULL, 0, 0, N'0800 470 80 90', 0, N'www.thesilverline.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'e3f960ee-a32f-4105-b30e-4141e86a64e8', N'Home Instead', N'Construct House
Winchester Road
Alresford SO24 9EZ', NULL, N' jon.gapper@homeinstead.co.uk ', N'Private', 0, 0, N'01962 736681', 0, N'www.homeinstead.co.uk/centralhampshire')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'665e9bdc-e88b-4203-939a-44e64bf66ffd', N'The Pensions Advisory Service', N'11 Belgrave Rd, Pimlico, London SW1V 1RB', NULL, NULL, NULL, 0, 0, N' 0800 011 3797', 0, N'www.pensionsadvisoryservice.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'f35e3d70-2561-44d1-8da9-48ff811845fc', N'Dying Matters', N'44 Britannia St, London WC1X 9JG', NULL, N'N/A', NULL, 0, 0, N'0800 021 4466', 0, N'www.dyingmatters.org ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'69d0fa49-a537-4e3a-a0ff-49cc9d9d80c9', N'Mind ', N'Mind Infoline, Unit 9 Cefn Coed Parc Nantgarw, Cardiff CF15 7QQ', NULL, N'info@mind.org.uk', NULL, 0, 0, N'0300 123 3393 ', 0, N'www.mind.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'e55535f8-46c2-42ad-9aa3-4c2fc3caf85a', N'National Debtline', N'Tricorn House, 51-53 Hagley Rd, Birmingham B16 8TP', NULL, N'N/A', NULL, 0, 0, N'0808 808 4000 ', 0, N'www.nationaldebtline.org')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'98009f8c-4d78-4c15-ae8d-4d20f50f5ddb', N'Parkinson''s UK', N'215 Vauxhall Bridge Road, London SW1V 1EJ', NULL, N'hello@parkinsons.org.uk', NULL, 0, 0, N'0808 800 0303 ', 0, N'www.parkinsons.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'6ee5b521-ce7e-456c-90d3-5ab1274a8eb0', N'St Catherines View', N'212 Stanmore Lane, Winchester SO22 4BL', NULL, N'N/A', NULL, 0, 0, N'N/A', 0, N'N/A')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'b52ee183-65c9-4245-b9cd-5bdcbd504110', N'Andover Mind', N'Westbrook Close, South Street, Andover SP10 2BN', NULL, N'dementiaadvicewinchester@andovermind.org', NULL, 1, 0, N' 01264 332297', 0, N'www.andovermind.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'db91188a-6da9-46ad-b9a6-5c111f3a1f91', N'Silver Sunday', N'c/o Sir Simon Milton Foundation
5 The Strand (6th Floor)
London
WC2N 5HR', NULL, N'info@silversunday.org.uk', NULL, 0, 0, N'020 7641 3609', 0, N'www.silversunday.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'7cf76be9-d2f7-428e-98f1-5ca3fbc29f4a', N'Winchester Bereavement Society', N'Paternoster House, Colebrook St, Winchester SO23 9LG', NULL, N'help@winchesterbereavementsupport.org.uk', NULL, 0, 0, N'01962 863626', 0, N'www.winchesterbereavementsupport.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'ecf53e7b-3a89-4bc6-b189-6131c81e03d7', N'Office of the Public Guardian', N'PO Box 16185
Birmingham
B2 2WH', NULL, N'customerservices@publicguardian.gsi.gov.uk ', NULL, 0, 0, N'0300 456 0300 ', 0, N'www.gov.uk/government/organisations/office-of-the-publicguardian')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'f94acd69-ce18-4a87-bc3d-634c68ea7c06', N'AdviceUK', N'101E, Universal House, 88-94 Wentworth Street, London E1 7SA', NULL, N'mail@adviceuk.org.uk', NULL, 0, 0, N'0300 777 0107 / 0300 777 0108', 0, N'www.adviceuk.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'0509bd49-f4b4-4d22-a6b0-6446df863b71', N'Winchester & District Sports Association for the Disabled', N'River Park Leisure Centre, Gordon Road,
Hampshire, SO23 7DD', NULL, N'WADSADclub@gmail.com', NULL, 0, 0, N'01962 862516', 0, NULL)
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'2ef7b465-9715-4cca-90ac-648bef33ae59', N'ASB Help Charity', N'PO Box 699
York YO1 0GG', NULL, N'admin@asbhelp.co.uk', NULL, 0, 0, N'N/A', 0, N'www.asbhelp.co.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'0a0a505f-bbd9-41cd-95eb-64a92e9ac56f', N'Ombudsman Services', N'3300 Daresbury Park
Daresbury, Warrington
WA4 4HS', NULL, N'N/A', NULL, 0, 0, N'0330 440 1624 / 0333 300 1620', 0, N'www.ombudsman-services.org ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'29c2618a-b697-4f23-9c8c-65d8587060a4', N'Hampshire Cultural Trust', N'Hampshire Cultural Trust, Chilcomb House, Chilcomb Lane, Winchester
SO23 8R', NULL, N'enquiries@hampshireculturaltrust.org.uk', NULL, 0, 0, N'01962 678140', 0, N'www.hampshireculture.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'75d02cf8-27e8-413a-8d46-6649bd45c12c', N'Winchester Disabilities', N'68 The Winchester Centre St Georges Street Winchester SO23 8AH', NULL, NULL, NULL, 0, 0, N'01962 848016', 0, NULL)
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'09f039f4-f055-4564-aff7-69c58aa4b40e', N'St John''s Winchester', N'32 St John’s South, The Broadway, Winchester SO23 9LN', NULL, N'office@stjohnswinchester.co.uk handinhand@stjohnswinchester.co.uk', NULL, 0, 0, N'01962 854226', 0, N'www.stjohnswinchester.co.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'2627e3dc-242e-4d72-a951-6ded72f754f7', N'Integr8 ', N'Unit 12
Winnall Valley Road
Winchester
SO23 0LD', NULL, N'info@integr8movement.com', NULL, 0, 0, N'01962 807328', 0, N'www.integr8dance.com')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'7b781dc5-0d9c-401a-bff6-6f303297d227', N'The Olive Branch', N'The Olive Branch Christian Counselling Service Company Limited
 14 St. Clement Street
 Winchester
 SO23 9HH', NULL, N'secretary@theolivebranch.org.uk', NULL, 0, 0, N'01962 842858', 1, N'www.theolivebranch.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'7fb27ae9-640b-485c-92f7-71d5fa85e491', N'Thrive', N'The Geoffrey Udall Centre                              
Beech Hill                                                                          Reading, Berkshire RG7 2AT  ', NULL, N'info@thrive.org.uk', NULL, 0, 0, N'0118 988 5688 ', 0, N'www.thrive.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'348a9d72-a67d-44ee-82e0-74a9fb55015a', N'NHS ', N'NHS England, PO Box 16738, Redditch, B97 9PT', NULL, N'england.contactus@nhs.net', NULL, 0, 0, N'0300 311 22 33', 0, N'www.england.nhs.uk/contact-us')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'ab5150dd-db93-436b-809a-77782f3d2c17', N'National Insurance Contributions Office', N'N/A', NULL, N'N/A', NULL, 0, 0, N'0300 200 3500', 0, N'www.gov.uk/check-national-insurance-record')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'16eed828-91e1-4d67-af90-7aca0a0abf98', N'Driving Miss Daisy Ltd', N'18 The Slipway, Port Solent, Portsmouth PO6 4TR', NULL, N'winchester@drivingmissdaisy.co.uk', NULL, 0, 0, N'07552 331353', 0, N'www.drivingmissdaisy.co.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'152f3b3a-1adb-4b53-b956-7ba0f2d8ea54', N'Winston''s Wish', N'Winston’s Wish, Ventnor Villas, Hove, East Sussex BN3 3DD', NULL, N'info@winstonswish.org', NULL, 0, 0, N'01242 515 157', 0, N'www.winstonswish.org')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'b7c1e612-53a6-447a-87ef-8068b8257c95', N'Hope Church', N'Hope Church Winchester
The Middle Brook Centre
Middle Brook Street
Winchester
Hampshire
SO23 8DQ', NULL, N'office@hopewinchester.org ', NULL, 0, 0, N'01962 840800', 1, N'www.hopechurch.org')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'ffb5d36c-5f23-4111-8db4-84dfc68c48d4', N'Hampshire Fire and Rescue Service', N'Leigh Road, Eastleigh, Hampshire, SO50 9SJ', NULL, N' reception@hantsfire.gov.uk ', NULL, 0, 0, N'023 8064 4000', 0, N'www.hantsfire.gov.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'a4826f67-c8ef-4973-9a80-8977e6c42e77', N'Pension Service', N'The Pension Service 1
Post Handling Site B
Wolverhampton
WV99 1AL', NULL, N'N/A', NULL, 0, 0, N' 0800 731 7898 ', 0, N'www.gov.uk/state-pension ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'1468dc81-30ce-4963-bf27-8e26f76f8577', N'Winchester City Council', N'City Offices, Colebrook St, Winchester SO23 9LJ', NULL, N'customerservice@winchester.gov.uk', NULL, 0, 1, N'01962 840222', 0, N'www.winchester.gov.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'228019cd-dad5-436b-8cf6-8e499101ba58', N'Stroke Association', N'N/A', NULL, N'helpline@stroke.org.uk ', NULL, 0, 0, N'0303 303 3100 ', 0, N'www.stroke.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'3a0e0caa-ad01-4a77-b2cb-9424983861ce', N'Alzheimer''s Society', N'The Coach House, St Waleric Resource Centre, Park Road
Winchester SO23 7BE', NULL, N'winchester@alzheimers.org.uk', NULL, 0, 1, N' 01962 865 585', 0, N'www.alzheimers.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'4a0d42e0-40eb-4a75-a7ef-9546cf6c4bde', N'National Society of Allied and Independent Funeral Directors', N'3 Bullfields, Sawbridgeworth, Herts CM21 9DB', NULL, N'info@saif.org.uk', NULL, 0, 0, N' 0345 230 6777 / 01279 726 777 ', 0, N'www.saif.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'd71c3fe3-289d-48d9-bbf2-988e6dac63e5', N'TrustMark', N'Arena Business Centre, The Square, Basing View, Basingstoke
RG21 4EB', NULL, N'info@trustmark.org.uk', NULL, 0, 0, N'0333 555 1234 ', 0, N'www.trustmark.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'f5152061-fdc8-4618-8fb0-9953e33a0ed1', N'Winchester Area Access For All (WAAFA)', N'WAAFA c/o WACA
Winchester Discovery Centre
Jewry Street
Winchester
SO23 8SB', NULL, N'info@waafa.org.uk', NULL, 0, 0, N'01962 855 016', 0, N'www.waafa.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'd3927ade-1420-480c-b83f-99a207e50900', N'Macmillan Cancer Support', N'89 Albert Embankment, London SE1 7UQ', NULL, N'contact@macmillan.org.uk ', NULL, 0, 0, N'0808 808 00 00', 0, N'www.macmillan.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'425099ff-96eb-4ad2-8bee-99b9ea2f6230', N'British Heart Foundation', N'Lyndon Place, 2096 Coventry Road, Sheldon, Birmingham, B26 3YU', NULL, N'heretohelp@bhf.org.uk ', NULL, 0, 0, N'0300 330 3311', 0, N'www.bhf.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'8b8e9b3e-4e16-46bb-9222-a1af6eaf4297', N'Tax Help for Older People', N'Unit 10, Pineapple Business Park
Salway Ash Bridport
Dorset DT6 5DB', NULL, N'taxvol@taxvol.org.uk', NULL, 0, 0, N'0845 601 3321 or 01308 488066', 0, N'www.taxvol.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'21b9f466-c45a-48d2-ad0e-a2e0f2c604e6', N'Solicitors for the Elderly', N'Sue Carraturo
Studio 209, 
Mill Studio Business Centre, 
Crane Mead, Ware, 
Hertfordshire SG12 9PY', NULL, N'admin@sfe.legal', NULL, 0, 0, N'0844 567 6173 ', 0, N'www.sfe.legal ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'9d47ce9b-8c1f-4f31-b4d2-a4778493bb37', N'Hobbs Rehabilitation', N'Bridgetts Lane
Martyr Worthy
Winchester SO21 1AR', NULL, N'olderpeople@hobbsrehabilitation.co.uk ', NULL, 0, 0, N'01962 779796', 0, N'www.hobbsrehabilitation.co.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'b33cbe20-7add-49a2-92a2-aa7ce31d172d', N'Society of Genealogists ', N'14 Charterhouse Buildings
Goswell Road
London EC1M 7BA
UK', NULL, N'genealogy@sog.org.uk', NULL, 0, 0, N'020 7251 8799 ', 0, N'www.sog.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'3f1d2481-aa5c-4cec-be3a-ab26b081ce17', N'Foundations', N'The Old Co-Op Building, 11 Railway Street, Glossop, Derbyshire, SK13 7AG', NULL, N'info@foundations.uk.com / info@filt.uk.org', NULL, 0, 0, N'0300 124 0315 ', 0, N'www.foundations.uk.com ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'37fd957c-58b4-4fe3-bc35-abcc318c399c', N'Wessex Cancer Trust', N'91-95 Winchester Road
Chandler''s Ford
SO53 2GG', N'Jane Wooton', N'chandlersford.centre@wessexcancer.org.uk', NULL, 0, 0, N'02380 576576', 0, N'www.wessexcancer.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'ea55afce-d9fd-4bc6-8913-ac2b4bf4041d', N'Weeke Community Centre ', N'Taplings Road 
Weeke 
Winchester 
SO22 6HG ', NULL, N'info@weekecommunitycentre.co.uk', NULL, 0, 0, N'01962 888547', 0, N'www.weekecommunitycentre.co.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'de39aeb5-7133-4801-bc10-ac999c1085cd', N'Institute of Civil Funerals ', N'186a Station Road, Burton Latimer, Kettering, Northamptonshire NN15 5NT', NULL, N'info@iocf.org.uk', NULL, 0, 0, N'01480 861 411 ', 0, N'www.iocf.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'7f00bdd0-e34d-452f-8daa-af67fd9ec41c', N'Department for Disability Service Centre', N'If born after 8 April 1948: Disability Living Allowance, Warbreck House, Warbreck Hill Road, Blackpool FY2 0YE 
If born on/before 8 April 1948: Disability Living Allowance DLA65+, Mail Handling Site A, Wolverhampton, WV98 2AH ', NULL, N'N/A', NULL, 0, 0, N'0800 731 0122 / 0800 121 4600', 0, N'www.gov.uk/disability-benefits-helpline')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'da50147f-a4e6-4d8e-8232-b38dab5490d2', N'Shelter', N'N/A', NULL, N'N/A', NULL, 0, 0, N'0808 800 4444 ', 0, N'www.england.shelter.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'b6b1f000-24bd-4a97-9795-b6012af6990e', N'Best Foot Forward', N'Tubbs Hall, Fraser Road, Kings Worthy, Winchester', NULL, N'N/A', NULL, 0, 0, N'07462197058', 0, N'N/A')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'55c7842b-fc4f-4c9a-9e6b-b72935a8e3c1', N'Action Fraud', N'N/A', NULL, N'N/A', NULL, 0, 0, N'0300 123 2040 ', 0, N'www.actionfraud.police.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'f66c5783-436d-4231-ad7d-ba4b5970f3ef', N'The Compassionate Friends', N'Kilburn Grange, Priory Park Road, London NW6 7UJ', NULL, N'info@tcf.org.uk ', NULL, 0, 0, N'0345 120 3785', 0, N'www.tcf.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'6ea75ded-7262-4370-853d-bb3ef7153bae', N'Winchester Live at Home Scheme', N'Jewry Street, Winchester, SO23 8RZ


', NULL, N'Winchester.LiveAtHome@mha.org.uk', NULL, 0, 0, N'01962 890995', 0, N'www.mha.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'a960a657-09d6-4966-9d84-bb5e839877ff', N'Diabetes UK ', N'Wells Lawrence House, 126 Back Church Lane, London
E1 1FH', NULL, N'helpline@diabetes.org.uk', NULL, 0, 0, N'0345 123 2399 ', 0, N'www.diabetes.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'6a6c8ecd-24d4-43e7-8a32-bc1f436e22a3', N'Turn2Us Charity', N'200 Shepherds Bush Road, Hammersmith, W6 7NL', NULL, NULL, NULL, 0, 0, N' 0808 802 2000 ', 0, N'www.turn2us.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'e7103675-eafa-44e7-8926-bc387e3b1dbe', N'Hat Fair', N'Theatre Royal Winchester, Jewry Street, Winchester SO23 8SB', NULL, N'amy.pendry@hatfair.co.uk', NULL, 0, 0, N'01962 844 600', 0, N'www.hatffair.co.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'fa2d3f94-4812-48fc-93c3-be2edb84a1c5', N'National Osteoporosis Society', N'Camerton, Bath BA2 0PJ', NULL, N'nurses@nos.org.uk', NULL, 0, 0, N'0808 800 0035 ', 0, N'www.nos.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'3b584634-f995-4961-9a05-bf7c60a62955', N'Arthritis Care', N'Saffron House, 6-10 Kirby Street, London EC1N 8TS', NULL, N'info@arthritiscare.org.uk', NULL, 0, 0, N'0808 800 4050 / 0300 790 0400', 0, N'www.arthritiscare.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'5b94b2a2-42c2-4b52-8c4f-c069a40e8baa', N'Action on Hearing Loss', N'9 Bakewell Road, Peterborough, PE2 6XU  ', NULL, N'informationline@hearingloss.org.uk', NULL, 0, 0, N'0808 8080 123', 0, N'www.actiononhearingloss.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'502af31b-d09e-4a1d-98fe-c75caaa753d9', N'Resolver', N'56 Ayres St, London SE1 1EU', NULL, N'N/A', NULL, 0, 0, N'N/A', 0, N'www.resolver.co.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'a200e8d4-8d61-4811-a623-ceeacbb764fb', N'Community First', N'163 West Street
Fareham
PO16 0EF', NULL, N'admin@cfirst.org.uk.', NULL, 0, 0, N'0300 500 8085', 0, N'www.cfirst.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'318381d5-8ed2-43d2-8117-d1a6ed385a32', N'Citizens Advice Winchester District', N'The Winchester Centre 68 St George''s Street
Winchester SO23 8AH', NULL, N'advice@winchesterdistrictcab.org.uk', NULL, 0, 0, N' 03444 111306 / 01962 848003', 0, N'www.citizensadvice.org.uk/winchester-district')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'f283bad8-baca-4260-a862-d23a2e44d58f', N'Ramblers', N'Ramblers, 2nd Floor, Camelford Health, 87-89 Albert Embankment, Lamberth, London SE1 7TW ', NULL, N'walkingforhealth@ramblers.org.uk', NULL, 0, 0, N'01962 853640', 0, N'www.ramblers.org.uk/winchester')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'364535eb-8359-47d1-9af6-d521b7f109b3', N'University of the Third Age (U3A)', N'The Third Age Trust, 52 Lant Street, London SE1 1RB', NULL, NULL, NULL, 0, 0, N'020 8466 6139', 0, N'www.u3a.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'e7b6af90-e9d3-47a2-ac62-d9b54185d0aa', N'Healthwatch England ', N'Healthwatch England, National Customer Service Centre, Citygate, Gallowgate, Newcastle upon Tyne, NE1 4PA', NULL, N'enquiries@healthwatch.co.uk', NULL, 0, 0, N'0300 068 3000 ', 0, N'www.healthwatch.co.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'd6bec560-9707-46af-9857-dc5029149a86', N'Good Neighbours Network', N'GNN First Floor
Peninsular House
Wharf Road, Portsmouth
PO2 8HB', NULL, N'info@goodneighbours.org.uk', NULL, 0, 0, N'02392 899671 / 07827 925327', 0, N'www.goodneighbours.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'9ec6f994-dfa3-4da2-923d-e5d34a7ca7a3', N'Tubbs Hall Community Centre', N'Community Centre, Fraser Road, Kings Worthy, SO23 7PJ', N'', N'', N'', 0, 0, N'', 0, N'http://www.tubbshall.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'eceb054f-478c-4ac1-8e11-e6497405fcaa', N'Carer’s Allowance Unit', N'N/A', NULL, N'N/A', NULL, 0, 0, N'0800 731 0297', 0, N'www.gov.uk/carers-allowance-unit ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'c2273a27-08ae-49c6-91ef-e677e5a4bd64', N'National Preference Service', N'	Telephone Preference Service (TPS)
DMA House
70 Margaret Street
London
W1W 8SS', NULL, N'N/A', NULL, 0, 0, N'0345 070 0707 ', 0, NULL)
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'7506a37d-a4cb-4cca-8aa7-ea3c386289a9', N'AbilityNet', N'Microsoft Campus, Thames Valley Park, Reading, Berkshire RG6 1WG', NULL, N'enquiries@abilitynet.org.uk', NULL, 0, 0, N'0800 269 545', 0, N'www.abilitynet.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'02ed9535-9785-42cc-b57e-eaf40052acbf', N'Brendoncare', N'The Old Malthouse, Victoria Road, Winchester SO23 7DU ', NULL, N'N/A', NULL, 0, 0, N'01962 852133', 0, N'www.brendoncare.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'5788fcec-2508-4793-8a15-ebbc47a0bb30', N'Age Concern Hampshire', N'Second Floor, St George''s House
18 St George''s Street
Winchester
Hampshire
SO23 8BG', NULL, N'info@ageconcernhampshire.org.uk', NULL, 0, 0, N'0800 328 7154 / 01962 868545', 0, N'www.ageconcernhampshire.org.uk')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'7113521a-2cea-4eb2-84f4-ebd8c3e1ea3e', N'Oliver''s Battery Community and Parish', N' Olivers Battery Rd S, Winchester SO22 4EU', NULL, N'clerkoliversbattery@gmail.com ', NULL, 0, 0, N'N/A', 1, N'www.oliversbattery.info')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'1c4f6bf4-3096-41d8-b7a5-ec5b27436833', N'Princess Royal Trust for Carers in Hampshire', N'Charlton Road, Andover SP10 3LB', NULL, N'info@carercentre.com', NULL, 0, 0, N'01264 835246 ', 0, N'www.carers.org/partner/princess-royal-trust-carers-hampshire')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'6018ee18-f4d6-4747-a05a-ed937b221846', N'Disability Rights UK', N'Here East, 14 E Bay Ln, London E20 3BS', NULL, N'enquiries@disabilityrightsuk.org', NULL, 0, 0, N'0330 995 0400', 0, N'www.disabilityrightsuk.org ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'06b343a1-4ba7-41b3-a71f-f267cf10b553', N'Cruse Bereavement Care', N'PO Box 800, Richmond, Surrey TW9 1RG ', NULL, N'info@cruse.org.uk', NULL, 0, 0, N'0808 808 1677', 0, N'www.cruse.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'96ee7c0e-c78c-47f0-90f2-f3c525668f17', N'Samaritans', N'13 Upper High Street, Winchester, Hampshire, SO23 8UT', NULL, N'jo@samaritans.org', NULL, 0, 0, N'116 123', 0, N'www.samaritans.org')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'62164edd-606d-4f5a-9a87-f4c9ca33ee7f', N'Cinnamon Trust', N'10 Market Square, Hayle, Cornwall, TR27 4HE

', NULL, N'admin@cinnamon.org.uk', NULL, 0, 0, N'01736 757 900', 0, N'www.cinnamon.org.uk ')
GO
INSERT [dbo].[Organisations] ([Id], [Name], [Address], [Contact], [Email], [Funding], [Generic], [Noteworthy], [Phone], [Religious], [Website]) VALUES (N'983945f7-2757-47a9-9e50-f54e095cd762', N'Winchester Horticultural Society', N'Highfield Lodge, Winchester, S023 7AB', NULL, NULL, NULL, 0, 0, N'01962 865557 ', 0, N'www.winchesterhorticulturalsociety.org.uk')
GO
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (N'8458e355-e06b-47eb-bd74-f306f72c1a06', N'SuperAdmin')
GO
INSERT [dbo].[Users] ([Id], [DateCreated], [DateDeleted], [Email], [Name], [Password], [RoleId], [UserCompanyId], [DateUpdated]) VALUES (N'6cfdb4fd-884c-460c-9885-3d702c73fe7a', CAST(N'2018-10-05 07:22:18.1956748' AS DateTime2), NULL, N'wilkimatt@gmail.com', N'Matt', N'UKA/nNh6fYxrkfHnW+hRBSId1TTrvOOVpWoTTV7evbM=', N'8458e355-e06b-47eb-bd74-f306f72c1a06', N'09f039f4-f055-4564-aff7-69c58aa4b40e', CAST(N'2018-10-05 07:22:18.1956790' AS DateTime2))
GO
INSERT [dbo].[Users] ([Id], [DateCreated], [DateDeleted], [Email], [Name], [Password], [RoleId], [UserCompanyId], [DateUpdated]) VALUES (N'279d77d0-922a-40db-bb05-5ffc9c7ff681', CAST(N'2018-10-18 13:59:42.3733302' AS DateTime2), NULL, N'clive.cook@stjohnswinchester.co.uk', N'Clive Cook', N'RJUdd8PdyWicWxsWpJB2yJwvv1NDGbDks90UAMeJP60=', N'8458e355-e06b-47eb-bd74-f306f72c1a06', N'09f039f4-f055-4564-aff7-69c58aa4b40e', CAST(N'2018-10-18 13:59:42.3733317' AS DateTime2))
GO
INSERT [dbo].[Users] ([Id], [DateCreated], [DateDeleted], [Email], [Name], [Password], [RoleId], [UserCompanyId], [DateUpdated]) VALUES (N'840d5b1c-51e4-4a32-95b9-7ce92683c7e1', CAST(N'2018-10-18 09:13:31.6246407' AS DateTime2), NULL, N'marie.johnsonhall@stjohnswinchester.co.uk', N'Marie Johnson-Hall', N'zCW69yUdtHbRkJR3w4DiwoDkhnsShrtvBwFxhK5Tq7s=', N'8458e355-e06b-47eb-bd74-f306f72c1a06', N'09f039f4-f055-4564-aff7-69c58aa4b40e', CAST(N'2018-10-18 09:13:31.6246421' AS DateTime2))
GO
INSERT [dbo].[Users] ([Id], [DateCreated], [DateDeleted], [Email], [Name], [Password], [RoleId], [UserCompanyId], [DateUpdated]) VALUES (N'8fa3eab7-2709-42f9-82dc-8748566ea8fb', CAST(N'2018-10-18 09:12:08.2413710' AS DateTime2), NULL, N'julie.harman@stjohnswinchester.co.uk', N'Julie Harman', N'2HXC9h9R6zNV1WN7xPb/L3bFODpXBDiKHo5y+zO9MLQ=', N'8458e355-e06b-47eb-bd74-f306f72c1a06', N'09f039f4-f055-4564-aff7-69c58aa4b40e', CAST(N'2018-10-18 09:12:08.2413728' AS DateTime2))
GO
INSERT [dbo].[Users] ([Id], [DateCreated], [DateDeleted], [Email], [Name], [Password], [RoleId], [UserCompanyId], [DateUpdated]) VALUES (N'098ad3c8-3316-4c74-ba37-907668243581', CAST(N'2018-10-18 09:34:22.7789934' AS DateTime2), NULL, N'sarah.weekes@stjohnswinchester.co.uk', N'Sarah Weekes', N'/Ws1iyW2t4xzXK7RsdGhWalPIIVzMW9lh0ihQ7uvPxM=', N'8458e355-e06b-47eb-bd74-f306f72c1a06', N'09f039f4-f055-4564-aff7-69c58aa4b40e', CAST(N'2018-10-18 09:34:22.7789950' AS DateTime2))
GO
INSERT [dbo].[Users] ([Id], [DateCreated], [DateDeleted], [Email], [Name], [Password], [RoleId], [UserCompanyId], [DateUpdated]) VALUES (N'3459a547-4d80-42e7-b8b2-ba3d1f128071', CAST(N'2018-09-12 21:01:01.1200000' AS DateTime2), NULL, N'admin@gmail.com', N'Admin', N'O67i5n5A1rY8ZQaRkBZNEorMSeyOAHpk32pDZH7V57I=', N'8458e355-e06b-47eb-bd74-f306f72c1a06', N'09f039f4-f055-4564-aff7-69c58aa4b40e', CAST(N'2018-09-12 21:06:10.9166667' AS DateTime2))
GO
ALTER TABLE [dbo].[Activities]  WITH CHECK ADD  CONSTRAINT [FK_Activities_Organisations_OrganisationId] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisations] ([Id])
GO
ALTER TABLE [dbo].[Activities] CHECK CONSTRAINT [FK_Activities_Organisations_OrganisationId]
GO
ALTER TABLE [dbo].[Activities]  WITH CHECK ADD  CONSTRAINT [FK_Activities_Users_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Activities] CHECK CONSTRAINT [FK_Activities_Users_CreatedById]
GO
ALTER TABLE [dbo].[Activities]  WITH CHECK ADD  CONSTRAINT [FK_Activities_Users_UpdatedById] FOREIGN KEY([UpdatedById])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Activities] CHECK CONSTRAINT [FK_Activities_Users_UpdatedById]
GO
ALTER TABLE [dbo].[ActivityClassifications]  WITH CHECK ADD  CONSTRAINT [FK_ActivityClassifications_Activities_ActivityId] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[Activities] ([Id])
GO
ALTER TABLE [dbo].[ActivityClassifications] CHECK CONSTRAINT [FK_ActivityClassifications_Activities_ActivityId]
GO
ALTER TABLE [dbo].[ActivityClassifications]  WITH CHECK ADD  CONSTRAINT [FK_ActivityClassifications_ActivityTypes_ActivityTypeId] FOREIGN KEY([ActivityTypeId])
REFERENCES [dbo].[ActivityTypes] ([Id])
GO
ALTER TABLE [dbo].[ActivityClassifications] CHECK CONSTRAINT [FK_ActivityClassifications_ActivityTypes_ActivityTypeId]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Organisations_UserCompanyId] FOREIGN KEY([UserCompanyId])
REFERENCES [dbo].[Organisations] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Organisations_UserCompanyId]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Roles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Roles_RoleId]
GO
