
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Choir/Music', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Church/Spiritual hub', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Cinema', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Computer/Technology', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Cookery classes', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Discussion groups', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Drama', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Further educational', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Mens', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Rambling', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Women', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Writing and Reading', (select id from ActivityTypes where description = 'Hobbies, clubs & societies'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Chronic diseases (non-dementia)', (select id from ActivityTypes where description = 'Support Groups & Counselling'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Counselling (general)', (select id from ActivityTypes where description = 'Support Groups & Counselling'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Dementia', (select id from ActivityTypes where description = 'Support Groups & Counselling'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Ethnic Minority', (select id from ActivityTypes where description = 'Support Groups & Counselling'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Advice and support', (select id from ActivityTypes where description = 'Nutrition advice'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Cookery classes', (select id from ActivityTypes where description = 'Nutrition advice'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Hospital discharge', (select id from ActivityTypes where description = 'Crisis point information & support (inc housing options & hospital discharge)'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Bereavement', (select id from ActivityTypes where description = 'Crisis point information & support (inc housing options & hospital discharge)'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Retirement', (select id from ActivityTypes where description = 'Crisis point information & support (inc housing options & hospital discharge)'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Diagnosis of chronic illness', (select id from ActivityTypes where description = 'Crisis point information & support (inc housing options & hospital discharge)'),getdate())
insert dbo.ActivityTypes(id,description,parentId,dateCreated) values(newid(),'Moving home/housing options', (select id from ActivityTypes where description = 'Crisis point information & support (inc housing options & hospital discharge)'),getdate())

select 
	* 
from 
	ActivityTypesLevel1 l1 
	join ActivityTypesLevel2 l2 on l1.id = l2.parentid 
	join ActivityTypesLevel3 l3 on l2.id = l3.parentid
order by l1.description 

--select * from ActivityTypesLevel1
--select * from ActivityTypesLevel2
--select * from ActivityTypesLevel3






