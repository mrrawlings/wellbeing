--delete dbo.Organisations
--select * from Organisations



insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Age Concern Hampshire','add','phon','email','web','','','','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Age UK Mid Hampshire','St George`s House, 18 St George`s St, Winchester SO23 8BG','01962 871700','enquiries@ageukmidhampshire.org.uk','www.ageuk.org.uk/midhampshire','','','','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Winchester City Council','add','phon','email','web','','','TRUE','FALSE','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Age Concern Hampshire - East','add','phon','email','web','','','','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Age Concern Hampshire - West','add','phon','email','web','','','','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Best Foot Forward','Tubbs Hall, Fraser Road, Kings Worthy, Winchester','7462197058','none','none','','','','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Winchester City Council - Disabled facilities grant for Essential Home Adaptions','','01962 848455','disabledfacilitiesgrant@winchester.gov.uk','','','','','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Winchester live at Home Scheme','Jewry Street, Winchester, SO23 8RZ','01962 890995','','www.mha.org.uk','','','','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Alzheimer`s Society','The Coach House, St Waleric Resource Centre, Park Road, Winchester. SO23 7BE','01962 363393','sheila.ancient@alzheimers.org.uk (Local contact)','www.alzheimers.org.uk','','','TRUE','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Home Instead','','01962 736681','','www.homeinstead.co.uk/centralhampshire','','Private','','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'ACE (Adults Continuing Education) Winchester','Stoney Lane, Weeke, Winchester','01962 886166','nmorris@psc.ac.uk','www.psc.ac.uk/ace','Nicky Morris','State','','TRUE','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Tubbs Hall Community Centre','Community Centre, Fraser Road, Kings Worthy, SO23 7PJ','','','http://www.tubbshall.org.uk','','','','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Kings Worthy Community Centre','Community Centre, Kings Worthy ','','','http://www.kingsworthy-pc.org.uk','','','','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'St Catherines View','212 Stanmore Lane, Winchester SO22 4BL','','','','','','','','')
insert Organisations(Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious) values (NEWID(),'Princess Royal Trust for Carers in Hampshire','','01264 835246 ','info@carercentre.com','','','','','','')
