1. Restore Wellbeing Project Development\src\Database\Wellbeing20190521Fix.bak
2. Set Wellbeing.Data.Repository as startup project
3. Edit connection string in Wellbeing Project Development\src\Wellbeing.Data.Repository\WellbeingContextFactory.cs to restored db
4. Run update-database in Package Manager Console - should run one migration - "20190521184932_Sych-Model"

Note to self - add this index to live db on next update - done
            migrationBuilder.CreateIndex(
                name: "IX_Users_UserCompanyId",
                table: "Users",
                column: "UserCompanyId");


