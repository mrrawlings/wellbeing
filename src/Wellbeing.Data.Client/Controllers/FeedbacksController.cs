﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Wellbeing.Data.Client.Extensions;
using Wellbeing.Data.Client.Models;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Interfaces;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Client.Controllers
{
    public class FeedbacksController : Controller
    {
	    private readonly WellbeingDbContext _context;
	    private readonly INotificationService _notificationService;
	    private ILogger _logger;

		public FeedbacksController(WellbeingDbContext context, INotificationService notificationService, ILogger<FeedbacksController> logger)
		{
			_context = context;
			_logger = logger;
			_notificationService = notificationService;
		}

	    public async Task<IActionResult> Index()
        {
	        var feedbackVms = await _context.Feedbacks
		        .OrderByDescending(x => x.DateCreated)
		        .Select(x => new FeedbackViewModel
		        {
			        Id = x.Id,
					Comment = x.Comment,
					Contact = x.Contact,
					DateCreated = x.DateCreated,
					Email = x.Email,
					NotificationSent = x.NotificationSent,
					Recipients = x.Recipients
		        })
		        .ToListAsync();

	        return View(feedbackVms);
        }

	    // GET: Activities/Details/5
	    public async Task<IActionResult> Details(Guid? id)
	    {
		    if (id == null)
		    {
			    return NotFound();
		    }



		    var feedback = await  _context.Feedbacks.FirstOrDefaultAsync(x => x.Id == (Guid) id);

		    var feedbackVm = new FeedbackViewModel()
		    {
			    Id = feedback.Id,
			    Contact= feedback.Contact,
			    Email= feedback.Email,
			    Comment= feedback.Comment,
			    DateCreated = feedback.DateCreated,
		    };


		    return View(feedbackVm);
	    }


		// GET: Activities/Create
		public IActionResult Create()
		{
			var vm = new FeedbackViewModel();
			return View(vm);
		}


		// POST: Activities/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("Id,Contact,Email,Comment")] FeedbackViewModel vm, IFormCollection formCollection)
		{
			if (!ModelState.IsValid) return View(vm);

			var creationDate = DateTime.Now;

			var model = new Feedback()
			{
				Id = Guid.NewGuid(),
				Contact = vm.Contact,
				Email = vm.Email,
				Comment = vm.Comment,
				DateCreated = creationDate,
			};
			await _notificationService.NotifyOnFeedbackCreated(model);

			_context.Add(model);

			await _context.SaveChangesAsync();
			if (User.Identity.IsAuthenticated)
			{
				return RedirectToAction("Index", "Activities");
			}

			return RedirectToAction("Search", "Home");
		}
	}
}