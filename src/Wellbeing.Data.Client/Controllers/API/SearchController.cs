﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Wellbeing.Data.Client.Extensions;
using Wellbeing.Data.Client.Models;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Models;
using  Wellbeing.Data.Client.Models;
using Wellbeing.Data.Service.Interfaces;

namespace Wellbeing.Data.Client.Controllers.API
{
    [Produces("application/json")]
    [Route("api/search")]
    public class SearchController : Controller
    {
        private readonly WellbeingDbContext _context;
        private IActivityService _activityService;
        

        public SearchController(WellbeingDbContext context, IActivityService activityService)
        {
            _context = context;
            _activityService = activityService;
        }

        // GET: ActivityTypes
        public async Task<IActionResult> Index(SearchViewModel searchmodel)
        {
            // get only the root activity type for display
            

            return null;
        }
    
    }
}
