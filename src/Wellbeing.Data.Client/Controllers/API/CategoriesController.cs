﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Wellbeing.Data.Client.Extensions;
using Wellbeing.Data.Client.Models;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Client.Controllers.API
{
    [Produces("application/json")]
    [Route("api/categories")]
    public class CategoriesController : Controller
    {
        private readonly WellbeingDbContext _context;

        public CategoriesController(WellbeingDbContext context)
        {
            _context = context;
        }

        // GET: ActivityTypes
        public async Task<IActionResult> Index()
        {
            // get only the root activity type for display

            var activityTypeInfo = _context.ActivityTypes
                .Select(x=>new ActivityTypeInfo() { Id = x.Id, ParentId=x.ParentId, Description=x.Description, DateCreated=x.DateCreated, DateDeleted=x.DateDeleted })
                .ToList();
            ViewBag.ActivityTypeList = activityTypeInfo.BuildCheckboxList(null);

            return View(await ActivityTypeHelper.GetTypesInParentChildOrder(_context));
        }
    
    }
}
