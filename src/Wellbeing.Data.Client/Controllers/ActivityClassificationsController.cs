﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Client.Controllers
{
    [Authorize]
    public class ActivityClassificationsController : Controller
    {
        private readonly WellbeingDbContext _context;

        public ActivityClassificationsController(WellbeingDbContext context)
        {
            _context = context;
        }

        // GET: ActivityClassifications
        public async Task<IActionResult> Index()
        {
            return View(await _context.ActivityClassifications.ToListAsync());
        }

        // GET: ActivityClassifications/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activityClassifications = await _context.ActivityClassifications
                .SingleOrDefaultAsync(m => m.Id == id);
            if (activityClassifications == null)
            {
                return NotFound();
            }

            return View(activityClassifications);
        }

        // GET: ActivityClassifications/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ActivityClassifications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,TypePriority")] ActivityClassifications activityClassifications)
        {
            if (ModelState.IsValid)
            {
                activityClassifications.Id = Guid.NewGuid();
                activityClassifications.DateCreated = DateTime.Now;
                _context.Add(activityClassifications);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(activityClassifications);
        }

        // GET: ActivityClassifications/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activityClassifications = await _context.ActivityClassifications.SingleOrDefaultAsync(m => m.Id == id);
            if (activityClassifications == null)
            {
                return NotFound();
            }
            return View(activityClassifications);
        }

        // POST: ActivityClassifications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,TypePriority")] ActivityClassifications activityClassifications)
        {
            if (id != activityClassifications.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    activityClassifications.DateCreated = DateTime.Now;
                    _context.Update(activityClassifications);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ActivityClassificationsExists(activityClassifications.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(activityClassifications);
        }

        // GET: ActivityClassifications/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activityClassifications = await _context.ActivityClassifications
                .SingleOrDefaultAsync(m => m.Id == id);
            if (activityClassifications == null)
            {
                return NotFound();
            }

            return View(activityClassifications);
        }

        // POST: ActivityClassifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var activityClassifications = await _context.ActivityClassifications.SingleOrDefaultAsync(m => m.Id == id);
            _context.ActivityClassifications.Remove(activityClassifications);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ActivityClassificationsExists(Guid id)
        {
            return _context.ActivityClassifications.Any(e => e.Id == id);
        }
    }
}
