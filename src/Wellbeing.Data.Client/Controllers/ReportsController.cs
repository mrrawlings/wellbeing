﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Wellbeing.Data.Client.Models;
using Wellbeing.Data.Service.Interfaces;

namespace Wellbeing.Data.Client.Controllers
{
	public class ReportsController : Controller
	{
		private readonly IActivityService _activityService;
		private string _googleApiKey;

		public ReportsController(IActivityService activityService, IConfiguration configuration)
		{
			_activityService = activityService;
			_googleApiKey = configuration["GoogleMapsAPI"];
		}

		public IActionResult Index()
		{
			return Redirect("/Search");
		}

		[HttpPost]
		public IActionResult Print()
		{
			ViewBag.GoogleApiKey = _googleApiKey;

			var activitiesToPrint = new List<Guid>();
			foreach (var item in HttpContext.Request.Form)
			{
				if (item.Key.Contains("printId_"))
				{
					if (Guid.TryParse(item.Value, out var activityId))
					{
						activitiesToPrint.Add(activityId);
					}
				}
			}

			if (activitiesToPrint.Count == 0)
			{
				return RedirectToAction("Search", "Home");
			}

			var activities = _activityService.GetActivitiesToPrint(activitiesToPrint)
				.Select(x => new ActivityViewModel()
				{
					Id = x.Id,
					Title = x.Title,
					Description = x.Description,
					StatusId = x.StatusId,
					Contact = x.Contact,
					DateTimeDescription = x.DateTimeDescription,
					LocationDescription = x.LocationDescription,
					LocationPostcode = x.LocationPostcode,
					DateCreated = x.DateCreated,
					CreatedBy = x.CreatedBy,
					DateDeleted = x.DateDeleted,
					UpdatedBy = x.UpdatedBy,
					OrganisationId = x.OrganisationId,
					OrganisationName = x.OrganisationName,
					OrganisationWebsite = x.OrganisationWebsite,
					OrganisationWebsiteLink = x.OrganisationWebsiteLink
				}).ToList();


			return View(activities);

		}
	}
}