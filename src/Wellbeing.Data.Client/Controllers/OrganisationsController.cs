﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Client.Controllers
{
    public class OrganisationsController : Controller
    {
        private readonly WellbeingDbContext _context;

        public OrganisationsController(WellbeingDbContext context)
        {
            _context = context;
        }

        // GET: Organisations
        public async Task<IActionResult> Index()
        {
            ViewBag.UserCompany = Guid.Parse(User.FindFirst("CompanyOrganisationId").Value);

            return View(await _context.Organisations.OrderBy(x => x.Name).ToListAsync());
        }

        // GET: Organisations/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var organisation = await _context.Organisations
                .SingleOrDefaultAsync(m => m.Id == id);
            if (organisation == null)
            {
                return NotFound();
            }

            return View(organisation);
        }

        // GET: Organisations/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Organisations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious")] Organisation organisation)
        {
            if (ModelState.IsValid)
            {
                organisation.Id = Guid.NewGuid();
                _context.Add(organisation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(organisation);
        }

        // GET: Organisations/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var organisation = await _context.Organisations.SingleOrDefaultAsync(m => m.Id == id);
            if (organisation == null)
            {
                return NotFound();
            }
            return View(organisation);
        }

        // POST: Organisations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Name,Address,Phone,Email,Website,Contact,Funding,Noteworthy,Generic,Religious")] Organisation organisation)
        {
            if (id != organisation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(organisation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrganisationExists(organisation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(organisation);
        }

        // GET: Organisations/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var organisation = await _context.Organisations
                .SingleOrDefaultAsync(m => m.Id == id);
            if (organisation == null)
            {
                return NotFound();
            }
            var usersCount = await _context.Users.Where(e => e.UserCompany.Id == id).CountAsync();
            var activitiesCount = await _context.Activities.Where(e => e.Organisation.Id == id).CountAsync();
            ViewBag.NeedConfirmation = false;

            if (usersCount > 0)
            {
                ViewBag.CanDelete = false;
                ViewBag.Message = "There are users linked to this organisation so it is not possible to delete it.";
            }
            else if (activitiesCount > 0)
            {
                ViewBag.Message = "There are activities related to this organisation. Are you sure?";
                ViewBag.DeleteButtonText = "Confirm";
                ViewBag.CanDelete = true;
                ViewBag.NeedConfirmation = true;
            }
            else
            {
                ViewBag.DeleteButtonText = "Delete";
                ViewBag.CanDelete = true;
            }

            return View(organisation);

        }

        // POST: Organisations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var organisation = await _context.Organisations.SingleOrDefaultAsync(m => m.Id == id);
            var activities = await _context.Activities.Where(e => e.Organisation.Id == id)
                //.Select(e => e.Id)
                .ToListAsync();

            //remove references first
            foreach (var activity in activities)
            {
                var activityClassifications = await _context.ActivityClassifications
                    .Where(e => e.Activity.Id == activity.Id).ToListAsync();
                _context.ActivityClassifications.RemoveRange(activityClassifications);

                var activityHistories = await _context.ActivityHistories
                    .Where(e => e.Activity.Id == activity.Id).ToListAsync();

                _context.ActivityHistories.RemoveRange(activityHistories);
            }
            _context.Activities.RemoveRange(activities);

//            var users = await _context.Users.Where(e => e.UserCompany.Id == id).ToListAsync();
//            _context.Users.RemoveRange(users);

            _context.Organisations.Remove(organisation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrganisationExists(Guid id)
        {
            return _context.Organisations.Any(e => e.Id == id);
        }
    }
}
