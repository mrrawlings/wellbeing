﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ValueGeneration.Internal;
using Microsoft.Extensions.Logging;
using Wellbeing.Data.Client.Extensions;
using Wellbeing.Data.Client.Interfaces;
using Wellbeing.Data.Client.Models;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service;
using Wellbeing.Data.Service.Interfaces;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Client.Controllers
{
    [Authorize]
    public class ActivitiesController : Controller
    {
        private readonly WellbeingDbContext _context;
        private IActivityTypeService _activityTypeService;
        private IActivityService _activityService;
        private readonly INotificationService _notificationService;
        private IActivitiesDataCache _activitiesDataCache;
        IUserService _userService;
        ILogger _logger;

        public ActivitiesController(WellbeingDbContext context, IActivityTypeService activityTypeService, IUserService userService
            , IActivityService activityService, ILogger<AccountController> logger, IActivitiesDataCache activitiesDataCache, INotificationService notificationService)
        {
            _context = context;
            _activityTypeService = activityTypeService;
            _userService = userService;
            _activityService = activityService;
            _activitiesDataCache = activitiesDataCache;
            _notificationService = notificationService;
            _logger = logger;
        }

        // GET: Activities
        public async Task<IActionResult> Index(int page = 1)
        {
            var rowsToDisplay = 20;
            Guid? eventProviderFilter = null;
            Guid? dataProviderFilter = null;
            Guid? activityTypeFilter = null;
            int? statusFilter = null;
            string searchText = null;
            string[] keywords = null;

            _logger.LogDebug($"About to get activities data {DateTime.Now}");

            var activities = new List<WellbeingActivity>();
            if (Request.Query.Any())
            {
                if (!string.IsNullOrEmpty(Request.Query["search"]))
                {
                    searchText = Request.Query["search"];
                    var search = searchText ?? string.Empty;
                    keywords = search.Split(new char[] { ' ' });
                }

                if (!string.IsNullOrEmpty(Request.Query["eventProviderOrganisationFilter"])) 
                    eventProviderFilter = Guid.Parse(Request.Query["eventProviderOrganisationFilter"]);

                if (!string.IsNullOrEmpty(Request.Query["dataProviderOrganisationFilter"]))
                    dataProviderFilter = Guid.Parse(Request.Query["dataProviderOrganisationFilter"]);

                if (!string.IsNullOrEmpty(Request.Query["activityTypeFilter"]))
                    activityTypeFilter = Guid.Parse(Request.Query["activityTypeFilter"]);

                if (!string.IsNullOrEmpty(Request.Query["statusFilter"]))
                    statusFilter = int.Parse(Request.Query["statusFilter"]);

                // invoked by pager - build query from querystring
                activities = _activityService.GetActivities(keywords, eventProviderFilter, dataProviderFilter, statusFilter, activityTypeFilter);

            }

            _logger.LogDebug($"About to got activities data {DateTime.Now}");

            var userCompanyId = Guid.Parse(User.FindFirst("CompanyOrganisationId").Value);

            var activitiesVm = activities
                .Select(a =>
                    new ActivityViewModel()
                    {
                        Id = a.Id,
                        Title = a.Title,
                        DateTimeDescription = a.DateTimeDescription,
                        Contact = a.Contact,
                        StatusId = a.StatusId,
                        CreatedBy = a.CreatedBy,
                        DateCreated = a.DateCreated,
                        UpdatedBy = a.UpdatedBy,
                        DateUpdated = a.DateUpdated,
                        Description = a.Description,
                        OrganisationName = a.OrganisationName,
                        ActivityType = a.ActivityType,
                        UserCanEdit = a.UserOrganisationId == userCompanyId || User.IsInRole("SuperAdmin"),
                        PrimaryClassification = a.ActivityType == null
                            ? null
                            : ActivityTypeHelper.GetRootType(_context, a.ActivityType).Result
                    }
                );

            var orgList = new SelectList(_activitiesDataCache.Organisations, "Id", "Name");
            
            var activityTypeList = new SelectList(_activitiesDataCache.SelectListActivityTypeTree, "Value", "Text");

            var pagedData = activitiesVm.AsQueryable().GetPagedDataEntry(page, rowsToDisplay);
            pagedData.EventProviderOrganisationList = orgList;

            if (eventProviderFilter.HasValue)
                pagedData.EventProviderOrganisationFilter = eventProviderFilter.Value;

            if (dataProviderFilter.HasValue)
                pagedData.DataProviderOrganisationFilter = dataProviderFilter.Value;

            if (activityTypeFilter.HasValue)
                pagedData.ActivityTypeFilter = activityTypeFilter.Value;

            if (statusFilter.HasValue)
                pagedData.StatusFilter = statusFilter.Value;

            if (!string.IsNullOrEmpty(searchText))
            {
                pagedData.Search = searchText;
            }

            pagedData.DataProviderOrganisationList = orgList;
            pagedData.StatusList = new SelectList(ActivityHelper.GetStatusList(), "Value", "Text");
            pagedData.ActivityTypeList = activityTypeList;
            pagedData.HaveSearched = false;            

            return View(pagedData);
        }

        [HttpPost]
        public async Task<IActionResult> Index(string search, Guid eventProviderOrganisationFilter, Guid dataProviderOrganisationFilter, int? statusFilter, Guid activityTypeFilter, int page=1)
        {
            var rowsToDisplay = 20;
            _logger.LogDebug($"About to get activities data {DateTime.Now}");
            search = search ?? string.Empty;
            var keywords = search.Split(new char[] { ' ' });

            var activities = _activityService.GetActivities(keywords, eventProviderOrganisationFilter, dataProviderOrganisationFilter, statusFilter, activityTypeFilter);
            
            _logger.LogDebug($"About to got activities data {DateTime.Now}");

            var userCompanyId = Guid.Parse(User.FindFirst("CompanyOrganisationId").Value);

            var activitiesVm = activities
                .Select(a =>
                    new ActivityViewModel()
                    {
                        Id = a.Id,
                        Title = a.Title,
                        DateTimeDescription = a.DateTimeDescription,
                        Contact = a.Contact,
                        StatusId = a.StatusId,
                        CreatedBy = a.CreatedBy,
                        DateCreated = a.DateCreated,
                        UpdatedBy = a.UpdatedBy,
                        DateUpdated = a.DateUpdated,
                        Description = a.Description,
                        OrganisationName = a.OrganisationName,
                        ActivityType = a.ActivityType,
                        UserCanEdit = a.UserOrganisationId == userCompanyId || User.IsInRole("SuperAdmin"),
                        PrimaryClassification = a.ActivityType == null
                            ? null
                            : ActivityTypeHelper.GetRootType(_context, a.ActivityType).Result
                    }
                );

            var orgList = new SelectList(_activitiesDataCache.Organisations, "Id", "Name", eventProviderOrganisationFilter);

            var activityTypeList = new SelectList(_activitiesDataCache.SelectListActivityTypeTree, "Value", "Text",
                activityTypeFilter);

            var pagedData = activitiesVm.AsQueryable().GetPagedDataEntry(page, rowsToDisplay);
            pagedData.EventProviderOrganisationList = orgList;
            pagedData.DataProviderOrganisationList = orgList;
            pagedData.StatusList = new SelectList(ActivityHelper.GetStatusList(), "Value", "Text", statusFilter.ToString());
            pagedData.ActivityTypeList = activityTypeList;
            pagedData.HaveSearched = false;
            pagedData.EventProviderOrganisationFilter = eventProviderOrganisationFilter;
            pagedData.DataProviderOrganisationFilter = dataProviderOrganisationFilter;
            pagedData.StatusFilter = statusFilter;
            pagedData.HaveSearched = true;
            pagedData.ActivityTypeFilter = activityTypeFilter;
            pagedData.Search = search;

            return View(pagedData);
        }
        // GET: Activities/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activityTypeInfo = (_context.ActivityTypes.Select(x => new ActivityTypeInfo()
            {
                Id = x.Id,
                ParentId = x.ParentId,
                Description = x.Description,
                DateCreated = x.DateCreated,
                DateDeleted = x.DateDeleted
            }).ToList());


            var activity = _activityService.GetActivity(id.Value);

            var activityVm = new ActivityViewModel()
            {
                Id = activity.Id,
                Title = activity.Title,
                DateTimeDescription = activity.DateTimeDescription,
                Contact = activity.Contact,
                CreatedBy = activity.CreatedBy,
                DateCreated = activity.DateCreated,
                UpdatedBy = activity.UpdatedBy,
                DateUpdated = activity.DateUpdated,
                Description = activity.Description,
            };

            ViewData["Organisation"] = new SelectList(_activitiesDataCache.Organisations, "Id", "Name", activity.OrganisationId);

            var classifications = GetActivityClassifications((Guid)id);
            var cbList = activityTypeInfo.BuildCheckboxList(classifications);

            activityVm.ActivityTypeList = cbList;

            return View(activityVm);
        }

        // GET: Activities/Create
        public IActionResult Create()
        {
            ViewBag.Organisation = new SelectList(_activitiesDataCache.Organisations, "Id", "Name",0);
            var list = _context.ActivityTypes.ToList();
            ViewBag.ActivityTypes = list;

            var activityTypeInfo = _context.ActivityTypes.Select(x => new ActivityTypeInfo() { Id = x.Id, ParentId = x.ParentId, Description = x.Description, DateCreated = x.DateCreated, DateDeleted = x.DateDeleted }).ToList();

            var activitityViewModel = new ActivityViewModel();  
            
            var cbList = activityTypeInfo.BuildCheckboxList(null);

            activitityViewModel.ActivityTypeList = cbList;

            return View(activitityViewModel);
        }


        // POST: Activities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,Contact,DateTimeDescription,LocationDescription,LocationPostcode,OrganisationId,StatusId")] ActivityViewModel activityvm, IFormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                Organisation organisation = null;

                if (activityvm.OrganisationId != null)
                {
                   organisation = _context.Organisations.Where(x => x.Id == activityvm.OrganisationId).FirstOrDefault();
                }

                var currentUser = _userService.GetByEmail(HttpContext.User.Identity.Name);
                var creationDate = DateTime.Now;
                var user = new User() {Email = currentUser.Result.Email, Id = currentUser.Result.Id, Name = currentUser.Result.Name};


				var activity = new Activity() {
                       Id= Guid.NewGuid(),
                       Title = activityvm.Title,
                       Description= activityvm.Description,
                       Contact= activityvm.Contact,
                       DateTimeDescription = activityvm.DateTimeDescription,
                       LocationDescription = activityvm.LocationDescription,
                       LocationPostcode = activityvm.LocationPostcode,
                       DateCreated = creationDate,
                       CreatedBy = user,
                       DateUpdated =  DateTime.Now,
                       UpdatedBy = new User() { Email = currentUser.Result.Email, Id = currentUser.Result.Id },
                       Organisation = organisation,
                       Status =  activityvm.StatusId
				};


				var activityHistory = new ActivityHistory
				{
					Id = Guid.NewGuid(),
					Activity = activity,
					DateCreated = creationDate,
					CreatedBy = user,
					Description = "Created"
				};
				await _notificationService.NotifyOnActivityCreated(activityHistory);


				_context.Add(activity);
                _context.Add(activityHistory);

                var cbList = activityvm.ActivityTypeList;

                await _context.SaveChangesAsync();

                var selectedCbDict = formCollection.Keys.Where(k => k.StartsWith("cb_")).ToDictionary(k => k, k => Convert.ToBoolean(formCollection[k].ToString().Split(',')[0])).Where(x => x.Value == true);
                var cbFormValues = selectedCbDict.Select(x => new CheckboxListItem() { Id = x.Key, Display = string.Empty, IsChecked = true }).ToList();

                if (cbFormValues.Count>0 ) await SaveActivityTypeAsync(activity.Id, cbFormValues);

                return RedirectToAction(nameof(Index));
            }
            ViewData["Organisation"] = new SelectList(_context.Organisations, "Id", "Name", activityvm.OrganisationId);
            var list = _context.ActivityTypes.ToList();
            ViewBag.ActivityTypes = list;

            return View(activityvm);
        }

       
        // GET: Activities/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activityTypeInfo = (_context.ActivityTypes.Select(x => new ActivityTypeInfo() {
                                     Id = x.Id,
                                     ParentId = x.ParentId,
                                     Description = x.Description,
                                     DateCreated = x.DateCreated,
                                     DateDeleted = x.DateDeleted }).ToList());//.BuildTree();

            var classifications = GetActivityClassifications((Guid)id);

            var cbList = activityTypeInfo.BuildCheckboxList(classifications);
            
            var activity = await _context.Activities
                .Where(m => m.Id.Equals(id))
                .Include(x=>x.Organisation)
                .Select(x=>new ActivityViewModel() {
                    Id= x.Id,
                    Title = x.Title,
                    Description = x.Description,
                    StatusId = x.Status,
                    Contact= x.Contact,
                    DateTimeDescription = x.DateTimeDescription,
                    LocationDescription = x.LocationDescription,
                    LocationPostcode = x.LocationPostcode,
                    DateCreated = x.DateCreated,
                    CreatedBy = x.CreatedBy,
                    DateDeleted = x.DateDeleted,
                    UpdatedBy  = x.UpdatedBy,
                    OrganisationId = x.Organisation.Id
                    
                })
                .SingleOrDefaultAsync(m => m.Id == id);


            if (activity == null)
            {
                return NotFound();
            }

            ViewData["Organisation"] = new SelectList(_context.Organisations, "Id", "Name", activity.OrganisationId);
            ViewData["Statuses"] = new SelectList (ActivityHelper.GetStatusList(), "Value", "Text",activity.StatusId.ToString());

            activity.ActivityTypeList = cbList;

            return View(activity);
        }

        // POST: Activities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Title,Description,Contact,DateTimeDescription,LocationDescription,LocationPostcode,OrganisationId,StatusId")] ActivityViewModel activityvm
            , IFormCollection formCollection)
        {
            if (id != activityvm.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    Organisation organisation = null;

                    if (activityvm.OrganisationId != null)
                    {
                        organisation = _context.Organisations.Where(x => x.Id == activityvm.OrganisationId).FirstOrDefault();
                    }

                    // updated date added for concurrency check
                    var activity = await _context.Activities.SingleOrDefaultAsync(m => m.Id == activityvm.Id);
                    //var activity = await _context.Activities.SingleOrDefaultAsync(m => m.Id == activityvm.Id && m.DateUpdated == activityvm.DateUpdated);
                    var identity = _userService.GetByEmail(HttpContext.User.Identity.Name);
                    var currentUser = _context.Users.FirstOrDefault(u => u.Email == identity.Result.Email);

                    if (activity != null)
                    {
	                    var updateDate = DateTime.Now;
	                    var wasApproved = activityvm.StatusId != activity.Status &&
	                                      (WellbeingActivity.ActivityStatus) activityvm.StatusId ==
	                                      WellbeingActivity.ActivityStatus.Approved;

						activity.Title = activityvm.Title;
                        activity.Description = activityvm.Description;
                        activity.Contact = activityvm.Contact;
                        activity.DateTimeDescription = activityvm.DateTimeDescription;
                        activity.LocationDescription = activityvm.LocationDescription;
                        activity.LocationPostcode = activityvm.LocationPostcode;
                        activity.Organisation = organisation;
                        activity.Status = activityvm.StatusId;
                        activity.DateUpdated = updateDate;
                        activity.UpdatedBy = currentUser;
                        _context.Update(activity);

						if(wasApproved)
						{
	                        var activityHistory = new ActivityHistory
	                        {
		                        Id = Guid.NewGuid(),
		                        Activity = activity,
		                        DateCreated = updateDate,
		                        CreatedBy = currentUser,
		                        Description = "Status Changed to Approved"
	                        };
	                        await _notificationService.NotifyOnActivityApproved(activityHistory);

							_context.Add(activityHistory);
                        }

                        await _context.SaveChangesAsync();

                        var selectedCbDict = formCollection.Keys.Where(k => k.StartsWith("cb_")).ToDictionary(k => k, k => Convert.ToBoolean(formCollection[k].ToString().Split(',')[0])).Where(x => x.Value == true);
                        var cbFormValues = selectedCbDict.Select(x => new CheckboxListItem() { Id = x.Key, Display = string.Empty, IsChecked = true }).ToList();

                        if (cbFormValues.Count > 0) await SaveActivityTypeAsync(activity.Id, cbFormValues);

                    }

                  
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ActivityExists(activityvm.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Organisation"] = new SelectList(_context.Organisations, "Id", "Name", activityvm.OrganisationId);
           
            var activityTypeInfo = (_context.ActivityTypes.Select(x => new ActivityTypeInfo() {
                                     Id = x.Id,
                                     ParentId = x.ParentId,
                                     Description = x.Description,
                                     DateCreated = x.DateCreated,
                                     DateDeleted = x.DateDeleted }).ToList());//.BuildTree();

            var classifications = GetActivityClassifications((Guid)id);


            var cbList = activityTypeInfo.BuildCheckboxList(classifications);
            activityvm.ActivityTypeList = cbList;


            return View(activityvm);
        }

        // GET: Activities/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activity = await _context.Activities
                .SingleOrDefaultAsync(m => m.Id == id);
            if (activity == null)
            {
                return NotFound();
            }

            return View(activity);
        }

        // POST: Activities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var activity = await _context.Activities.SingleOrDefaultAsync(m => m.Id == id);
            //_context.Activities.Remove(activity);

            //soft delete
            if (activity != null)
            {
                activity.DateDeleted = DateTime.Now;
                _context.Update(activity);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction(nameof(Index));
        }


        private bool ActivityExists(Guid id)
        {
            return _context.Activities.Any(e => e.Id == id);
        }

        private string[] GetActivityClassifications(Guid activityId)
        {
            var classifications = _context.ActivityClassifications
                .Where(x => x.Activity.Id == activityId)
                .Select(x => x.ActivityType.Id.ToString()).ToArray();
            return classifications;
        }

        //Save activity types
        private async Task SaveActivityTypeAsync(Guid activityId, List<CheckboxListItem> cbList)
        {
            // remove all the existing ones before adding new ones

            var activityClassifications =
                _context.ActivityClassifications.Where(x => x.Activity.Id == activityId).ToList();

            foreach (var classification in activityClassifications)
            {
                _context.Remove(classification);
                await _context.SaveChangesAsync();
            }

            foreach (var cbItem in cbList)
            {
                var activityTypeParts = cbItem.Id.Split("_");             

                for (int i = 1; i < activityTypeParts.Count(); i++)
                {
                    // only add last node - Shiju - I added this, to stop the whole hierachy being added..
                    if (i != activityTypeParts.Length-1)
                        continue;

                    // add only if it doesn't exist
                    var activityClassificationExists = _context.ActivityClassifications.Where(x => x.Activity.Id == activityId && x.ActivityType.Id == Guid.Parse(activityTypeParts[i])).FirstOrDefault();

                    if (activityClassificationExists == null)
                    {
                        var activityClassification = new ActivityClassifications();
                        activityClassification.Activity = _context.Activities.Where(x => x.Id == activityId).FirstOrDefault();
                        activityClassification.ActivityType = _context.ActivityTypes.Where(x => x.Id == Guid.Parse(activityTypeParts[i])).FirstOrDefault();
                        activityClassification.DateCreated = DateTime.Now;
                        _context.Add(activityClassification);
                        await _context.SaveChangesAsync();
                    }

                }
            }
        }


        public async Task<IActionResult> HistoryEdit(Guid? id)
        {
	        if (id == null)
	        {
		        return NotFound();
	        }

	        ViewBag.ActivityId = id.Value;
	        ViewBag.SourceActionEdit = true;
			var historyVms = await GetHistoryViewModels(id.Value);
	        return View("History", model: historyVms);
        }

        public async Task<IActionResult> HistoryDetails(Guid? id)
        {
	        if (id == null)
	        {
		        return NotFound();
	        }

	        ViewBag.ActivityId = id.Value;
	        ViewBag.SourceActionEdit = false;
	        var historyVms = await GetHistoryViewModels(id.Value);
	        return View("History", model: historyVms);
        }

        private async Task<List<ActivityHistoryViewModel>> GetHistoryViewModels(Guid id)
        {
	        var models = await _activityService.GetActivityHistories(id);

	        var historyVms = new List<ActivityHistoryViewModel>();
	        foreach (var model in models.OrderByDescending(m => m.DateCreated))
	        {
		        var vm = new ActivityHistoryViewModel
		        {
			        Id = model.Id,
			        CreatedByUser = model.CreatedByUser,
			        DateCreated = model.DateCreated,
			        Description = model.Description,
			        NotificationSent = model.NotificationSent,
			        Recipients = model.Recipients
		        };
		        historyVms.Add(vm);
	        }

	        return historyVms;
        }
	}
}
