﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Wellbeing.Data.Client.Models;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Service.Models;
using Wellbeing.Data.Client.Extensions;
using Wellbeing.Data.Service.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Wellbeing.Data.Client.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly WellbeingDbContext _context;
        private IActivityService _activityService;
        private string _googleApiKey;

        public HomeController(WellbeingDbContext context, IActivityService activityService, IConfiguration configuration)
        {
            _context = context;
            _activityService = activityService;
            _googleApiKey = configuration["GoogleMapsAPI"];
        }

        public IActionResult Search(int page = 1)
        {
        
                var activityTypeInfo = _context.ActivityTypes
                                               .Select(x => new ActivityTypeInfo() { Id = x.Id, ParentId = x.ParentId,
                                                                                     Description = x.Description,
                                                                                     DateCreated = x.DateCreated,
                                                                                     DateDeleted = x.DateDeleted })
                                               .ToList();
                ViewBag.ActivityTypeList = activityTypeInfo.BuildCheckboxList(null);
                ViewBag.OrganisationList = GetOrganisations();

			// get form 

			    var search = string.Empty;
                var categories = string.Empty;
                var categoryid = string.Empty;
                var cbList = new List<string>();
                var selectedNodeIds = new List<string>();
				var selectedOrganizationIds = new List<Guid>();
				var orgList = new List<string>();
				var allOrganizationsSelected = false;

                if (HttpContext.Request.Method.Equals("POST"))
                {
                    search = HttpContext.Request.Form["search"];
                    categories = HttpContext.Request.Form["categories"];
                    categoryid = HttpContext.Request.Form["categoryid"];   
                
                    foreach(var item in HttpContext.Request.Form)
                    {
                        if (item.Key.Contains("cb_"))
                        {
                            // add id of last part of the id of any selected nodes
                            selectedNodeIds.Add(item.Key.Replace("cb_", "").Split("_")[
                                item.Key.Replace("cb_", "").Split("_").Length - 1]);

                            cbList.Add(item.Key.Replace("cb_",""));

                        } else if (item.Key.Contains("org_"))
                        {
	                        var id = item.Key.Replace("org_", "");
	                        if (id == "AllId")
	                        {
		                        allOrganizationsSelected = true;
	                        } else if (Guid.TryParse(id, out var orgId))
	                        {
		                        selectedOrganizationIds.Add(orgId);
		                        orgList.Add(id);
	                        }
                        }
                    }

                }
                else
                {
                    foreach (var key in HttpContext.Request.Query.Keys)
                    {
                        if (key == "search")
                        {
                            search = HttpContext.Request.Query["Search"];
                        }
                        if (key == "categories")
                        {
                            categories = HttpContext.Request.Query["categories"];
                        }
                        if (key == "categoryid")
                        {
                            categoryid = HttpContext.Request.Query["categoryid"];
                        }

                        if (key.Contains("cb_"))
                        {
                            cbList.Add(key.Replace("cb_", ""));
                        }

                        if (key.Contains("org_"))
                        {
							var id = key.Replace("org_", "");
							if (Guid.TryParse(id, out var orgId))
							{
								selectedOrganizationIds.Add(orgId);
								orgList.Add(id);
							}

						}
					}
                }
            
            //on pagination link
            if(selectedNodeIds.Count()==0 && !string.IsNullOrEmpty(categories))
            {
                foreach(var cat in categories.Split(new char[] { ',' })){
                    selectedNodeIds.Add(cat);
                }
            }

            var categoryids = string.Join(",", selectedNodeIds).Split(new char[] { ',' });
            var keywords = search.Split(new char[] { ' ' });

            var activities = new List<WellbeingActivity>().AsEnumerable();
            ViewBag.initialLoad = false;
            // only run search for post or query string query
            if (HttpContext.Request.Method.Equals("POST") || HttpContext.Request.Query.Keys.Any())
                activities = _activityService.Search(keywords, categoryids, allOrganizationsSelected ? null : selectedOrganizationIds);
            else
                ViewBag.initialLoad = true;

            ViewBag.cbList = categoryids.ToList(); // pass through selected leaf nodes
            ViewBag.orgList = orgList;


			return View("Search", model: activities.AsQueryable().GetPaged(page, 10, search, selectedNodeIds));

         }

        public IActionResult DataEntryPage()
        {           
            return RedirectToAction("index", "Activities");                      
        }
      
        public IActionResult Detail(Guid id)
        {
            ViewBag.GoogleApiKey = _googleApiKey;

            var activity = _activityService.GetActivityNew(id);

            var activityVm = new ActivityViewModel()
            {
                Id = activity.Id,
                Title = activity.Title,
                Description = activity.Description,
                DateTimeDescription = activity.DateTimeDescription,
                Contact = activity.Contact,
                LocationDescription = activity.LocationDescription,
                LocationPostcode = activity.LocationPostcode,
                OrganisationName = activity.OrganisationName,
                OrganisationWebsite = activity.OrganisationWebsite,
                OrganisationWebsiteLink = activity.OrganisationWebsiteLink,
                CreatedBy = activity.CreatedBy,
                DateCreated = activity.DateCreated,
                UpdatedBy = activity.UpdatedBy,
                DateUpdated = activity.DateUpdated,
                
            };

            var activityTypeInfo = (_context.ActivityTypes.Select(x => new ActivityTypeInfo()
            {
                Id = x.Id,
                ParentId = x.ParentId,
                Description = x.Description,
                DateCreated = x.DateCreated,
                DateDeleted = x.DateDeleted
            }).ToList());

            var classifications = GetActivityClassifications((Guid)id);
            var cbList = activityTypeInfo.BuildCheckboxList(classifications);

            activityVm.ActivityTypeAndAncestors = GetLeafAncestors(classifications, activityTypeInfo);
            
            activityVm.ActivityTypeList = cbList;
            activityVm.OrganisationList = GetOrganisations();

            return View(activityVm);
        }

            public IActionResult Login()
        {
            // if logged in, redirect to admin home
            return RedirectToAction(nameof(Login));
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        // get a list of activity types which are the ancestor of end leaf node
        private List<ActivityTypeInfo> GetLeafAncestors(string[] leafTypes, List<ActivityTypeInfo> allTypes)
        {
            var list = new List<ActivityTypeInfo>();

            foreach (var leaf in leafTypes)
            {
                // find first parent
                var parent = allTypes.Find(t => t.Id == Guid.Parse(leaf));
                if (parent != null)
                {
                    list.Add(parent);
                    list = GetLeafAncestorsRecurse(list, parent, allTypes);
                }
                
            }
            return list;
        }

        private List<ActivityTypeInfo> GetLeafAncestorsRecurse(List<ActivityTypeInfo> list, ActivityTypeInfo currentNode, List<ActivityTypeInfo> allTypes)
        {
            // find parent, add and return
            var parent = allTypes.Find(t => t.Id == currentNode.ParentId);
            if (parent != null)
            {
                list.Add(parent);
                list = GetLeafAncestorsRecurse(list, parent, allTypes);
            }

            return list;
        }

        private string[] GetActivityClassifications(Guid activityId)
        {
            var classifications = _context.ActivityClassifications
                .Where(x => x.Activity.Id == activityId)
                .Select(x => x.ActivityType.Id.ToString()).ToArray();
            return classifications;
        }

        private IList<CheckboxListItem> GetOrganisations()
        {
	        var organizations = _context.Organisations
				.OrderBy(o => o.Name)
				.Select(o => new CheckboxListItem()
	        {
		        Id = $"org_{o.Id}",
		        Display = o.Name,
		        IsChecked = false
	        }).ToList();

	        return organizations;
        }

		//private string FormatFilterList(string cbList)
		//{
		//    var cbValues = cbList.Replace("_",",").Split(",");
		//}
	}
}
