﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service;
using Wellbeing.Data.Service.Interfaces;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Client.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private readonly WellbeingDbContext _context;
        private readonly ISecurityService _securityService;
        private readonly IUserService _userService;
        private readonly IUserMappingService _userMappingService;

        public UsersController(WellbeingDbContext context, ISecurityService securityService, IUserService userService, IUserMappingService userMappingService)
        {
            _context = context;
            _securityService = securityService;
            _userService = userService;
            _userMappingService = userMappingService;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            var users = await _userService.GetAll();           
            return View(users);
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userService.GetById(id.Value);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            
            ViewBag.Organisation = new SelectList(_context.Organisations.OrderBy(o => o.Name), "Id", "Name", 0);
            ViewBag.Roles = new SelectList(_context.Roles, "Id", "Name");

            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id, Name, Email, Password, RoleId, UserCompanyId, NotifyOnActivityUpdate")] WellbeingUser user)
        {
           
            var newUser = new User();
      
            newUser.Id = Guid.NewGuid();
            newUser.Name = user.Name;
            newUser.Email = user.Email;
            newUser.DateCreated = DateTime.Now;
            newUser.DateUpdated = DateTime.Now;
            newUser.NotifyOnActivityUpdate = user.NotifyOnActivityUpdate;

            var role = _context.Roles.FirstOrDefault(r => r.Id == user.RoleId);
            if (role != null)
                newUser.Role = role;

            var company = _context.Organisations.FirstOrDefault(o => o.Id == user.UserCompanyId);
            if (company != null)
                newUser.UserCompany = company;

            if (!string.IsNullOrEmpty(user.Password))
                newUser.Password = _securityService.HashText(user.Password, user.Email);

            _context.Add(newUser);
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));

        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userService.GetById(id.Value);
            if (user == null)
            {
                return NotFound();
            }

            ViewBag.Organisation = new SelectList(_context.Organisations.OrderBy(o => o.Name), "Id", "Name", 0);
            ViewBag.Roles = new SelectList(_context.Roles, "Id", "Name", user.RoleId);

            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id, Name, Email, Password, RoleId, UserCompanyId, NotifyOnActivityUpdate")] WellbeingUser user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var existingUser = _context.Users.FirstOrDefault(u => u.Id == id);

                    if (existingUser != null)
                    {
                        existingUser.Name = user.Name;
                        existingUser.NotifyOnActivityUpdate = user.NotifyOnActivityUpdate;
                        var role = _context.Roles.FirstOrDefault(r => r.Id == user.RoleId);
                        if (role != null)
                            existingUser.Role = role;

                        var company = _context.Organisations.FirstOrDefault(o => o.Id == user.UserCompanyId);
                        if (company != null)
                            existingUser.UserCompany = company;

                        if (!string.IsNullOrEmpty(user.Password))
                            existingUser.Password = _securityService.HashText(user.Password, existingUser.Email);

                        existingUser.DateUpdated = DateTime.Now;
                        
                        _context.Update(existingUser);
                    }
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .SingleOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var user = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(Guid id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}
