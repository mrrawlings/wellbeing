﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Wellbeing.Data.Client.Models;
using Wellbeing.Data.Service.Interfaces;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Client.Controllers
{
    public class AccountController : Controller
    {
        private const int DefaultIdelTimeoutInMinutes = 20;
        IUserService _userService;
        ISecurityService _securityService;
        ILogger _logger;
        private readonly int _idleTimeoutInMinutes;

        public AccountController(IUserService userService, ISecurityService securityService, ILogger<AccountController> logger, IConfiguration configuration)
        {
            _userService = userService;
            _securityService = securityService;
            _logger = logger;
            _idleTimeoutInMinutes = configuration.GetValue<int?>("IdleTimeoutInMinutes") ?? DefaultIdelTimeoutInMinutes;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            _logger.LogDebug($"login get return Url = {returnUrl}");
         
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public IActionResult LogIn(LoginViewModel form)
        {
            if (!ModelState.IsValid)
                return View(form);
            try
            {
                _logger.LogDebug($"Checking login for {form.Email}, {form.Password}");

                // login check
                 if (!(_userService.IsValidLoginAsync(form.Email, _securityService.HashText(form.Password, form.Email)).Result))
                {
                    _logger.LogDebug("Login failed");

                    ModelState.AddModelError("summary", "Login Failed!");
                    return View(form);
                }
                else
                {
                    _logger.LogDebug(string.Format("Login succeeded"));

                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, form.Email, ClaimValueTypes.String, "http://localhost")
                    };

                    var user = _userService.GetByEmail(form.Email);
                    if (user.Result.Role == WellbeingUser.UserRole.SuperAdmin)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, "SuperAdmin"));
                    }

                    if (user.Result.Role == WellbeingUser.UserRole.CompanyAdmin)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, "CompanyAdmin"));
                    }


                    claims.Add(new Claim("CompanyOrganisationId", user.Result.UserCompanyId.ToString()));
                   

                    var userIdentity = new ClaimsIdentity(claims, "SecureLogin");
                    var userPrincipal = new ClaimsPrincipal(userIdentity);

                     HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                        userPrincipal,
                        new AuthenticationProperties
                        {
                            ExpiresUtc = DateTime.UtcNow.AddMinutes(_idleTimeoutInMinutes),
                            IsPersistent = false,
                            AllowRefresh = true
                        });


                    if (!string.IsNullOrEmpty(ViewBag.ReturnUrl))
                    { 
                        Redirect(ViewBag.ReturnUrl);
                    }

                    return RedirectToHomePage();
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error logging in : {ex.ToString()}");

                ModelState.AddModelError("summary", ex.Message);
                return View(form);
            }
        }

        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction(nameof(Login));
        }

        public IActionResult RedirectToHomePage()
        {
            return RedirectToAction("Search", "Home");
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

        private IActionResult GoToReturnUrl(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToHomePage();            
        }
    }
}