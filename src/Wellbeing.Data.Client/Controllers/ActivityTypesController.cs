﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Wellbeing.Data.Client.Extensions;
using Wellbeing.Data.Client.Models;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Client.Controllers
{
    [Authorize]
    public class ActivityTypesController : Controller
    {
        private readonly WellbeingDbContext _context;

        public ActivityTypesController(WellbeingDbContext context)
        {
            _context = context;
        }

        // GET: ActivityTypes
        public async Task<IActionResult> Index()
        {
            // get only the root activity type for display

            var activityTypeInfo = _context.ActivityTypes
                .Select(x=>new ActivityTypeInfo() { Id = x.Id, ParentId=x.ParentId, Description=x.Description, DateCreated=x.DateCreated, DateDeleted=x.DateDeleted })
                .ToList();
            ViewBag.ActivityTypeList = activityTypeInfo.BuildCheckboxList(null);

            return View(await ActivityTypeHelper.GetTypesInParentChildOrder(_context));
        }

        
        // GET: ActivityTypes/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activityType = await _context.ActivityTypes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (activityType == null)
            {
                return NotFound();
            }

            return View(activityType);
        }

        // GET: ActivityTypes/Create
        public IActionResult Create()
        {
            ViewBag.ActivityTypeParentList = new SelectList(_context.ActivityTypes, "Id", "Description", 0);

            var list = _context.ActivityTypes.ToList();
            list.Add(new ActivityType { Id = Guid.Empty, Description = "Root Level" });
            ViewBag.ActivityTypes = list;

            return View();
        }

        // POST: ActivityTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ParentId,Description")] ActivityType activityType)
        {
            if (!string.IsNullOrEmpty(activityType.Description))
            {
                activityType.Id = Guid.NewGuid();
                if (activityType.ParentId != null) activityType.ParentId = activityType.ParentId;
                activityType.Description = activityType.Description;
                activityType.DateCreated = DateTime.Now; 
                _context.Add(activityType);

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(activityType);
        }

        // GET: ActivityTypes/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activityType = await _context.ActivityTypes.SingleOrDefaultAsync(m => m.Id == id);
            if (activityType == null)
            {
                return NotFound();
            }
            var list = _context.ActivityTypes.Where(at => at.Id != id).ToList();
            list.Add(new ActivityType { Id = Guid.Empty, Description = "Root Level" });
            ViewBag.ActivityTypes = list;

            return View(activityType);
        }

        // POST: ActivityTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,ParentId,Description")] ActivityType activityType)
        {
            if (id != activityType.Id)
            {
                return NotFound();
            }

            if (!string.IsNullOrEmpty(activityType.Description))
            {
                try
                {            
                        _context.Update(activityType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ActivityTypeExists(activityType.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View(activityType);
        }

        // GET: ActivityTypes/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activityType = await _context.ActivityTypes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (activityType == null)
            {
                return NotFound();
            }

            return View(activityType);
        }

        // POST: ActivityTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var activityType = await _context.ActivityTypes.SingleOrDefaultAsync(m => m.Id == id);
            // activityType.DateDeleted = DateTime.Now;
           // _context.ActivityTypes.Update(activityType);
            _context.ActivityTypes.Remove(activityType);           
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ActivityTypeExists(Guid id)
        {
            return _context.ActivityTypes.Any(e => e.Id == id);
        }

        public ActionResult ActionTypeTree()
        {
            return Content("test > test");
        }
    }
}
