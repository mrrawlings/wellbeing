﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace Wellbeing.Data.Client
{
    public class Program
    {
        public static void Main(string[] args)
        {
	        //Build Config
	        var currentEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
	        var configuration = new ConfigurationBuilder()
		        .SetBasePath(Directory.GetCurrentDirectory())
		        .AddJsonFile("appsettings.json")
		        .AddJsonFile($"appsettings.{currentEnv}.json", optional: true)
		        .AddEnvironmentVariables()
		        .Build();

	        //Configure logger
	        Log.Logger = new LoggerConfiguration()
		        .ReadFrom.Configuration(configuration)
		        .CreateLogger();

	        try
	        {
		        Log.Information("Starting web host");
		        BuildWebHost(args).Run();
	        }
	        catch (Exception ex)
	        {
		        Log.Fatal(ex, "Web Host terminated unexpectedly");
	        }
	        finally
	        {
		        Log.CloseAndFlush();
	        }
		}

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                //.UseApplicationInsights()               
                .UseStartup<Startup>()
                .UseSerilog()
				.Build();
    }
}
