﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Wellbeing.Data.Client.Models
{
    public class ActivityTypeViewModel
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        [Required]
        public string Description { get; set; }       
    }
}
