﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wellbeing.Data.Client.Models
{
    public class Pagination<T> : PaginationBase where T : class
    {
        public IList<T> ResultSet { get; set; }
       

        public Pagination()
        {
            ResultSet = new List<T>();
        }
    }
}
