﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Wellbeing.Data.Client.Models
{
    public class ActivityListViewModel: Pagination<ActivityViewModel>
    {
        
       

        public int RowsFound { get; set; }
        public int RowsDisplayed { get; set; }
    }
}
