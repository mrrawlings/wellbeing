﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Wellbeing.Data.Client.Models
{
	public class FeedbackViewModel
	{
		public Guid Id { get; set; }

		[Required]

        [DisplayName("Your name")]
		public string Contact { get; set; }

		[Required]
        [DisplayName("Your email")]
		public string Email { get; set; }

		[Required]
		[DisplayName("Your comments or feedback")]
        public string Comment { get; set; }

		public DateTime DateCreated { get; set; }

		public bool NotificationSent { get; set; }

		public string Recipients { get; set; }

	}
}