﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wellbeing.Data.Client.Models
{
    public abstract class PaginationBase
    {
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public int RowCount { get; set; }
        public int FirstRow => (CurrentPage - 1) * PageSize + 1;
        public int LastRow => Math.Min(CurrentPage * PageSize, RowCount);
        public IList<string> Categories { get; set; }
        public string Search { get; set; }
    }
}
