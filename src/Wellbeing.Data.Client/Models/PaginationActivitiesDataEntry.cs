﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Wellbeing.Data.Client.Models
{
    public class PaginationActivitiesDataEntry<T> : PaginationBase where T : class
    {
        public IList<T> ResultSet { get; set; }

        public Guid EventProviderOrganisationFilter { get; set; }
        public Guid DataProviderOrganisationFilter { get; set; }
        public int? StatusFilter { get; set; }
        public Guid ActivityTypeFilter { get; set; }
        public bool HaveSearched { get; set; }

        public IEnumerable<SelectListItem> EventProviderOrganisationList { get; set; }
        public IEnumerable<SelectListItem> DataProviderOrganisationList { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
        public IEnumerable<SelectListItem> ActivityTypeList { get; set; }

        public PaginationActivitiesDataEntry()
        {
            ResultSet = new List<T>();
        }
    }
}
