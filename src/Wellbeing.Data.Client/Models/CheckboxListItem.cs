﻿namespace Wellbeing.Data.Client.Models
{
    public class CheckboxListItem
    {
        public string Id { get; set; }
        public string Display { get; set; }
        public bool IsChecked { get; set; }
    }
}