﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Client.Models
{
	public class ActivityHistoryViewModel
	{
		public Guid Id { get; set; }

		public string Description { get; set; }

		public string CreatedByUser { get; set; }

		public DateTime DateCreated { get; set; }

		public bool NotificationSent { get; set; }

		public string Recipients { get; set; }

	}
}
