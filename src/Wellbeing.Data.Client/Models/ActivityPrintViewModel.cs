﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Client.Models
{
	public class ActivityPrintViewModel
	{
		[DisplayName("When")]
		public string DateTimeDescription { get; set; }
		[Required]
		public string Description { get; set; }

		[DisplayName("Location")]
		public string LocationDescription { get; set; }

		[DisplayName("Postcode")]
		public string LocationPostcode { get; set; }

		[DisplayName("Organisation")]
		public Guid? OrganisationId { get; set; }

		[DisplayName("Organisation provider")]
		public string OrganisationName { get; set; }

		[DisplayName("Organisation Website")]
		public string OrganisationWebsite { get; set; }

		public string OrganisationWebsiteLink { get; set; }

		[DisplayName("Approval Status")]
		public int StatusId { get; set; }

		[Required]
		[StringLength(500, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 20)]
		public string Title { get; set; }

		public string Contact { get; set; }

		public DateTime DateCreated { get; set; }
		public User CreatedBy { get; set; }

		[DisplayName("User Organisation")]
		public string UserOrganisationName { get; set; }


		public DateTime? DateUpdated { get; set; }
		public User UpdatedBy { get; set; }

		public DateTime? DateDeleted { get; set; }

		public ActivityType ActivityType { get; set; }

		public IList<CheckboxListItem> ActivityTypeList { get; set; }
		public IList<CheckboxListItem> OrganisationList { get; set; }
		public ICollection<ActivityClassifications> ActivityClassifications { get; set; }

		[DisplayName("Primary Activity Type")]
		public ActivityType PrimaryClassification { get; set; }

		public IList<ActivityTypeInfo> ActivityTypeAndAncestors { get; set; }

	}
}