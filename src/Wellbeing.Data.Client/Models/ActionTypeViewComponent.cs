﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wellbeing.Data.Service.Interfaces;

namespace Wellbeing.Data.Client.Models
{
    public class ActionTypeViewComponent : ViewComponent
    {
        private IActivityTypeService _activitytypeService;

        public ActionTypeViewComponent(IActivityTypeService activitytypeService)
        {
            _activitytypeService = activitytypeService;
        }

        public IViewComponentResult Invoke(Guid actionTypeId)
        {
            var activityTypeList = _activitytypeService.GetActivityTypeTreeAsync(actionTypeId);
            activityTypeList.Reverse(); // changed the order

            string list = "";
            foreach (var item in activityTypeList)
            {
               // list += string.Format("<a href='{0}'>{1}</a>", item.Id, item.Description);
                list += string.Format("{0} ", item.Description);
            }

            

            return Content(list);
        }
    }

  
}
