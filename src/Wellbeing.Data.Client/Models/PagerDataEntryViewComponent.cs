﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Wellbeing.Data.Client.Models
{
    public class PagerDataEntryViewComponent : ViewComponent
    {
        public Task<IViewComponentResult> InvokeAsync(PaginationBase result)
        {            
            return Task.FromResult((IViewComponentResult)View("PageNavDataEntry", result));
        }
    }
}
