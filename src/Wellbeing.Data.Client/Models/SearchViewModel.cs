﻿using System.Collections.Generic;

namespace Wellbeing.Data.Client.Models
{
    public class SearchViewModel
    {
        public IList<string> Categories { get; set; }
        public string  Search { get; set; }
        public IEnumerable<ActivityViewModel> ActivityList { get; set; }
    }
}