﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Wellbeing.Data.Client.Extensions;
using Wellbeing.Data.Client.Interfaces;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Client.Services
{
    public class ActivitiesDataCache : IActivitiesDataCache
    {
        private WellbeingDbContext _context;
        private IList<Organisation> _organisations;
        private IList<SelectListItem> _selectListActivityTypeTree;

        public ActivitiesDataCache(WellbeingDbContext context)
        {
            _context = context;
        }

        public IList<Organisation> Organisations
        {
            get
            {
                if (_organisations == null)
                {
                    _organisations = _context.Organisations.OrderBy(o => o.Name).ToList();
                }
                return _organisations;
            }
        }

        public IList<SelectListItem> SelectListActivityTypeTree
        {
            get
            {
                if (_selectListActivityTypeTree == null)
                {
                    _selectListActivityTypeTree = ActivityTypeHelper.BuildSelectListTree(_context);
                }
                return _selectListActivityTypeTree;
            }
        }

    }
}
