jQuery(document).ready(function($) {

	var state = {
		activitiesToPrint: [],
		addActivityToPrint: function (activityId) {
			if (this.activitiesToPrint.indexOf(activityId) === -1) {
				this.activitiesToPrint.push(activityId);
				if (window.localStorage) {
					window.localStorage.setItem("activitiesToPrint", JSON.stringify(this.activitiesToPrint));
				}
				if (window.messageBus) {
					window.messageBus.activitiesToPrintChanged.dispatch(this.activitiesToPrint);
				}
			}
		},
		removeActivityToPrint: function (activityId) {
			var index = this.activitiesToPrint.indexOf(activityId);
			if (index !== -1) {
				this.activitiesToPrint.splice(index, 1);
				if (window.localStorage) {
					if (this.activitiesToPrint.length > 0) {
						window.localStorage.setItem("activitiesToPrint", JSON.stringify(this.activitiesToPrint));
					} else {
						window.localStorage.removeItem("activitiesToPrint");
					}
				}
				if (window.messageBus) {
					window.messageBus.activitiesToPrintChanged.dispatch(this.activitiesToPrint);
				}
			} 
		}
	};
	window.state = state;

	function updateStateFromLocationStorage() {
		if (window.localStorage) {
			var reportsToPrintString = window.localStorage.getItem('activitiesToPrint');
			if (reportsToPrintString) {
				window.state.activitiesToPrint = JSON.parse(reportsToPrintString);
			} else {
				window.state.activitiesToPrint = [];
			}
		}
	}

	if (window.localStorage) {
		var reportsToPrintString = window.localStorage.getItem('activitiesToPrint');
		if (reportsToPrintString) {
			state.activitiesToPrint = JSON.parse(reportsToPrintString);
		}
	}


	if (!state.activitiesToPrint) state.activitiesToPrint = [];

	function updateReportBadge() {
		if (state.activitiesToPrint.length === 0) {
			$('span.report-badge').text('');
			$('span.report-badge').hide();
		} else {
			$('span.report-badge').text(state.activitiesToPrint.length.toString());
			$('span.report-badge').show();
		}
	}

	function onReportAdded(activityId) {
		state.addActivityToPrint(activityId);
		updateReportBadge();
	}

	function onReportRemoved(activityId, reportUrl) {
		state.removeActivityToPrint(activityId);
		reloadWindow(reportUrl);

		updateReportBadge();
	}

	var Signal = signals.Signal;
	//console.log("Initializing app...");
	updateReportBadge();

	//custom object that dispatch signals
	window.messageBus = {
		reportAdded: new Signal(),
		reportRemoved: new Signal(),
		activitiesToPrintChanged: new Signal()
	};
	window.messageBus.reportAdded.add(onReportAdded);
	window.messageBus.reportRemoved.add(onReportRemoved);
	window.messageBus.activitiesToPrintChanged.dispatch(state.activitiesToPrint);

	function openReportWindow(actionUrl) {
		if (state.activitiesToPrint.length === 0) return;
		var mapForm = document.createElement("form");
		var milliseconds = new Date().getTime();
		var windowName = 'Print Activities' + milliseconds;
		mapForm.target = windowName;
		mapForm.method = "POST";
		mapForm.action = actionUrl;
		for (var i = 0; i < state.activitiesToPrint.length; i++) {
			var mapInput = document.createElement("input");
			mapInput.type = "hidden";
			mapInput.name = "printId_" + i.toString();
			mapInput.value = state.activitiesToPrint[i];
			mapForm.appendChild(mapInput);
		}
		document.body.appendChild(mapForm);

		//console.log("openReportWindow. Opening new window");
		var map = window.open('', windowName);
		if (map) {
			mapForm.submit();
		} else {
			console.error('Popups are not allowed. Report window will not be opened');
			alert('You must allow popups for this map to work.');
		}
	}

	function reloadWindow(actionUrl) {
		console.log("reloadWindow actionUrl: ", actionUrl);
		var reportForm = document.getElementById("printForm");
		if (reportForm) {
			reportForm.parentNode.removeChild(reportForm);
		}
		reportForm = document.createElement("form");
		reportForm.id = "printForm";
		reportForm.method = "POST";
		reportForm.action = actionUrl;
		for (var i = 0; i < state.activitiesToPrint.length; i++) {
			var mapInput = document.createElement("input");
			mapInput.type = "hidden";
			mapInput.name = "printId_" + i.toString();
			mapInput.value = state.activitiesToPrint[i];
			reportForm.appendChild(mapInput);
		}
		document.body.appendChild(reportForm);
		reportForm.submit();
	}


	$('a.report-link').click(function (evt) {
		evt.preventDefault();
		var reportUrl = $(this).attr('data-report-url');
		openReportWindow(reportUrl);
	});

	$(window).bind('storage', function (e) {
		//console.log("local storage event  e: ", e);
		updateStateFromLocationStorage();
		updateReportBadge();
	});

    var subData = [
        {title:"jQueryScript",href:"#1",dataAttrs:[{title:"dataattr1",data:"value1"},{title:"dataattr2",data:"value2"},{title:"dataattr3",data:"value3"}]}
        ,
        {title:"AngularJS",href:"#2",dataAttrs:[{title:"dataattr4",data:"value4"},{title:"dataattr5",data:"value5"},{title:"dataattr6",data:"value6"}]}
        ,
        {title:"ReactJS",href:"#3",dataAttrs:[{title:"dataattr7",data:"value7"},{title:"dataattr8",data:"value8"},{title:"dataattr9",data:"value9"}]}
    ];
    const data = [
        {title:"HTML5",href:"#1",dataAttrs:[{title:"dataattr1",data:"value1"},{title:"dataattr2",data:"value2"},{title:"dataattr3",data:"value3"}]}
        ,
        {title:"JavaScript",href:"#2",dataAttrs:[{title:"dataattr4",data:"value4"},{title:"dataattr5",data:"value5"},{title:"dataattr6",data:"value6"}],data:subData}
        ,
        {title:"CSS3",href:"#3",dataAttrs:[{title:"dataattr7",data:"value7"},{title:"dataattr8",data:"value8"},{title:"dataattr9",data:"value9"}]}
    ];


    var options = {
        title : "jQueryScriptNet",
        data: data,
        maxHeight: 3000,
        selectChildren : true,
        clickHandler: function(element){
            //gets clicked element parents
            console.log($(element).GetParents());
            //element is the clicked element
            console.log(element);
            $("#example").SetTitle($(element).find("a").first().text());
            console.log("Selected Elements",$("#example").GetSelected());
        },
        checkHandler: function(element){
            $("#example").SetTitle($(element).find("a").first().text());
        },
        closedArrow: '<i class="fa fa-caret-right" aria-hidden="true"></i>',
        openedArrow: '<i class="fa fa-caret-down" aria-hidden="true"></i>',
        multiSelect: true,
    }


    $("#example").DropDownTree(options);

});


