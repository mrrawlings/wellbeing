﻿
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;

namespace Wellbeing.Data.Client.Extensions
{
    public static class CustomHtmlHelpers
    {
        // not sure how to use this yet..
        public static IHtmlContent RenderExternalLink(this IHtmlHelper htmlHelper, string link)
            => new HtmlString($"<a href={link}>{link}</a>");
    
       
    }

}






