﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Client.Extensions
{
    public static class ActivityTypeHelper
    {

        public static async Task<ActivityType> GetRootType(WellbeingDbContext context, ActivityType at)
        {
            var parent = at;
            if (at.ParentId != null)
            {
                return await GetParent(context, at);
            }
            else
            {
                return at;
            }
        }

        private static async Task<ActivityType> GetParent(WellbeingDbContext context, ActivityType at)
        {
            var parent = context.ActivityTypes.FirstOrDefault(a => a.Id == at.ParentId);
            if (parent == null)
            {
                return at;
            }
            else
            {
                return await GetParent(context, parent);
            }
        }

        public static async Task<IEnumerable<ActivityType>> GetTypesInParentChildOrder(WellbeingDbContext context)
        {
            List<ActivityType> list = new List<ActivityType>();

            foreach (var parentActivity in context.ActivityTypes.Where(x => x.ParentId == Guid.Empty).OrderBy(x => x.Description))
            {
                // add parent
                list.Add(parentActivity);

                // then children
                list.AddRange(await AddChildren(context, parentActivity));
            }

            return list;
        }

        public static async Task<IEnumerable<ActivityType>> AddChildren(WellbeingDbContext context, ActivityType parent)
        {
            List<ActivityType> list = new List<ActivityType>();

            foreach (var child in context.ActivityTypes.Where(x => x.ParentId == parent.Id))
            {
                // add parent
                list.Add(child);

                // then children
                list.AddRange(await AddChildren(context, child));

            }

            return list;
        }

        public static IList<SelectListItem> BuildSelectListTree(WellbeingDbContext context)
        {

            var list = new List<SelectListItem>();

            foreach (var activityType in context.ActivityTypes.Where(x => x.ParentId == Guid.Empty).OrderBy(x => x.Description))
            {
                list.Add(new SelectListItem()
                    {
                        Value = activityType.Id.ToString(),
                        Text = activityType.Description
                    }
                );
                list.AddRange(GetChildSelectItems(context, activityType, 1));
            }        

            return list.ToList();

        }

        private static IList<SelectListItem> GetChildSelectItems(WellbeingDbContext context, ActivityType parent, int level)
        {
            var list = new List<SelectListItem>();
            var padding = new string[level];
            var strPadding = string.Join(string.Empty, padding.Select(c => "-"));

            foreach (var child in context.ActivityTypes.Where(x => x.ParentId == parent.Id))
            {
                // add parent
                list.Add(new SelectListItem()
                    {
                        Value = child.Id.ToString(),
                        Text = $"{strPadding}{child.Description}"
                    }
                );

                // then children
                list.AddRange(GetChildSelectItems(context, child, level + 1));
            }
         
            return list;
        }
    }
}
