﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wellbeing.Data.Client.Models;

namespace Wellbeing.Data.Client.Extensions
{
    public static class LinqExtensions
    {
        public static Pagination<T> GetPaged<T>(this IQueryable<T> query,
                                                    int page, int pageSize, string search, IList<string> categoriesSelected) where T : class
        {
            var result = new Pagination<T>();
            result.CurrentPage = page;
            result.PageSize = pageSize;
            result.RowCount = query.Count();
            result.Search = search;
            result.Categories = categoriesSelected;

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.ResultSet = query.Skip(skip).Take(pageSize).ToList();

            return result;
        }

        public static Pagination<T> GetPaged<T>(this IQueryable<T> query,
            int page, int pageSize) where T : class
        {
            var result = new Pagination<T>();
            result.CurrentPage = page;
            result.PageSize = pageSize;
            result.RowCount = query.Count();

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.ResultSet = query.Skip(skip).Take(pageSize).ToList();

            return result;
        }

        public static PaginationActivitiesDataEntry<T> GetPagedDataEntry<T>(this IQueryable<T> query,
            int page, int pageSize) where T : class
        {
            var result = new PaginationActivitiesDataEntry<T>();
            result.CurrentPage = page;
            result.PageSize = pageSize;
            result.RowCount = query.Count();

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.ResultSet = query.Skip(skip).Take(pageSize).ToList();

            return result;
        }

    }
}
