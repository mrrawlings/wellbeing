﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Wellbeing.Data.Client.Models;

namespace Wellbeing.Data.Client.Extensions
{
    public static class ActivityHelper
    {
        public static List<SelectListItem> GetStatusList()
        {
            var list = new List<SelectListItem>
            {
                new SelectListItem {Selected = false, Text = "Draft", Value = "0"},
                new SelectListItem {Selected = false, Text = "Approved", Value = "1"}
            };

            return list;
        }
    }
}
