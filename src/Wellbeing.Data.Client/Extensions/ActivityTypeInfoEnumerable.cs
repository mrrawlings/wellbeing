﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wellbeing.Data.Client.Models;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Client.Extensions
{
    public static class ActivityTypeInfoEnumerable
    {   

       public static IEnumerable<T> Expand<T>(
       this IEnumerable<T> source, Func<T, IEnumerable<T>> elementSelector)
        {
            var stack = new Stack<IEnumerator<T>>();
            var e = source.GetEnumerator();
            try
            {
                while (true)
                {
                    while (e.MoveNext())
                    {
                        var item = e.Current;
                        yield return item;
                        var elements = elementSelector(item);
                        if (elements == null) continue;
                        stack.Push(e);
                        e = elements.GetEnumerator();
                    }
                    if (stack.Count == 0) break;
                    e.Dispose();
                    e = stack.Pop();
                }
            }
            finally
            {
                e.Dispose();
                while (stack.Count != 0) stack.Pop().Dispose();
            }
        }

        private static IEnumerable<ActivityTypeInfo> FindAllChildren(IList<ActivityTypeInfo> allActivityTypes, ActivityTypeInfo actinfo)
        {
            var childrenByParentId = allActivityTypes.ToLookup(r => r.ParentId);
            return childrenByParentId[actinfo != null ? actinfo.Id : (Guid?)null].Expand(r => childrenByParentId[r.Id]);
        }

        public static IList<ActivityTypeInfo> BuildTree(this IList<ActivityTypeInfo> source)
        {
            var roots = new List<ActivityTypeInfo>();

            var rootLevelData = source.Where(x => x.ParentId == Guid.Empty|| x.ParentId == null )
                .OrderBy(x => x.Description)
                .ToList();

            foreach(var root in rootLevelData)
            {
                root.ChildActivityTypes = FindAllChildren(source, root).ToList();
                roots.Add(root);
            }

            return roots;
        }

        public static IList<CheckboxListItem> BuildCheckboxList(this IList<ActivityTypeInfo> source, string[] selectedTypes)
        {
            var cbList = new List<CheckboxListItem>();          

            var rootLevelData = source
                .Where(x => x.ParentId == Guid.Empty || x.ParentId == null)
                .OrderBy(x => x.Description)
                .ToList();

            foreach (var root in rootLevelData)
            {
                var cbName = string.Format("cb_{0}", root.Id.ToString());
                var isCheckboxSelected = IsArrayContaineTheValue(selectedTypes, cbName);    // check box tick if the value is already being selected
                cbList.Add(new CheckboxListItem() { Id= cbName, Display= root.Description, IsChecked = isCheckboxSelected });                          
                SubActivities(source, root.Id, ref cbList, ref cbName, selectedTypes);
              
            }
            return cbList;
        }

     

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringArray"></param>
        /// <param name="stringToCheck"></param>
        /// <returns></returns>

        private static bool IsArrayContaineTheValue(string[] stringArray, string stringToCheck)
        {
            if (stringArray == null) return false;
            var vals = stringToCheck.Split("_");
            var valueToCheck = vals[vals.Count()-1];

            if (stringArray != null &&  Array.Exists<string>(stringArray, (Predicate<string>)delegate (string s) {
                if (s == null) return false;
                return valueToCheck.IndexOf(s, StringComparison.OrdinalIgnoreCase) > -1;
            }))
            {
                return true;
            }
            return false;
        }

        private static void SubActivities(IList<ActivityTypeInfo> sourceChild, Guid activityId, ref List<CheckboxListItem> cbList, ref string cbName, string[] selectedTypes)
        {
            var childLevelData = sourceChild.Where(x => x.ParentId == activityId).ToList();
            var childCbName = cbName.ToString();
            foreach (var subLevel in childLevelData)
            {
                cbName = string.Format("{0}_{1}", childCbName, subLevel.Id.ToString());
                var isCheckboxSelected = IsArrayContaineTheValue(selectedTypes, cbName);    // check box tick if the value is already being selected
                cbList.Add(new CheckboxListItem() { Id = cbName, Display = subLevel.Description, IsChecked = isCheckboxSelected });
                SubActivities(sourceChild, subLevel.Id, ref cbList, ref cbName, selectedTypes);              
            }

        }


        private static void ChildActivities(IList<ActivityTypeInfo> sourceChild, Guid activityId, ref List<CheckboxListItem> cbList, ref string cbName)
        {           
            var childLevelData = sourceChild.Where(x => x.ParentId == activityId).ToList();
            var childCbName = cbName.ToString();
            foreach (var subLevel in childLevelData)
            {
                cbName = string.Format("{0}_{1}", childCbName, subLevel.Id.ToString());
                cbList.Add(new CheckboxListItem() { Id = cbName , Display = subLevel.Description, IsChecked= false});
                if(subLevel.ChildActivityTypes.Count > 0)
                {
                    ChildActivities(subLevel.ChildActivityTypes, subLevel.Id, ref cbList, ref cbName);
                }         
            }

        }


        private static void AddChildActivityTypes(ActivityTypeInfo node, IDictionary<Guid, List<ActivityTypeInfo>> source)
        {
            if (source.ContainsKey(node.Id))
            {
                node.ChildActivityTypes = source[node.Id];
                for (int i = 0; i < node.ChildActivityTypes.Count; i++)
                    AddChildActivityTypes(node.ChildActivityTypes[i], source);
            }
            else
            {
                node.ChildActivityTypes = new List<ActivityTypeInfo>();
            }
        }

    }
}
