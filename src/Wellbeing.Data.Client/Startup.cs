﻿using System.IO;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Wellbeing.Data.Service;
using Wellbeing.Data.Service.Interfaces;
using Microsoft.Extensions.Logging;
using Serilog;
using Wellbeing.Data.Client.Interfaces;
using Wellbeing.Data.Client.Services;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Client
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;           
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
	       services.AddSingleton(Log.Logger);
           services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme,
               options =>
               {
                   options.LoginPath = new PathString("/Account/Login");
                   options.AccessDeniedPath = new PathString("/Account/LogOff");
               });

           services.Configure<SmtpConfiguration>(Configuration.GetSection("Smtp"));

			services.AddTransient<IUserService, UserService>();
            services.AddTransient<ISecurityService, SecurityService>();
            services.AddTransient<IActivityTypeService, ActivityTypeService>();
            services.AddTransient<IUserMappingService, UserMapperService>();
            services.AddTransient<IActivityService, ActivityService> ();
            services.AddTransient<IActivityMappingService, ActivityMappingService>();
            services.AddTransient<IActivitiesDataCache, ActivitiesDataCache>();
            services.AddTransient<INotificationService, NotificationService>();
            services.AddTransient<IEmailService, EmailService>();

            services.AddDistributedMemoryCache();


			var connection = Configuration.GetConnectionString("WellbeingConnection");

            services.AddDbContext<Wellbeing.Data.Repository.WellbeingDbContext>
                (opt =>  opt.UseSqlServer(connection) );

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
                       
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();
        
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "rerootRoot",
                    template: "Index.html",
                    defaults: new {controller = "Home", action = "Search"});

            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "rerootRoot2",
                    template: "Home/Index",
                    defaults: new { controller = "Home", action = "Search" });

            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Search}/{id?}");
            });


            // Handle Lets Encrypt Route (before MVC processing!)
            // from https://weblog.west-wind.com/posts/2017/sep/09/configuring-letsencrypt-for-aspnet-core-and-iis
            app.UseRouter(r =>
            {
                r.MapGet(".well-known/acme-challenge/{id}", async (request, response, routeData) =>
                {
                    var id = routeData.Values["id"] as string;
                    var file = Path.Combine(env.WebRootPath, ".well-known", "acme-challenge", id);
                    await response.SendFileAsync(file);
                });
            });

            // put last so header configs like CORS or Cookies etc can fire
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
