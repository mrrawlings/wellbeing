﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Client.Interfaces
{
    // service to store data used in various methods to avoid too much database activity
    public interface IActivitiesDataCache
    {
        IList<Organisation> Organisations { get; }
        IList<SelectListItem> SelectListActivityTypeTree { get; }
    }
}
