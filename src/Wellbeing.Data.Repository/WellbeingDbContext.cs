﻿using Microsoft.EntityFrameworkCore;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Repository
{
    public class WellbeingDbContext : DbContext
    {
        public WellbeingDbContext(DbContextOptions<WellbeingDbContext> options)
            : base(options)
        {

        }

        public DbSet<ActivityType> ActivityTypes { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<ActivityClassifications> ActivityClassifications { get; set; }
		public DbSet<ActivityHistory> ActivityHistories { get; set; }
        public DbSet<Organisation> Organisations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
		public DbSet<Feedback> Feedbacks { get; set; }
    }
}