﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Wellbeing.Data.Repository.Models
{
    public class ActivityClassifications
    {
        [Key]        
        public Guid Id { get; set; }
        public Activity Activity { get; set; }
        public ActivityType ActivityType{ get; set; }
        public int TypePriority { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
