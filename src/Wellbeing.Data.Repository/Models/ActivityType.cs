﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Wellbeing.Data.Repository.Models
{
    /*
     * 
     * Domain class for defining an activity type.
     * */
    public class ActivityType
    {
        [Key]
        public Guid Id { get; set; }
        [DisplayName("Parent Type")]
        public Guid? ParentId { get; set; }
        public string Description { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
