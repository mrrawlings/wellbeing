﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Wellbeing.Data.Repository.Models
{
	public class Feedback
	{
		[Key]
		public Guid Id { get; set; }

		public string Contact { get; set; }

		public string Email { get; set; }

		public string Comment { get; set; }

		public DateTime DateCreated { get; set; }

		public bool NotificationSent { get; set; }

		public string Recipients { get; set; }

	}
}