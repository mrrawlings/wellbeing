﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Wellbeing.Data.Repository.Models
{
	public class ActivityHistory
	{
		[Key]
		public Guid Id { get; set; }

		public string Description { get; set; }

		[DisplayName("Activity")]
		public Activity Activity { get; set; }

		[DisplayName("Created By")]
		public User CreatedBy { get; set; }

		[DisplayName("Date created")]
		public DateTime DateCreated { get; set; }

		public bool NotificationSent { get; set; }

		public string Recipients { get; set; }

	}
}