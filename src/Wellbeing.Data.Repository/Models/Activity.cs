﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Wellbeing.Data.Repository.Models
{
    public class Activity
    {
        [Key]
        public Guid Id { get; set; }
        public Organisation Organisation { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string Contact { get; set; }

        [DisplayName("When")]
        public string DateTimeDescription { get; set; }

        [DisplayName("Location")]
        public string LocationDescription { get; set; }

        [DisplayName("Postcode")]
        public string LocationPostcode { get; set; }

        public DateTime? DateDeleted { get; set; }

        [DisplayName("Date created")]
        public DateTime DateCreated { get; set; }

        [DisplayName("Date updated")]
        public DateTime? DateUpdated { get; set; }

        [DisplayName("Created By")]
        public User CreatedBy { get; set; }

        [DisplayName("Last Updated By")]
        public User UpdatedBy { get; set; }

        [DisplayName("Status")]
        public int Status { get; set; }

    }
}
