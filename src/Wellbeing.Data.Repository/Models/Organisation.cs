﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wellbeing.Data.Repository.Models
{
    public class Organisation
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Contact { get; set; }
        public string Funding { get; set; }
        public bool Noteworthy { get; set; }
        public bool Generic { get; set; }
        public bool Religious { get; set; }

    }
}
