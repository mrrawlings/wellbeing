﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Wellbeing.Data.Repository
{
    public class WellbeingContextFactory: IDesignTimeDbContextFactory<WellbeingDbContext>
    {
        public WellbeingDbContext CreateDbContext(string[] args)
        {
            var currentEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{currentEnv}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<WellbeingDbContext>();
            var connectionString = configuration.GetConnectionString("WellbeingConnection");
            optionsBuilder.UseSqlServer(connectionString);
            return new WellbeingDbContext(optionsBuilder.Options);
        }
            
    }
}
