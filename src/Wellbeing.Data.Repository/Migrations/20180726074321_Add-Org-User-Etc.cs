﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wellbeing.Data.Repository.Migrations
{
    public partial class AddOrgUserEtc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UserCompanyId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateUpdated",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedById",
                table: "Activities",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateUpdated",
                table: "Activities",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Activities",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedById",
                table: "Activities",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserCompanyId",
                table: "Users",
                column: "UserCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Activities_CreatedById",
                table: "Activities",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Activities_UpdatedById",
                table: "Activities",
                column: "UpdatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_Activities_Users_CreatedById",
                table: "Activities",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Activities_Users_UpdatedById",
                table: "Activities",
                column: "UpdatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Organisations_UserCompanyId",
                table: "Users",
                column: "UserCompanyId",
                principalTable: "Organisations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            // create st John's organisation
            migrationBuilder.Sql(
                @"  insert Organisations(id, name, address, email)
                    select newid(), 'St John`s Winchester', '32 St John’s South, The Broadway, Winchester', 'office@stjohnswinchester.co.uk'
                    where not exists(select id from Organisations where email = 'office@stjohnswinchester.co.uk')
            ");

            // set admin user to St Johns
            migrationBuilder.Sql(
                @"update users set DateUpdated = getdate(), UserCompanyId = (select top 1 id from Organisations where email = 'office@stjohnswinchester.co.uk')"
            );

            // set creator on all current activities
            migrationBuilder.Sql(
                @" update Activities set DateUpdated = GETDATE(), CreatedById = (select id from users where name = 'Admin'),  UpdatedById = (select id from users where name = 'Admin')"
            );

            migrationBuilder.Sql(
                @" update Roles set Name = 'SuperAdmin'"
            );

            migrationBuilder.Sql(
                @" insert Roles(id, name) select newid(), 'CompanyAdmin' from roles where name not in (select name from roles)"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Activities_Users_CreatedById",
                table: "Activities");

            migrationBuilder.DropForeignKey(
                name: "FK_Activities_Users_UpdatedById",
                table: "Activities");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Organisations_UserCompanyId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserCompanyId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Activities_CreatedById",
                table: "Activities");

            migrationBuilder.DropIndex(
                name: "IX_Activities_UpdatedById",
                table: "Activities");

            migrationBuilder.DropColumn(
                name: "UserCompanyId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "Activities");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Activities");

            migrationBuilder.DropColumn(
                name: "UpdatedById",
                table: "Activities");
        }
    }
}
