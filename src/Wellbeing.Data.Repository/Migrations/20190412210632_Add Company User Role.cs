﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wellbeing.Data.Repository.Migrations
{
    public partial class AddCompanyUserRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DateUpdated",
                table: "Users",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.Sql(
                @" insert Roles(id, name) select newid(), 'CompanyAdmin' from roles where name not in (select name from roles where name = 'CompanyAdmin')"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DateUpdated",
                table: "Users",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.Sql(
                @" delete Roles where name like 'CompanyAdmin'"
            );
        }
    }
}
