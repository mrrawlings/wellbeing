﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Wellbeing.Data.Repository.Migrations
{
    public partial class NewFieldsAddedOrganisation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Organisations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact",
                table: "Organisations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Organisations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Funding",
                table: "Organisations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Generic",
                table: "Organisations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Noteworthy",
                table: "Organisations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Organisations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Religious",
                table: "Organisations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Website",
                table: "Organisations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Organisations");

            migrationBuilder.DropColumn(
                name: "Contact",
                table: "Organisations");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Organisations");

            migrationBuilder.DropColumn(
                name: "Funding",
                table: "Organisations");

            migrationBuilder.DropColumn(
                name: "Generic",
                table: "Organisations");

            migrationBuilder.DropColumn(
                name: "Noteworthy",
                table: "Organisations");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Organisations");

            migrationBuilder.DropColumn(
                name: "Religious",
                table: "Organisations");

            migrationBuilder.DropColumn(
                name: "Website",
                table: "Organisations");
        }
    }
}
