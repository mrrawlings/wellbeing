﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Wellbeing.Data.Repository.Migrations
{
    public partial class AddedOrganisation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "OrganisationId",
                table: "Activities",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Activities",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Organisations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organisations", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Activities_OrganisationId",
                table: "Activities",
                column: "OrganisationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Activities_Organisations_OrganisationId",
                table: "Activities",
                column: "OrganisationId",
                principalTable: "Organisations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Activities_Organisations_OrganisationId",
                table: "Activities");

            migrationBuilder.DropTable(
                name: "Organisations");

            migrationBuilder.DropIndex(
                name: "IX_Activities_OrganisationId",
                table: "Activities");

            migrationBuilder.DropColumn(
                name: "OrganisationId",
                table: "Activities");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Activities");
        }
    }
}
