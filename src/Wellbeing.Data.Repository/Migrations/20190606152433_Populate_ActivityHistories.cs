﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wellbeing.Data.Repository.Migrations
{
    public partial class Populate_ActivityHistories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
	        migrationBuilder.Sql(
				"insert into [dbo].[ActivityHistories] ([Id],[Description],[ActivityId],[CreatedById],[DateCreated],[NotificationSent],[Recipients]) " 
				+"select NEWID(), 'Created', a.Id, a.CreatedById, a.DateCreated, 0, NULL "
		        +"from Activities a left "
		        +"join ActivityHistories ah on a.Id = ah.ActivityId "
				+"where ah.Id is null");
		}

		protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
