﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wellbeing.Data.Repository.Migrations
{
    public partial class AddSearchProc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            var sql = @"
                create proc dbo.ActivitySearch
                @SearchText varchar(200) = null
                as

                select 
	                * 
                from 
	                Activities a
                where
	                (
		                @SearchText is null 
		                or a.Description like '%' + @SearchText + '%'
	                )
	                or
	                (
		                @SearchText is null 
		                or a.Title like '%' + @SearchText + '%'
	                )

	                or
	                (
		                a.Id in (

				                select 
					                ActivityId 
				                from 
					                ActivityClassifications ac join 
					                ActivityTypes at on ac.ActivityTypeId = at.Id
				                where
					                at.Description like '%' + @SearchText + '%'
				                ) 
	                )
	                or
	                (
		                @SearchText is null 
		                or a.LocationDescription like '%' + @SearchText + '%'
	                )
	                or
	                (
		                @SearchText is null 
		                or a.LocationPostcode like '%' + @SearchText + '%'
	                )
	                or
	                (
		                @SearchText is null 
		                or a.Contact like '%' + @SearchText + '%'
	                )
	                or
	                (
		                @SearchText is null 
		                or a.DateTimeDescription like '%' + @SearchText + '%'
	                )
	                or
	                (
		                a.OrganisationId in (

				                select 
					                Id 
				                from 
					                Organisations o
				                where
					                o.Name like '%' + @SearchText + '%'
				                ) 
	                )

                go		                
            ";
            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            var sql = "drop procedure dbo.ActivitySearch";

            migrationBuilder.Sql(sql);

        }
    }
}
