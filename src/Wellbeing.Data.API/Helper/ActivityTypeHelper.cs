﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Wellbeing.Data.API.Mapper;
using Wellbeing.Data.API.Models;
using Wellbeing.Data.API.Models.Domain;
using Wellbeing.Data.Repository;
using ActivityType = Wellbeing.Data.API.Models.Domain.ActivityType;

namespace Wellbeing.Data.API.Helper
{
    public class ActivityTypeHelper
    {
        private readonly WellbeingDbContext _context;
        private readonly ActivityTypeMapper _mapper;

        public ActivityTypeHelper(WellbeingDbContext context, ActivityTypeMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ActivityType GetById(Guid id)
        {            
            var item = _context.ActivityTypes.FirstOrDefault(a => a.Id == id);
            if (item == null)
            {
                return null;
            }
            return _mapper.Map(item);

        }

        public IEnumerable<ActivityType> GetAll()
        {
            var list = _context.ActivityTypes
                // ignore deleted ones
                .Where(activityType => activityType.DateDeleted == null)
                .Where(a => a.ParentId == Guid.Empty)
                .ToList();

            var listDomain = new List<Models.Domain.ActivityType>();
            foreach (var activityType in list)
            {
                listDomain.Add(BuildHierarchy(_mapper.Map(activityType)));                
            }
            return listDomain;
        }

        public Models.Domain.ActivityType CreateNew(Models.Domain.ActivityType activityType)
        {
            var repoModel = _mapper.Map(activityType);           

            if (repoModel.Id == Guid.Empty)
            {
                // set new instance Id
                repoModel.Id = Guid.NewGuid();
            }

            _context.ActivityTypes.Add(repoModel);
            _context.SaveChanges();

            return _mapper.Map(repoModel);
        }

        public void Delete(Guid id)
        {
            var item = _context.ActivityTypes.FirstOrDefault(a => a.Id == id);
            if (item == null)
            {
                return;
            }

            if (item.DateDeleted != DateTime.MinValue)
            {
                // perform a soft delete because we might have attached activities
                item.DateDeleted = DateTime.Now;
                _context.ActivityTypes.Update(item);
                _context.SaveChanges();
            }
        }
        private int GetLevel(Guid id, int level)
        {
            // drill up hierarchy until root hit
            var item = _context.ActivityTypes.FirstOrDefault(a => a.Id == id);
            if (item == null)
            {
                // hit end
                return level;
            }
            else
            {
                // call up stack
                return GetLevel((Guid)item.ParentId, level + 1);
            }
        }

        private ActivityType BuildHierarchy(Models.Domain.ActivityType activityType)
        {
            
            // get a list of child types
            var children = _context.ActivityTypes
                // ignore deleted ones
                .Where(a => activityType.DateDeleted == null)
                .Where(a => a.ParentId == activityType.Id & a.ParentId != Guid.Empty)
                .ToList();

            // add mapped chidlers
            activityType.ChildActivityTypes = _mapper.Map(children); ;

            // recurse down hierachy
            foreach (var child in children)
            {
                BuildHierarchy(_mapper.Map(child));
            }

            return activityType;
        }

        public void AddSampleData()
        {
            if (!_context.ActivityTypes.Any())
            {
                _context.ActivityTypes.Add(new Repository.Models.ActivityType()
                {
                    Id = new Guid(),
                    Description = "Top level activity",
                    DateCreated = DateTime.Now
                });
                _context.SaveChanges();
            }
        }


      
    }
}
