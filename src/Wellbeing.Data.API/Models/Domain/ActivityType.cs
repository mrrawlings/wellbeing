﻿using System;
using System.Collections.Generic;

namespace Wellbeing.Data.API.Models.Domain
{
    /*
     * 
     * POCO class for defining an activity type.
     * */
    public class ActivityType
    {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public string Description { get; set; }
        public int Level { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime DateCreated { get; set; }
        public IEnumerable<ActivityType> ChildActivityTypes { get; set; }
    }
}
