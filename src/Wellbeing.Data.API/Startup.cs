﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Wellbeing.Data.API.Models;
using Microsoft.Extensions.Configuration;

namespace Wellbeing.Data.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
                      
            // switch to in memory option
            //services.AddDbContext<ActivityTypeContext>(opt => opt.UseInMemoryDatabase("Wellbeing"));
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseMvc();
        }
    }
}