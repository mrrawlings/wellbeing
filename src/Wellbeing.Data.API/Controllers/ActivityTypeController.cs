﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wellbeing.Data.API.Helper;
using Wellbeing.Data.API.Mapper;
using Wellbeing.Data.API.Models;
using Wellbeing.Data.API.Models.Domain;
using Wellbeing.Data.Repository;
using ActivityType = Wellbeing.Data.API.Models.Domain.ActivityType;

namespace Wellbeing.Data.API.Controllers
{
    [Produces("application/json")]
    [Route("api/ActivityType")]
    public class ActivityTypeController : Controller
    {
        private readonly ActivityTypeHelper _helper;

        public ActivityTypeController(WellbeingDbContext context)
        {
            _helper = new ActivityTypeHelper(context, new ActivityTypeMapper(context));
        }
        
        [HttpGet("{id}", Name = "GetActivityType")]
        public IActionResult GetById(Guid id)
        {
            var model = _helper.GetById(id);
            if (model == null)
            {
                return NotFound();
            }

            return new ObjectResult(model);
        }

        [HttpGet(Name = "activityType")]
        public IEnumerable<ActivityType> GetAllList()
        {
            return _helper.GetAll();
        }   

        [HttpPost()]
        public IActionResult Create([FromBody] ActivityType activityType)
        {
            if (activityType == null)
            {
                return BadRequest();
            }

            var model = _helper.CreateNew(activityType);
            return CreatedAtRoute("GetActivityType", new {id = activityType.Id}, model);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            if (_helper.GetById(id) == null)
            {
                return NotFound();
            }

            _helper.Delete(id);
            return new NoContentResult();
        }

        [HttpGet()]
        [Route("api/activitytype/createsampledata")]
        public IActionResult CreateSampleData()
        {
            return new NoContentResult();
        }



    }
}