﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wellbeing.Data.API.Models;
using Wellbeing.Data.API.Models.Domain;
using Wellbeing.Data.Repository;
using ActivityType = Wellbeing.Data.Repository.Models.ActivityType;

namespace Wellbeing.Data.API.Mapper
{
    public class ActivityTypeMapper
    {
        private readonly WellbeingDbContext _context;

        public ActivityTypeMapper(WellbeingDbContext context)
        {
            _context = context;
        }

        public Models.Domain.ActivityType Map(ActivityType from)
        {
            //  set basic attributes
            var model = new Models.Domain.ActivityType
            {
                Id = from.Id,
                ParentId = (Guid)from.ParentId,
                Description = from.Description,
                DateCreated = from.DateCreated,
                DateDeleted = from.DateDeleted,
                ChildActivityTypes = new Models.Domain.ActivityType[0]
            };

            return model;
        }

        public ActivityType Map(Models.Domain.ActivityType from)
        {
            var model = new ActivityType
            {
                Id = from.Id,
                ParentId = from.ParentId,
                Description = from.Description,
                DateCreated = from.DateCreated,
                DateDeleted = from.DateDeleted,
            };

            return model;
        }

        public List<Models.Domain.ActivityType> Map(List<ActivityType> from)
        {
            var childrenMapped = new List<Models.Domain.ActivityType>();
            foreach (var child in from)
            {
                childrenMapped.Add(Map(child));
            }
            return childrenMapped;
        }
    }
}
