﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Wellbeing.Data.Service.Interfaces;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Service
{
	public class EmailService : IEmailService
	{
		private readonly SmtpConfiguration _smtpConfiguration;
	    private readonly ILogger<EmailService> _logger;

        public EmailService(IOptionsMonitor<SmtpConfiguration> options, ILogger<EmailService> logger)
        {
            _logger = logger;
			_smtpConfiguration = options.CurrentValue;
		}

		public async Task Send(string recipients, string subject, string body, bool isHtml)
		{
		    try
		    {
		        _logger.LogDebug("About to get SmtpClient");
		        
                var smtpClient = GetSmtpClient();


                var message = new MailMessage();

		        message.From = new MailAddress(_smtpConfiguration.FromAddress, _smtpConfiguration.FromAddress);
		        message.Body = body;
		        message.Subject = subject;
		        message.IsBodyHtml = isHtml;
		        message.To.Add(recipients);

		        _logger.LogDebug("Sending mail recipients:" + recipients);
		        await smtpClient.SendMailAsync(message);
		    }
            catch (Exception e)
		    {
		        _logger.LogError("Error sending mail: " + e.Message);
		        //throw;
		    }
		}

		private SmtpClient GetSmtpClient()
		{
			var smtpClient = new SmtpClient();
		    if (string.IsNullOrEmpty(_smtpConfiguration.User))
		    {
		        smtpClient.UseDefaultCredentials = true;
            }
		    else
		    {
		        smtpClient.UseDefaultCredentials = false;
		        smtpClient.Credentials = new NetworkCredential(_smtpConfiguration.User, _smtpConfiguration.Password);
            }
						
			smtpClient.Host = _smtpConfiguration.Host;
			smtpClient.Port = _smtpConfiguration.Port;
			smtpClient.EnableSsl = _smtpConfiguration.EnableSsl;

			if (!string.IsNullOrWhiteSpace(_smtpConfiguration.PickupDirectoryLocation))
			{
				smtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
				smtpClient.PickupDirectoryLocation = _smtpConfiguration.PickupDirectoryLocation;
			}

		    _logger.LogDebug("SmtpClient.EnableSsl - " + smtpClient.EnableSsl);
		    _logger.LogDebug("SmtpClient.Port - " + smtpClient.Port);
		    _logger.LogDebug("SmtpClient.Host - " + smtpClient.Host);
		    _logger.LogDebug("SmtpClient.UseDefaultCredentials - " + smtpClient.UseDefaultCredentials);
		    _logger.LogDebug("SmtpClient.DeliveryMethod - " + smtpClient.DeliveryMethod);
		    _logger.LogDebug("SmtpClient.PickupDirectoryLocation - " + smtpClient.PickupDirectoryLocation);

            return smtpClient;
		}
	}
}