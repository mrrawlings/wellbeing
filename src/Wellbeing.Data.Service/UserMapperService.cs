﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Interfaces;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Service
{
    public class UserMapperService: IUserMappingService
    {
        private readonly WellbeingDbContext _context;
        private WellbeingUser.UserRole _defaultRole = WellbeingUser.UserRole.CompanyAdmin;

        public UserMapperService(WellbeingDbContext context)
        {
            _context = context;
        }

        public WellbeingUser Map(User from)
        {
            //  set basic attributes
            var model = new WellbeingUser()
            {
                Id = from.Id,
                DateCreated = from.DateCreated,
                UserCompany = from.UserCompany,
                DateDeleted = from.DateDeleted,
                Role = ParseRole(from.Role.Name),
                RoleId = from.Role.Id,
                DateUpdated = from.DateUpdated,
                Email = from.Email,
                Name = from.Name,
				NotifyOnActivityUpdate = from.NotifyOnActivityUpdate ?? false,
                UserCompanyId = (from.UserCompany != null) ? from.UserCompany.Id : Guid.Empty
            };
            return model;
        }

        public User Map(WellbeingUser from)
        {
            var role = _context.Roles.FirstOrDefault(r => r.Name == from.Role.ToString());
            //var company = _context.Organisations.FirstOrDefault(o => o.Id == from.UserCompany.Id);

            var model = new User()
            {
                Id = from.Id,
                DateCreated = from.DateCreated,
                UserCompany = from.UserCompany,
                DateDeleted = from.DateDeleted,
                Role = role,                
                DateUpdated = from.DateUpdated,
                Email = from.Email,
                Name = from.Name,
                Password = from.Password,
				NotifyOnActivityUpdate = from.NotifyOnActivityUpdate
            };
            return model;
        }

        private WellbeingUser.UserRole ParseRole(string value)
        {
            if (Enum.TryParse(typeof(WellbeingUser.UserRole), value, false, out var result))
            {
                return (WellbeingUser.UserRole)result;
            }
            else
            {
                return _defaultRole;
            }
        }
    }
}
