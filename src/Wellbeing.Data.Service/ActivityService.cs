﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Service.Interfaces;
using Wellbeing.Data.Service.Models;
using Wellbeing.Data.Repository.Models;


namespace Wellbeing.Data.Service
{
    public class ActivityService: IActivityService
    {
        private readonly WellbeingDbContext _context;
        private readonly IActivityMappingService _activityMappingService;
        private readonly ILogger<ActivityService> _logger;

		public ActivityService(WellbeingDbContext context, IActivityMappingService activityMappingServicee, ILogger<ActivityService> logger)
        {
            _context = context;
            _activityMappingService = activityMappingServicee;
            _logger = logger;
        }

		public List<WellbeingActivity> GetActivities(string[] keywords, Guid? eventProviderOrganisationId, Guid? dataProviderOrganisationId, int? statusFilter, Guid? activityTypeFilter)
        {
            var query = _context.Activities.AsQueryable();
            if (keywords != null && keywords.Count(x => !string.IsNullOrEmpty(x)) > 0)
            {
                foreach (var keyword in keywords)
                {
                    query = query.Where(x => x.Title.Contains(keyword)
                                             || x.Description.Contains(keyword)
                                             || x.Contact.Contains(keyword)
                                             || x.LocationDescription.Contains(keyword)
                                             || x.LocationPostcode.Contains(keyword));
                }
            }

            var activities =  query
                .Where(x => x.DateDeleted == null || x.DateDeleted >= DateTime.Now)
                .Where(x=> 
                    (eventProviderOrganisationId.HasValue && x.Organisation.Id == eventProviderOrganisationId.Value)
                    || (!eventProviderOrganisationId.HasValue || eventProviderOrganisationId == Guid.Empty))
                .Where(x =>
                    (dataProviderOrganisationId.HasValue && x.CreatedBy.UserCompany.Id == dataProviderOrganisationId.Value)
                    || (!dataProviderOrganisationId.HasValue || dataProviderOrganisationId == Guid.Empty))
                .Where(x => 
                    (statusFilter.HasValue && x.Status == statusFilter)
                    || (!statusFilter.HasValue))     
                .Where(x =>
                    (activityTypeFilter.HasValue 
                        && activityTypeFilter.Value != Guid.Empty
                        && _context.ActivityClassifications.Any(ac => ac.Activity.Id == x.Id && ac.ActivityType.Id == activityTypeFilter))
                    || (!activityTypeFilter.HasValue) || (activityTypeFilter.Value == Guid.Empty)
                )
                .Include(x => x.Organisation)
                .Include(y => y.CreatedBy.UserCompany)
                .OrderByDescending(x => x.DateCreated);

                var wbActivities = activities.Select(a =>
                _activityMappingService.Map(a)
            ).ToList();

            wbActivities.ForEach(a => 
                a.ActivityType = _context.ActivityClassifications
                    .Include(ac => ac.ActivityType)
                    .FirstOrDefault(b => b.Activity.Id == a.Id)?.ActivityType
            );

            return wbActivities;
        }

        public WellbeingActivity GetActivity(Guid id)
        {
            var activity = _context.Activities
                .Include(x => x.Organisation)
                .Include(y => y.CreatedBy.UserCompany)
                .FirstOrDefault(x => x.Id == id);                

            var wbActivitity = _activityMappingService.Map(activity);

            wbActivitity.ActivityType = _context.ActivityClassifications
                    .Include(ac => ac.ActivityType)
                    .FirstOrDefault(b => b.Activity.Id == wbActivitity.Id)?.ActivityType;

            return wbActivitity;
        }

        /// <summary>
        /// same as above except it includes all activity types - don't want to break / slow the above
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WellbeingActivity GetActivityNew(Guid id)
        {
            var activity = _context.Activities
                .Include(x => x.Organisation)
                .Include(y => y.CreatedBy.UserCompany)
                .FirstOrDefault(x => x.Id == id);

			if(activity == null)
			{
				_logger.LogWarning($"Activity with id: {id} not found in database");
			}
            var wbActivitity = _activityMappingService.Map(activity);

            wbActivitity.ActivityClassifications = _context.ActivityClassifications
                .Include(ac => ac.ActivityType)
                .Where(b => b.Activity.Id == wbActivitity.Id).ToList();


            return wbActivitity;
        }

        public IEnumerable<WellbeingActivity> Search(string[] keywords, string[] categoryids, IList<Guid> organizationIds)
        {

            //var categoryids = categories.Split(new char[] { ',' });
            IQueryable<Activity> query;

            if ((categoryids != null && categoryids.Any(x => !string.IsNullOrEmpty(x)))
	            || (organizationIds != null && organizationIds.Count > 0)) { 
               var filterQuery = _context.Activities.Join(_context.ActivityClassifications,
                                          activity => activity.Id,
                                          activityclass => activityclass.Activity.Id ,
                                          (activity, activityclass) => new { activity, activityclass }
                                        );



                // var predicate = PredicateBuilder.False<ActivityClassifications>();
                // var qs = "";
                // foreach (var categoryid in categoryids)
                // {
                //     Guid newGuid;
                //     if (Guid.TryParse(categoryid, out newGuid))
                //         filterQuery = filterQuery.Where(x => x.activityclass.ActivityType.Id == newGuid);
                ////        predicate = predicate.Or(p => p.Id == newGuid);

                // }

                if (categoryids != null && categoryids.Any(x => !string.IsNullOrWhiteSpace(x)))
                {
	                List<Guid> guidCategories = new List<Guid>();

	                foreach (var categoryid in categoryids)
	                {
		                Guid newGuid;
		                if (Guid.TryParse(categoryid, out newGuid))
			                guidCategories.Add(newGuid);
		                //        predicate = predicate.Or(p => p.Id == newGuid);
	                }

	                filterQuery = filterQuery.Where(x => guidCategories.Contains(x.activityclass.ActivityType.Id));
                }

                if (organizationIds != null && organizationIds.Count > 0)
                {
	                filterQuery = filterQuery.Where(x => organizationIds.Contains(x.activity.Organisation.Id));
                }

                query = filterQuery.Select(x => x.activity);
            }
            else
            {
                query = _context.Activities.AsQueryable();
            }

            if (keywords.Where(x => !string.IsNullOrEmpty(x)).Count()>0)
            {
                foreach(var keyword in keywords)
                {
                    query = query.Where(x => x.Title.Contains(keyword) 
                        || x.Description.Contains(keyword) 
                        || x.Contact.Contains(keyword)
                        || x.LocationDescription.Contains(keyword) 
                        || x.LocationPostcode.Contains(keyword));
                }
            }

             var activities= query.Where(y => !y.DateDeleted.HasValue).Select(
                        x => new WellbeingActivity()
                        {
                            Id = x.Id,
                            Title = x.Title,
                            DateTimeDescription = x.DateTimeDescription,
                            Description = x.Description,
                            LocationDescription = x.LocationDescription,
                            LocationPostcode = x.LocationPostcode,
                            OrganisationId = x.Organisation.Id,
                            OrganisationName = x.Organisation.Name,
                            StatusId = x.Status,
                            Contact = x.Contact,
                            DateCreated = x.DateCreated,
                            CreatedBy = x.CreatedBy,
                            UserOrganisationName = x.Organisation.Name,
                            DateUpdated = x.DateUpdated,
                            UpdatedBy = x.UpdatedBy,
                            DateDeleted = x.DateDeleted
                        }
                    ).OrderByDescending(x => x.DateCreated);


             return activities;
        }

        public IEnumerable<WellbeingActivity> GetActivitiesToPrint(IList<Guid> activitiesToPrint)
        {
	        var query = _context.Activities.AsQueryable();
	        query = query.Where(x => activitiesToPrint.Contains(x.Id));
	        var activities = query.Select(
		        x => new WellbeingActivity()
		        {
			        Id = x.Id,
			        Title = x.Title,
			        DateTimeDescription = x.DateTimeDescription,
			        Description = x.Description,
			        LocationDescription = x.LocationDescription,
			        LocationPostcode = x.LocationPostcode,
			        OrganisationId = x.Organisation.Id,
			        OrganisationName = x.Organisation.Name,
			        StatusId = x.Status,
			        Contact = x.Contact,
			        DateCreated = x.DateCreated,
			        CreatedBy = x.CreatedBy,
			        UserOrganisationName = x.Organisation.Name,
			        DateUpdated = x.DateUpdated,
			        UpdatedBy = x.UpdatedBy,
			        DateDeleted = x.DateDeleted
		        }
	        ).OrderByDescending(x => x.DateCreated);

	        return activities;
		}

		public async Task<IEnumerable<WellbeingActivityHistory>> GetActivityHistories(Guid id)
        {
	        var entities = _context.ActivityHistories
		        .Include(x => x.Activity)
		        .Include(y => y.CreatedBy.UserCompany)
		        .Where(x => x.Activity.Id == id);

	        var entitiesList = await entities.ToListAsync();
	        var models = _activityMappingService.Map(entitiesList);
	        return models;
        }


		/*
            public IEnumerable<WellbeingActivity> Search(string search, string categoryids)
        {
            // get a list of matching activity ids
            var activityIds = new List<Guid>();

            // only search if needed
            if (!string.IsNullOrEmpty(search))
            {
                activityIds = _context.Activities.FromSql("dbo.ActivitySearch @SearchText", new SqlParameter("@SearchText", search) )
                    .Select(a => a.Id)
                    .ToList();
            }


            var activities = _context.Activities.Where(a => string.IsNullOrEmpty(search))
                    .Include(x => x.Organisation)
                    .Select(
                        x => new WellbeingActivity()
                        {

                            Id = x.Id,
                            Title = x.Title,
                            DateTimeDescription = x.DateTimeDescription,
                            Description = x.Description,
                            LocationDescription = x.LocationDescription,
                            LocationPostcode = x.LocationPostcode,
                            OrganisationId = x.Organisation.Id,
                            OrganisationName = x.Organisation.Name,
                            StatusId = x.Status,
                            Contact = x.Contact,
                            DateCreated = x.DateCreated,
                            CreatedBy = x.CreatedBy,
                            UserOrganisationName = x.Organisation.Name,
                            DateUpdated = x.DateUpdated,
                            UpdatedBy = x.UpdatedBy,
                            DateDeleted = x.DateDeleted                        }
                    );

            if (!string.IsNullOrEmpty(categoryids)) {

                var result = activities.Where(l => categoryids.Split(new char[] { ',' }).Any(s => new [] { l.ActivityClassifications.Select(n => n.Id.ToString()).FirstOrDefault() }.Contains(s)));


                activities = activities
                    .Where(predicate: x => categoryids.Split(new char[] { ',' }).Join( x.ActivityClassifications.Select(n=>n.Id.ToString()).ToList(),
                                        p => p,
                                        q => q,
                                        (p, q) => p)
                                    .Any());
            }

            return activities.OrderByDescending(x => x.DateCreated);

        }
        */



	}
}
