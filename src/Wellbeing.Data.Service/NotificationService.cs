﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Interfaces;

namespace Wellbeing.Data.Service
{
	public class NotificationService : INotificationService
	{
		private const string DefaultStartDelimiter = "{";
		private const string DefaultEndDelimiter = "}";
		private const bool DefaultCaseInsensitiveMatchSearch = true;

		private readonly IUserService _userService;
		private readonly IEmailService _emailService;
		private const string ActivityCreatedTemplatePath = "ActivityCreatedTemplate.html";
		private const string ActivityApprovedTemplatePath = "ActivityApprovedTemplate.html";
		private const string FeedbackCreatedTemplatePath = "FeedbackCreateTemplate.html";

		private readonly ConcurrentDictionary<string, string> _templates = new ConcurrentDictionary<string, string>();

		private const string ActivityTitleToken = "ACTIVITY_TITLE";
		private const string ActivityOrganizationToken = "ACTIVITY_ORGANIZATION";
		private const string ActivityCreatedByToken = "ACTIVITY_CREATEDBY";
		private const string ActivityDateCreatedToken = "ACTIVITY_DATECREATED";

		private const string FeedbackContactToken = "FEEDBACK_CONTACT";
		private const string FeedbackEmailToken = "FEEDBACK_EMAIL";
		private const string FeedbackCommentToken = "FEEDBACK_COMMENT";
		private const string FeedbackDateCreatedToken = "FEEDBACK_DATECREATED";

		private const string ActivityCreatedSubject = "Activity Created";
		private const string ActivityApprovedSubject = "Activity Approved";
		private const string FeedbackCreatedSubject = "Feedback Created";

		public NotificationService(IUserService userService, IEmailService emailService)
		{
			_userService = userService;
			_emailService = emailService;
		}

		public async Task NotifyOnActivityCreated(ActivityHistory activityHistory)
		{
			await SendActivityNotification(activityHistory, ActivityCreatedTemplatePath, ActivityCreatedSubject);
		}

		public async Task NotifyOnActivityApproved(ActivityHistory activityHistory)
		{
			await SendActivityNotification(activityHistory, ActivityApprovedTemplatePath, ActivityApprovedSubject);
		}

		public async Task NotifyOnFeedbackCreated(Feedback feedback)
		{
			var recipients = await _userService.GetAllNotificationRecipients();
			if (recipients.Count == 0) return;

			var template = await GetEmbeddedTemplate(FeedbackCreatedTemplatePath);
			var parameters = ToTemplateParameters(feedback);

			var messageBody = HandleTemplate(template, parameters);

			foreach (var user in recipients)
			{
				await _emailService.Send(user.Email, FeedbackCreatedSubject, messageBody);
			}

			feedback.NotificationSent = true;
			feedback.Recipients = string.Join(',', recipients.Select(r => r.Email).ToList());
		}

		private async Task SendActivityNotification(ActivityHistory activityHistory, string templatePath, string subject)
		{
			var recipients = await _userService.GetAllNotificationRecipients();
			if (recipients.Count == 0) return;

			var template = await GetEmbeddedTemplate(templatePath);
			var parameters = ToTemplateParameters(activityHistory);

			var messageBody = HandleTemplate(template, parameters);

			foreach (var user in recipients)
			{
				await _emailService.Send(user.Email, subject, messageBody);
			}

			activityHistory.NotificationSent = true;
			activityHistory.Recipients = string.Join(',', recipients.Select(r => r.Email).ToList());
		}


		private async Task<string> GetEmbeddedTemplate(string templatePath)
		{
			if (string.IsNullOrWhiteSpace(templatePath)) return string.Empty;
			if (!_templates.TryGetValue(templatePath, out var template))
			{
				var embeddedFileProvider = new EmbeddedFileProvider(Assembly.GetAssembly(typeof(NotificationService)), "Wellbeing.Data.Service.Templates");
				var embeddedFileInfo = embeddedFileProvider.GetFileInfo(templatePath);
				using (var stream = embeddedFileInfo.CreateReadStream())
				{
					using (var reader = new StreamReader(stream))
					{
						template = await reader.ReadToEndAsync();
						_templates.TryAdd(templatePath, template);
					}
				}

			}
			return template;
		}

		private Dictionary<string, string> ToTemplateParameters(ActivityHistory activityHistory)
		{
			var parameters = new Dictionary<string, string>()
			{
				{ActivityTitleToken, activityHistory.Activity.Title},
				{ActivityOrganizationToken, activityHistory.Activity.Organisation != null ? activityHistory.Activity.Organisation.Name : string.Empty},
				{ActivityCreatedByToken, activityHistory.CreatedBy.Name},
				{ActivityDateCreatedToken, activityHistory.DateCreated.ToString("MM/dd/yyyy hh:mm")},
			};
			return parameters;
		}

		private Dictionary<string, string> ToTemplateParameters(Feedback feedback)
		{
			var parameters = new Dictionary<string, string>()
			{
				{FeedbackContactToken, feedback.Contact},
				{FeedbackEmailToken, feedback.Email},
				{FeedbackCommentToken, feedback.Comment},
				{FeedbackDateCreatedToken, feedback.DateCreated.ToString("MM/dd/yyyy hh:mm")},
			};
			return parameters;
		}

		private string HandleTemplate(string template, IDictionary<string, string> parameters, bool? considerCase = false)
		{
			if (parameters == null || parameters.Count == 0)
			{
				return template;
			}
			var startDelimiter = DefaultStartDelimiter;
			var endDelimiter = DefaultEndDelimiter;

			var caseInsensitiveSearch = !considerCase ?? DefaultCaseInsensitiveMatchSearch;

			foreach (var parameter in parameters)
			{
				if (caseInsensitiveSearch)
				{
					var escapedToken = Regex.Escape($"{startDelimiter}{parameter.Key}{endDelimiter}");
					var replacement = string.IsNullOrEmpty(parameter.Value) ? " " : parameter.Value;
					template = Regex.Replace(template, escapedToken, replacement, RegexOptions.IgnoreCase);
				}
				else
				{
					template = template.Replace($"{startDelimiter}{parameter.Key}{endDelimiter}", parameter.Value);
				}
			}
			return template;
		}


	}
}