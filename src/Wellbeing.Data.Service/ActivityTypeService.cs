﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Interfaces;

using System.Linq;
using Microsoft.EntityFrameworkCore;


namespace Wellbeing.Data.Service
{
    public class ActivityTypeService : IActivityTypeService
    {
        private readonly WellbeingDbContext _context;

        public ActivityTypeService(WellbeingDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// activity types
        /// </summary>
        /// <param name="activityTypeId"></param>
        /// <returns></returns>
        public List<ActivityType> GetActivityTypeTreeAsync(Guid activityTypeId)
        {
            var activityList = new List<ActivityType>();
            var rootActivity = _context.ActivityTypes.SingleOrDefault(m => m.Id == activityTypeId);

            if (rootActivity != null)
            {
                activityList.Add(rootActivity);
                activityList = RecursiveActivityTree(rootActivity, activityList);
            }


            return activityList;
        }

        /// <summary>
        /// Get all the child activity types
        /// </summary>
        /// <param name="activityType"></param>
        /// <param name="rootList"></param>
        /// <returns></returns>
        public List<ActivityType> RecursiveActivityTree(ActivityType activityType, List<ActivityType> rootList )
        {
            var activityList = new List<ActivityType>();
            var childActivity =  _context.ActivityTypes.SingleOrDefault(m => m.ParentId == activityType.Id);

            if(childActivity!=null)
            {
                rootList.Add(childActivity);
                activityList =RecursiveActivityTree(childActivity, rootList);
            }
            return rootList;
        }


    }
}
