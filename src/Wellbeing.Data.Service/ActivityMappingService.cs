﻿using System.Collections.Generic;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Interfaces;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Service
{
    public class ActivityMappingService : IActivityMappingService
    {
        public WellbeingActivity Map(Activity from)
        {
            return new WellbeingActivity()
            {
                Id = from.Id,
                Title = from.Title,
                Description = from.Description,
                Contact = from.Contact,
                DateCreated = from.DateCreated,
                DateTimeDescription = from.DateTimeDescription,
                LocationDescription = from.LocationDescription,
                LocationPostcode = from.LocationPostcode,
                StatusId = from.Status,
                DateDeleted = from.DateDeleted,
                OrganisationId = from.Organisation?.Id,
                OrganisationName = from.Organisation?.Name,
                OrganisationWebsite = from.Organisation?.Website,
                OrganisationWebsiteLink = $"http://{from.Organisation?.Website}",
                UserOrganisationId =  from.CreatedBy.UserCompanyId
            };
        }

        public List<WellbeingActivityHistory> Map(IList<ActivityHistory> sourceList)
        {
	        if (sourceList == null || sourceList.Count == 0)
	        {
				return new List<WellbeingActivityHistory>();
	        }

			var targetList = new List<WellbeingActivityHistory>();
			foreach (var sourceItem in sourceList)
			{
				var targetItem = new WellbeingActivityHistory
				{
					Activity = Map(sourceItem.Activity),
					CreatedByUser = sourceItem.CreatedBy.Name,
					DateCreated = sourceItem.DateCreated,
					Description = sourceItem.Description,
					Id = sourceItem.Id,
					NotificationSent = sourceItem.NotificationSent,
					Recipients = sourceItem.Recipients
				};
				targetList.Add(targetItem);
			}

			return targetList;
        }
    }
}
