﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Service.Interfaces
{
    public interface IActivityService
    {
        List<WellbeingActivity> GetActivities(string[] search, Guid? eventProviderOrganisationId, Guid? dataProviderOrganisationId, int? statusFilter, Guid? activityTypeFilter);
        WellbeingActivity GetActivity(Guid id);
        WellbeingActivity GetActivityNew(Guid id);
        IEnumerable<WellbeingActivity> Search(string[] search, string[] categoryids, IList<Guid> organizationIds);
        IEnumerable<WellbeingActivity> GetActivitiesToPrint(IList<Guid> activitiesToPrint);
        Task<IEnumerable<WellbeingActivityHistory>> GetActivityHistories(Guid id);
    }
}
