﻿using System;
using System.Collections.Generic;
using System.Text;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Service.Interfaces
{
    public interface IActivityMappingService
    {
        WellbeingActivity Map(Activity from);
        List<WellbeingActivityHistory> Map(IList<ActivityHistory> sourceList);
    }

   
}
