﻿using System;
using System.Collections.Generic;
using System.Text;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Service.Interfaces
{
    public interface IActivityTypeInfo
    {
        Guid Id { get; set; }
        Guid? ParentId { get; set; }
        string Description { get; set; }
        int Level { get; set; }
        DateTime? DateDeleted { get; set; }
        DateTime DateCreated { get; set; }
        List<ActivityTypeInfo> ChildActivityTypes { get; set; }
    }
}
