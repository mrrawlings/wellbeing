﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Service.Interfaces
{
    public interface ISecurityService
    {
        string HashText(string textToHash, string salt);
    }
}
