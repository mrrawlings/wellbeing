﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Service.Interfaces
{
    public interface IActivityTypeService
    {
        List<ActivityType> GetActivityTypeTreeAsync(Guid activityTypeId);
    }
}
