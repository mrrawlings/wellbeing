﻿using System;
using System.Collections.Generic;
using System.Text;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Service.Interfaces
{
    public interface IUserMappingService
    {
        WellbeingUser Map(User from);
        User Map(WellbeingUser from);

    }
}
