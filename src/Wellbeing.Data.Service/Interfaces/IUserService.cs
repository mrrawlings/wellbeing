﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Service.Interfaces
{
    public interface IUserService
    {
        Task<bool> IsValidLoginAsync(string email, string password);
        Task<WellbeingUser> GetByEmail(string email);
        Task<WellbeingUser> GetById(Guid id);
        Task<List<WellbeingUser>> GetAll();
        Task<List<WellbeingUser>> GetAllNotificationRecipients();
    }
}
