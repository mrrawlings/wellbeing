﻿using System.Threading.Tasks;

namespace Wellbeing.Data.Service.Interfaces
{
	public interface IEmailService
	{
		Task Send(string recipients, string subject, string body, bool isHtml = true);
	}
}