﻿using System.Threading.Tasks;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Service.Interfaces
{
	public interface INotificationService
	{
		Task NotifyOnActivityCreated(ActivityHistory activityHistory);
		Task NotifyOnActivityApproved(ActivityHistory activityHistory);
		Task NotifyOnFeedbackCreated(Feedback feedback);
	}
}