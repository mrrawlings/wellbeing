﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Interfaces;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Service
{
    public class UserService : IUserService
    {
        private readonly WellbeingDbContext _context;
        private readonly IUserMappingService _userMappingService;
        private readonly ILogger _logger;
        public UserService(WellbeingDbContext context, IUserMappingService userMappingService, ILogger<IUserService> logger)
        {
            _context = context;
            _userMappingService = userMappingService;
            _logger = logger;            
        }

        public async Task<bool> IsValidLoginAsync(string email, string password)
        {
            _logger.LogDebug($"Finding email {email} and password {password}");

            var user = await _context.Users.SingleOrDefaultAsync(m => m.Email.Equals(email) && m.Password.Equals(password));

            _logger.LogDebug("User found? {(user != null)}");

            return user != null ? true : false;
        }

        public async Task<WellbeingUser> GetByEmail(string email)
        {
            var users = await _context.Users
                .Where(u => u.Email == email)
                .Include(u => u.UserCompany)
                .Include(u => u.Role)
                .ToListAsync();

            var wellbeingUsers = users.Select(u => _userMappingService.Map(u)).ToList();
            return wellbeingUsers.FirstOrDefault();
        }

        public async Task<WellbeingUser> GetById(Guid id)
        {
            var users = await _context.Users
                .Where(u => u.Id == id)
                .Include(u => u.UserCompany)
                .Include(u => u.Role)
                .ToListAsync();

            var wellbeingUsers = users.Select(u => _userMappingService.Map(u)).ToList();
            return wellbeingUsers.FirstOrDefault();
        }

        public async Task<List<WellbeingUser>> GetAll()
        {
            var users = await _context.Users
                .Include(u => u.UserCompany)
                .Include(u => u.Role)
                .ToListAsync();
            
            var wellbeingUsers = users.Select(u => _userMappingService.Map(u)).ToList();

            return wellbeingUsers;
        }

        public async Task<List<WellbeingUser>> GetAllNotificationRecipients()
        {
	        var users = await _context.Users
		        .Include(u => u.UserCompany)
		        .Include(u => u.Role)
		        .Where(u => u.NotifyOnActivityUpdate.HasValue && u.NotifyOnActivityUpdate.Value)
		        .Where(u => u.Role.Name == WellbeingUser.UserRole.SuperAdmin.ToString())
		        .ToListAsync();

	        var wellbeingUsers = users.Select(u => _userMappingService.Map(u)).ToList();

	        return wellbeingUsers;
        }
	}
}
