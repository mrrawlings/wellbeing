﻿using System;
using System.Collections.Generic;
using System.Text;
using Wellbeing.Data.Repository;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Models;

namespace Wellbeing.Data.Service
{
    public class ActivityTypeMapperService
    {
        private readonly WellbeingDbContext _context;

        public ActivityTypeMapperService(WellbeingDbContext context)
        {
            _context = context;
        }

        public ActivityTypeInfo Map(ActivityType from)
        {
            //  set basic attributes
            var model = new ActivityTypeInfo
            {
                Id = from.Id,
                ParentId = (Guid)from.ParentId,
                Description = from.Description,
                DateCreated = from.DateCreated,
                DateDeleted = from.DateDeleted,
                ChildActivityTypes = new List<ActivityTypeInfo>()
        };

            return model;
        }

        public ActivityType Map(ActivityTypeInfo from)
        {
            var model = new ActivityType
            {
                Id = from.Id,
                ParentId = (Guid) from.ParentId,
                Description = from.Description,
                DateCreated = from.DateCreated,
                DateDeleted = from.DateDeleted,
            };

            return model;
        }

        public List<ActivityTypeInfo> Map(List<ActivityType> from)
        {
            var childrenMapped = new List<ActivityTypeInfo>();
            foreach (var child in from)
            {
                childrenMapped.Add(Map(child));
            }
            return childrenMapped;
        }
    }
}
