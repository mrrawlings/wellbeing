﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Interfaces;

namespace Wellbeing.Data.Service
{
    public class SecurityService : ISecurityService
    {

        public SecurityService()
        {
        }

        public string HashText(string textToHash, string saltVal)
        {
            var salt = System.Text.Encoding.Unicode.GetBytes(saltVal);
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: textToHash,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
            return hashed;
        }
    }
}

