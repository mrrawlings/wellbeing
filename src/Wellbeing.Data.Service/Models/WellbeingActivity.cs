﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Service.Models
{
    public class WellbeingActivity
    {
        public enum ActivityStatus
        {
            Draft = 0,
            Approved = 1
        }

        public Guid Id { get; set; }

        public string DateTimeDescription { get; set; }
        public string Description { get; set; }

        public string LocationDescription { get; set; }
        public string LocationPostcode { get; set; }

        public Guid? OrganisationId { get; set; }
        public string OrganisationName { get; set; }
        public string OrganisationWebsite { get; set; }
        public string OrganisationWebsiteLink { get; set; }

        public int StatusId { get; set; }
        public string Title { get; set; }
        public string Contact { get; set; }
        public DateTime DateCreated { get; set; }
        public User CreatedBy { get; set; }

        public Guid UserOrganisationId{ get; set; }
        public string UserOrganisationName { get; set; }

        public DateTime? DateUpdated { get; set; }
        public User UpdatedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public ActivityType ActivityType { get; set; }
        public IList<ActivityClassifications> ActivityClassifications {get; set; }
    }
}
