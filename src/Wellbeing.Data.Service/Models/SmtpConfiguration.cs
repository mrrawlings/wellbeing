﻿namespace Wellbeing.Data.Service.Models
{
	public class SmtpConfiguration
	{
		public string Host { get; set; }
		public int Port { get; set; }
		public string User { get; set; }
		public string Password { get; set; }
		public string FromAddress { get; set; }
		public string PickupDirectoryLocation { get; set; }
		public bool EnableSsl { get; set; }
	}
}