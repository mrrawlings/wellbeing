﻿using System;
using Wellbeing.Data.Repository.Models;

namespace Wellbeing.Data.Service.Models
{
	public class WellbeingActivityHistory
	{
		public Guid Id { get; set; }

		public string Description { get; set; }

		public WellbeingActivity Activity { get; set; }

		public string CreatedByUser { get; set; }

		public DateTime DateCreated { get; set; }

		public bool NotificationSent { get; set; }

		public string Recipients { get; set; }

	}
}