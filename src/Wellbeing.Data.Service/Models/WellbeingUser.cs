﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Extensions;

namespace Wellbeing.Data.Service.Models
{
    public class WellbeingUser
    {
        public enum UserRole
        {
            [Description("Third Party Admin")]
            CompanyAdmin,

            [Description("St John's Admin")]
            SuperAdmin
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }
        public string RoleDescription
        {
            get
            {
                return StringExtensions.GetDescription(Role);
            }
        }
        public Guid RoleId { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public Organisation UserCompany { get; set; }
        public Guid? UserCompanyId { get; set; }

        [DisplayName("Notify me when new activity is added")]
        public bool NotifyOnActivityUpdate { get; set; }
    }
}
