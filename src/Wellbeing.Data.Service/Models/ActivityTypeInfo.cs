﻿using System;
using System.Collections.Generic;
using System.Text;
using Wellbeing.Data.Repository.Models;
using Wellbeing.Data.Service.Interfaces;

namespace Wellbeing.Data.Service.Models
{
    public class ActivityTypeInfo
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public string Description { get; set; }
        public int Level { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime DateCreated { get; set; }
        public List<ActivityTypeInfo> ChildActivityTypes { get; set; }
    }
}
