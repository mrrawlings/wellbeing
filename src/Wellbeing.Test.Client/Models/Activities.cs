﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Wellbeing.Test.Client.Models
{
    public partial class Activities
    {
        public Activities()
        {
            ActivityClassifications = new HashSet<ActivityClassifications>();
        }

        public Guid Id { get; set; }
        
        [DisplayName("When")]
        public string DateTimeDescription { get; set; }
        public string Description { get; set; }

        [DisplayName("Location")]
        public string LocationDescription { get; set; }

        [DisplayName("Postcode")]
        public string LocationPostcode { get; set; }

        [DisplayName("Organisation")]
        public Guid? OrganisationId { get; set; }
        public string Title { get; set; }
        public string Contact { get; set; }
        public Organisations Organisation { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateDeleted { get; set; }

        public ICollection<ActivityClassifications> ActivityClassifications { get; set; }
    }
}
