﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Wellbeing.Test.Client.Models
{
    public partial class WellbeingContext : DbContext
    {
        public virtual DbSet<Activities> Activities { get; set; }
        public virtual DbSet<ActivityClassifications> ActivityClassifications { get; set; }
        public virtual DbSet<ActivityTypes> ActivityTypes { get; set; }
        public virtual DbSet<Organisations> Organisations { get; set; }

        public WellbeingContext(DbContextOptions<WellbeingContext> options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Activities>(entity =>
            {
                entity.HasIndex(e => e.OrganisationId);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Organisation)
                    .WithMany(p => p.Activities)
                    .HasForeignKey(d => d.OrganisationId);
            });

            modelBuilder.Entity<ActivityClassifications>(entity =>
            {
                entity.HasIndex(e => e.ActivityId);

                entity.HasIndex(e => e.ActivityTypeId);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Activity)
                    .WithMany(p => p.ActivityClassifications)
                    .HasForeignKey(d => d.ActivityId);

                entity.HasOne(d => d.ActivityType)
                    .WithMany(p => p.ActivityClassifications)
                    .HasForeignKey(d => d.ActivityTypeId);
            });

            modelBuilder.Entity<ActivityTypes>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<Organisations>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });
        }
    }
}
