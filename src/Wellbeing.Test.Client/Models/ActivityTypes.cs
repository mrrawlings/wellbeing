﻿using System;
using System.Collections.Generic;

namespace Wellbeing.Test.Client.Models
{
    public partial class ActivityTypes
    {
        public ActivityTypes()
        {
            ActivityClassifications = new HashSet<ActivityClassifications>();
        }

        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string Description { get; set; }
        public Guid ParentId { get; set; }

        public ICollection<ActivityClassifications> ActivityClassifications { get; set; }
    }
}
