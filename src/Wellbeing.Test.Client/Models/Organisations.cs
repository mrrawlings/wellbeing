﻿using System;
using System.Collections.Generic;

namespace Wellbeing.Test.Client.Models
{
    public partial class Organisations
    {
        public Organisations()
        {
            Activities = new HashSet<Activities>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public ICollection<Activities> Activities { get; set; }
    }
}
