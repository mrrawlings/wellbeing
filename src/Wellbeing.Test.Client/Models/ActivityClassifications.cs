﻿using System;
using System.Collections.Generic;

namespace Wellbeing.Test.Client.Models
{
    public partial class ActivityClassifications
    {
        public Guid Id { get; set; }
        public Guid? ActivityId { get; set; }
        public Guid? ActivityTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateDeleted { get; set; }
        public int TypePriority { get; set; }

        public Activities Activity { get; set; }
        public ActivityTypes ActivityType { get; set; }
    }
}
