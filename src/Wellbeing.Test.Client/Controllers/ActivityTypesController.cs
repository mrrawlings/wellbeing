﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Wellbeing.Test.Client.Models;

namespace Wellbeing.Test.Client.Controllers
{
    public class ActivityTypesController : Controller
    {
        private readonly WellbeingContext _context;

        public ActivityTypesController(WellbeingContext context)
        {
            _context = context;
        }

        // GET: ActivityTypes
        public async Task<IActionResult> Index()
        {
            return View(await _context.ActivityTypes.ToListAsync());
        }

        // GET: ActivityTypes/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activityTypes = await _context.ActivityTypes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (activityTypes == null)
            {
                return NotFound();
            }

            return View(activityTypes);
        }

        // GET: ActivityTypes/Create
        public IActionResult Create()
        {
            var list = _context.ActivityTypes.ToList();
            list.Add(new ActivityTypes { Id = Guid.Empty, Description = "Root Level" });
            ViewBag.ActivityTypes = list;

            return View();
        }

        // POST: ActivityTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,ParentId")] ActivityTypes activityTypes)
        {
            if (ModelState.IsValid)
            {
                activityTypes.Id = Guid.NewGuid();
                _context.Add(activityTypes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(activityTypes);
        }

        // GET: ActivityTypes/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activityTypes = await _context.ActivityTypes.SingleOrDefaultAsync(m => m.Id == id);
            if (activityTypes == null)
            {
                return NotFound();
            }
            // exclude self!
            var list = _context.ActivityTypes.Where(at => at.Id != id).ToList();
            list.Add(new ActivityTypes{ Id = Guid.Empty, Description = "Root Level"});
            ViewBag.ActivityTypes = list;


            return View(activityTypes);
        }

        // POST: ActivityTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,DateCreated,DateDeleted,Description,ParentId")] ActivityTypes activityTypes)
        {
            if (id != activityTypes.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(activityTypes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ActivityTypesExists(activityTypes.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(activityTypes);
        }

        // GET: ActivityTypes/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activityTypes = await _context.ActivityTypes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (activityTypes == null)
            {
                return NotFound();
            }

            return View(activityTypes);
        }

        // POST: ActivityTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var activityTypes = await _context.ActivityTypes.SingleOrDefaultAsync(m => m.Id == id);
            _context.ActivityTypes.Remove(activityTypes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ActivityTypesExists(Guid id)
        {
            return _context.ActivityTypes.Any(e => e.Id == id);
        }
    }
}
