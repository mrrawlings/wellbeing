﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Wellbeing.Test.Client.Models;

namespace Wellbeing.Test.Client.Controllers
{
    public class ActivitiesController : Controller
    {
        private readonly WellbeingContext _context;

        public ActivitiesController(WellbeingContext context)
        {
            _context = context;
        }

        // GET: Activities
        public async Task<IActionResult> Index()
        {
            var wellbeingContext = _context.Activities.Include(a => a.Organisation);
            return View(await wellbeingContext.ToListAsync());
        }

        // GET: Activities/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activities = await _context.Activities
                .Include(a => a.Organisation)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (activities == null)
            {
                return NotFound();
            }

            return View(activities);
        }

        // GET: Activities/Create
        public IActionResult Create()
        {
            ViewData["Organisation"] = new SelectList(_context.Organisations, "Id", "Name");
            var list = _context.ActivityTypes.ToList();
            ViewBag.ActivityTypes = list;
            return View();
        }

        // POST: Activities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,LocationDescription,LocationPostcode,Contact,OrganisationId,Title")] Activities activities)
        {
            if (ModelState.IsValid)
            {
                activities.Id = Guid.NewGuid();
                _context.Add(activities);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
     
            ViewData["Organisation"] = new SelectList(_context.Organisations, "Id", "Name", activities.OrganisationId);
            return View(activities);
        }

        // GET: Activities/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activities = await _context.Activities.SingleOrDefaultAsync(m => m.Id == id);
            if (activities == null)
            {
                return NotFound();
            }
            var list = _context.ActivityTypes.ToList();
            ViewBag.ActivityTypes = list;

            ViewData["Organisation"] = new SelectList(_context.Organisations, "Id", "Name", activities.OrganisationId);
            return View(activities);
        }

        // POST: Activities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Contact,DateTimeDescription,Description,LocationDescription,LocationPostcode,OrganisationId,Title")] Activities activities)
        {
            if (id != activities.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(activities);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ActivitiesExists(activities.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["OrganisationId"] = new SelectList(_context.Organisations, "Id", "Id", activities.OrganisationId);
            return View(activities);
        }

        // GET: Activities/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activities = await _context.Activities
                .Include(a => a.Organisation)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (activities == null)
            {
                return NotFound();
            }

            return View(activities);
        }

        // POST: Activities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var activities = await _context.Activities.SingleOrDefaultAsync(m => m.Id == id);
            _context.Activities.Remove(activities);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ActivitiesExists(Guid id)
        {
            return _context.Activities.Any(e => e.Id == id);
        }
    }
}
